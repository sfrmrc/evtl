INTRODUZIONE
------------
progetto di sviluppo di framework java in pattern ETVL (ExtractValidateTransformLoad)

- definizione strutture dati generiche
- definizione superinterfacce e superclassi di fasi (E,V,T,L)
- integrazione con framework di validazione esistenti per operazioni di validazioni atomiche
- definizione uso Spring per DI di componenti

DIPENDENZE ESPLICITE
--------------------
	- JDK1.5
	vedi pom
		apache commons-validator
		apache commons-logging
		apache poi
DIPENDENZE TOBE
	TBC:spring
	TBC:commons-lang

CONCETTI
--------
catalogo
	type
	constraint
	constraintChecker
		si definiscono tipi(dati);
		sui tipi si definiscono constraint con parametri di configurazione
		si definiscono constraintChecker che applicano i constrant su valori
		il catalogo contiene le componenti di definizione tipologie dati e di loro controllo


consistenza
	attribute
		constrainedAttribute
			si definiscono attributi con tipo
			si definiscono constraint con collection di attributi, ovvero constraintAttribute
			la consistenza contiene le componenti di definizione di strutture dati
	TBC:occurrence
	TBC:entity
	TBC:providers
runtime
	field
	fieldValidator
	TBC:row
	TBC:rowValidator
	TBC:table
	TBC:tableValidator
			il runtime contiene le componenti di definizione di strutture valori estratte e da lavorare
	
