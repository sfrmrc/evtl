package it.beesol.frameworks.evtl.core.validator.syntactic.row;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.BaseAttributeConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.NotNullAttributeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.AlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali.IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric.InRangeIntegerOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence.BaseOccurrenceConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.field.BaseFieldSyntacticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.row.BaseRowSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidDecimalAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.InRangeIntegerConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.ObjectType;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.alphanum.AlphaNumType;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric.IntegerType;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class BaseRowSyntacticValidatorTest {
	
	private static BaseRowSyntacticValidator rowSyntacticValidator = null;
	private static ConstrainedOccurrence constrainedOccurrence = null;
	private static BaseFieldSyntacticValidator fieldSyntacticValidator = null;
	private static ConstrainedAttribute constrainedAttribute1 = null;
	private static ConstrainedAttribute constrainedAttribute2 = null;
	
	private static SerializableField serializableField1 = null;
	private static SerializableField serializableField2 = null;
	
	private static SerializableFieldLinkedSetRow<SerializableField> serializableFieldLinkedSetRow = null;
	
	@BeforeClass
	public static void setUpTest() throws Exception{
		
		// (A) istanziazione fieldSyntacticValidator
		
		fieldSyntacticValidator = new BaseFieldSyntacticValidator();
		
		BaseAttributeConstraintCheckerFactory onTypeConstraintCheckerFactory =
			   new BaseAttributeConstraintCheckerFactory();
		
		HashMap<Type, HashMap<String,ConstraintChecker>> typeConsMap =
			  new HashMap<Type, HashMap<String,ConstraintChecker>>(); 

		// (A.1.a) mappa constraint su AlphaNum
		HashMap<String,ConstraintChecker> consMap = new HashMap<String,ConstraintChecker>();
		consMap.put(AlphaNumConstraint.CONSTRAINT_NAME,
				    new AlphaNumOnTypeConstraintCheckerImpl());
		consMap.put(IsValidDecimalAlphaNumConstraint.CONSTRAINT_NAME,
				    new IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl());
		typeConsMap.put(new AlphaNumType(), consMap);
		// (A.1.b) mappa constraint su Object
		HashMap<String,ConstraintChecker> consMap2 = new HashMap<String,ConstraintChecker>();
		consMap2.put(NotNullConstraint.CONSTRAINT_NAME,
				    new NotNullAttributeConstraintCheckerImpl());
		typeConsMap.put(new ObjectType(), consMap2);
		// (A.1.c) mappa constraint su Integer
		HashMap<String,ConstraintChecker> consMap3 = new HashMap<String,ConstraintChecker>();
		consMap3.put(InRangeIntegerConstraint.CONSTRAINT_NAME,
			    new InRangeIntegerOnTypeConstraintCheckerImpl());
		typeConsMap.put(new IntegerType(), consMap3);
		
		onTypeConstraintCheckerFactory.setPerTypeConstraintCheckerMap(typeConsMap);
		
		fieldSyntacticValidator.setAttributeConstraintCheckerFactory(onTypeConstraintCheckerFactory);

		// (A.2.a) attributo "valore_monetario"
		constrainedAttribute1 = new ConstrainedAttribute();
		constrainedAttribute1.setName("valore_monetario");
		constrainedAttribute1.setType(new AlphaNumType());
		List<AttributeConstraint> onTypeConstraints = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints.add(new NotNullConstraint());
		//1:AlphaNumConstraint
		onTypeConstraints.add(new AlphaNumConstraint());
		//2:IsValidDecimalAlphaNumConstraint
		IsValidDecimalAlphaNumConstraint isValidDecimalAlphaNumConstraint = new IsValidDecimalAlphaNumConstraint();
		isValidDecimalAlphaNumConstraint.setIncludeNegative(false);
		onTypeConstraints.add(isValidDecimalAlphaNumConstraint);
		constrainedAttribute1.setAttributeConstraints(onTypeConstraints);
		
		// (A.2.b) attributo "quantita"
		constrainedAttribute2 = new ConstrainedAttribute();
		constrainedAttribute2.setName("quantita");
		constrainedAttribute2.setType(new IntegerType());
		List<AttributeConstraint> onTypeConstraints2 = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints2.add(new NotNullConstraint());
		//1:InRangeIntegerConstraint
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setMin(0);
		inRangeIntegerConstraint.setMax(999);
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		onTypeConstraints2.add(inRangeIntegerConstraint);
		constrainedAttribute2.setAttributeConstraints(onTypeConstraints2);
		
		// (B) istanziazione rowSyntacticValidator
		rowSyntacticValidator = new BaseRowSyntacticValidator();
		rowSyntacticValidator.setFieldSyntacticValidator(fieldSyntacticValidator);
		
		BaseOccurrenceConstraintCheckerFactory occurrenceConstraintCheckerFactory =
			   new BaseOccurrenceConstraintCheckerFactory<OccurrenceConstraint>();
		
		rowSyntacticValidator.setOccurrenceConstraintCheckerFactory(occurrenceConstraintCheckerFactory);
		
		constrainedOccurrence = new ConstrainedOccurrence();
		constrainedOccurrence.setName("prova");
		// occurrence senza controlli trasversali su row
		constrainedOccurrence.setOccurrenceConstraints(null);
		
		Hashtable<String,ConstrainedAttribute> constrainedAttributes = new Hashtable<String,ConstrainedAttribute>();
		constrainedAttributes.put(constrainedAttribute1.getName(),constrainedAttribute1);
		constrainedAttributes.put(constrainedAttribute2.getName(),constrainedAttribute2);
		constrainedOccurrence.setConstrainedAttributes(constrainedAttributes);
		
		// ... ...
		serializableField1 = new SerializableField();
		serializableField1.setName("valore_monetario");
		serializableField1.setValue("1,23");
		serializableField2 = new SerializableField();
		serializableField2.setName("quantita");
		serializableField2.setValue(new Integer("22"));
		serializableFieldLinkedSetRow = new SerializableFieldLinkedSetRow();
		serializableFieldLinkedSetRow.setName("prova");
		serializableFieldLinkedSetRow.setNumber(1);
	
	}
	
	// TODO : estendere assert su intera struttura Validated...
	
	@Test
	public void validRow() throws Exception{
		
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		serializableField1.setValue("1,23");
		serializableField2.setValue(new Integer("22"));
		fields.add(serializableField1);
		fields.add(serializableField2);
		serializableFieldLinkedSetRow.setFields(fields);
		
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
			  rowSyntacticValidator.validate(serializableFieldLinkedSetRow,constrainedOccurrence);
		
		assertNotNull("",validatedSerializableFieldLinkedSetRow);
		assertTrue("",validatedSerializableFieldLinkedSetRow.getIsValid());
		
		Iterator<SerializableField> inputFieldsIterator = fields.iterator();
		for(ValidatedSerializableField validatedSerializableField : validatedSerializableFieldLinkedSetRow.getFields()){
			SerializableField serializableInputField = inputFieldsIterator.next();
			assertTrue("",validatedSerializableField.getIsValid());
			assertNotNull("",validatedSerializableField.getName());
			assertEquals("",serializableInputField.getName(),validatedSerializableField.getName());
			assertNotNull("",validatedSerializableField.getValue());
			assertEquals("",serializableInputField.getValue(),validatedSerializableField.getValue());
			assertNotNull("",validatedSerializableField.getValidationAttribute());
			assertNotNull("",validatedSerializableField.getValidationAttribute().getName());
			assertEquals("",validatedSerializableField.getValidationAttribute().getName(),
					        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getName());
			assertNotNull("",validatedSerializableField.getValidationAttribute().getType());
			assertEquals("",validatedSerializableField.getValidationAttribute().getType(),
			        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getType());
			assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
			assertNotNull("",validatedSerializableField.getValidationAttribute().getConstraintValidationResult());
		}
			
	}
	
	@Test
	public void notValidRowByFirstFieldAllConstraints() throws Exception{
		
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		serializableField1.setValue(null);
		serializableField2.setValue(new Integer("22"));
		fields.add(serializableField1);
		fields.add(serializableField2);
		serializableFieldLinkedSetRow.setFields(fields);
		
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
			  rowSyntacticValidator.validate(serializableFieldLinkedSetRow,constrainedOccurrence);
		
		assertNotNull("",validatedSerializableFieldLinkedSetRow);
		assertFalse("",validatedSerializableFieldLinkedSetRow.getIsValid());
		
		Iterator<ValidatedSerializableField> validatedFieldsIterator = validatedSerializableFieldLinkedSetRow.getFields().iterator();
		ValidatedSerializableField validatedSerializableField1 = validatedFieldsIterator.next();
		assertFalse("",validatedSerializableField1.getIsValid());
		HashMap<Constraint, ValidationResult> constraintValidationResult =
			 validatedSerializableField1.getValidationAttribute().getConstraintValidationResult();
		// validationResult per constrainedAttribute1.constraint1=NotNull
		ValidationResult validationResult = constraintValidationResult.get(constrainedAttribute1.getAttributeConstraints().get(0));
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		// validationResult per constrainedAttribute1.constraint2=AlphaNum
		validationResult = constraintValidationResult.get(constrainedAttribute1.getAttributeConstraints().get(1));
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		// validationResult per constrainedAttribute1.constraint3=IsValidDecimal
		validationResult = constraintValidationResult.get(constrainedAttribute1.getAttributeConstraints().get(2));
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
		ValidatedSerializableField validatedSerializableField2 = validatedFieldsIterator.next(); 
		assertTrue("",validatedSerializableField2.getIsValid());
			
	}
	
	@Test
	public void notValidRowByFirstFieldThirdConstraint() throws Exception{
		
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		serializableField1.setValue("AA");
		serializableField2.setValue(new Integer("22"));
		fields.add(serializableField1);
		fields.add(serializableField2);
		serializableFieldLinkedSetRow.setFields(fields);
		
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
			  rowSyntacticValidator.validate(serializableFieldLinkedSetRow,constrainedOccurrence);
		
		assertNotNull("",validatedSerializableFieldLinkedSetRow);
		assertFalse("",validatedSerializableFieldLinkedSetRow.getIsValid());
		
		Iterator<ValidatedSerializableField> validatedFieldsIterator = validatedSerializableFieldLinkedSetRow.getFields().iterator();
		ValidatedSerializableField validatedSerializableField1 = validatedFieldsIterator.next();
		assertFalse("",validatedSerializableField1.getIsValid());
		HashMap<Constraint, ValidationResult> constraintValidationResult =
			 validatedSerializableField1.getValidationAttribute().getConstraintValidationResult();
		// validationResult per constrainedAttribute1.constraint1=NotNull
		ValidationResult validationResult = constraintValidationResult.get(constrainedAttribute1.getAttributeConstraints().get(0));
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		// validationResult per constrainedAttribute1.constraint2=AlphaNum
		validationResult = constraintValidationResult.get(constrainedAttribute1.getAttributeConstraints().get(1));
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		// validationResult per constrainedAttribute1.constraint3=IsValidDecimal
		validationResult = constraintValidationResult.get(constrainedAttribute1.getAttributeConstraints().get(2));
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
		ValidatedSerializableField validatedSerializableField2 = validatedFieldsIterator.next(); 
		assertTrue("",validatedSerializableField2.getIsValid());
			
	}
	
	@Test
	public void notValidRowBySecondFieldSecondConstraint() throws Exception{
		
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		serializableField1.setValue("1,23");
		serializableField2.setValue(new Integer("1000"));
		fields.add(serializableField1);
		fields.add(serializableField2);
		serializableFieldLinkedSetRow.setFields(fields);
		
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
			  rowSyntacticValidator.validate(serializableFieldLinkedSetRow,constrainedOccurrence);
		
		assertNotNull("",validatedSerializableFieldLinkedSetRow);
		assertFalse("",validatedSerializableFieldLinkedSetRow.getIsValid());
		
		Iterator<ValidatedSerializableField> validatedFieldsIterator = validatedSerializableFieldLinkedSetRow.getFields().iterator();
		ValidatedSerializableField validatedSerializableField1 = validatedFieldsIterator.next();
		assertTrue("",validatedSerializableField1.getIsValid());
		
		
		ValidatedSerializableField validatedSerializableField2 = validatedFieldsIterator.next();
		assertFalse("",validatedSerializableField2.getIsValid());
		
		HashMap<Constraint, ValidationResult> constraintValidationResult =
			 validatedSerializableField2.getValidationAttribute().getConstraintValidationResult();
		// validationResult per constrainedAttribute2.constraint1=NotNull
		ValidationResult validationResult = constraintValidationResult.get(constrainedAttribute2.getAttributeConstraints().get(0));
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		// validationResult per constrainedAttribute2.constraint2=InRangeInteger
		validationResult = constraintValidationResult.get(constrainedAttribute2.getAttributeConstraints().get(1));
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
			
	}
	
	@Test
	public void notValidRowByFirstFieldThirdConstraintStopFirstNotValid() throws Exception{
		
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		serializableField1.setValue("AA");
		serializableField2.setValue(new Integer("22"));
		fields.add(serializableField1);
		fields.add(serializableField2);
		serializableFieldLinkedSetRow.setFields(fields);
		
		rowSyntacticValidator.setStopWhenFoundFirstNotValid(true);
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
			  rowSyntacticValidator.validate(serializableFieldLinkedSetRow,constrainedOccurrence);
		
		assertNotNull("",validatedSerializableFieldLinkedSetRow);
		assertFalse("",validatedSerializableFieldLinkedSetRow.getIsValid());
		
		Iterator<SerializableField> inputFieldsIterator = fields.iterator();
		int i=0;
		for(ValidatedSerializableField validatedSerializableField : validatedSerializableFieldLinkedSetRow.getFields()){
			i++;
			SerializableField serializableInputField = inputFieldsIterator.next();
			if ( i==1 ){
				// field controllato e non valido
				assertFalse("",validatedSerializableField.getIsValid());
				assertNotNull("",validatedSerializableField.getName());
				assertEquals("",serializableInputField.getName(),validatedSerializableField.getName());
				assertNotNull("",validatedSerializableField.getValue());
				assertEquals("",serializableInputField.getValue(),validatedSerializableField.getValue());
				assertNotNull("",validatedSerializableField.getValidationAttribute());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getName());
				assertEquals("",validatedSerializableField.getValidationAttribute().getName(),
						        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getName());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getType());
				assertEquals("",validatedSerializableField.getValidationAttribute().getType(),
				        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getType());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getConstraintValidationResult());	
			}else{
				// field non controllate
				assertNull("",validatedSerializableField.getIsValid());  // non controllato
				assertNotNull("",validatedSerializableField.getName());
				assertEquals("",serializableInputField.getName(),validatedSerializableField.getName());
				assertNotNull("",validatedSerializableField.getValue());
				assertEquals("",serializableInputField.getValue(),validatedSerializableField.getValue());
				assertNotNull("",validatedSerializableField.getValidationAttribute());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getName());
				assertEquals("",validatedSerializableField.getValidationAttribute().getName(),
						        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getName());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getType());
				assertEquals("",validatedSerializableField.getValidationAttribute().getType(),
				        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getType());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
				assertNull("",validatedSerializableField.getValidationAttribute().getConstraintValidationResult()); // non controllato
			}
			
		}
			
	}
	
	@Test
	public void notValidRowByFirstFieldThirdConstraintStopFirstNotValidWithThirdFieldNoConstraints() throws Exception{
		
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		serializableField1.setValue("AA");
		serializableField2.setValue(new Integer("22"));
		// senza constrazint
		SerializableField serializableField3 = new SerializableField();
		serializableField3.setName("descrizione");
		serializableField3.setValue("aaaaaaa");
		
		fields.add(serializableField1);
		fields.add(serializableField2);
		fields.add(serializableField3);
		serializableFieldLinkedSetRow.setFields(fields);
		
		rowSyntacticValidator.setStopWhenFoundFirstNotValid(true);
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
			  rowSyntacticValidator.validate(serializableFieldLinkedSetRow,constrainedOccurrence);
		
		assertNotNull("",validatedSerializableFieldLinkedSetRow);
		assertFalse("",validatedSerializableFieldLinkedSetRow.getIsValid());
		
		Iterator<SerializableField> inputFieldsIterator = fields.iterator();
		int i=0;
		for(ValidatedSerializableField validatedSerializableField : validatedSerializableFieldLinkedSetRow.getFields()){
			i++;
			SerializableField serializableInputField = inputFieldsIterator.next();
			if ( i==1 ){
				// field controllato e non valido
				assertFalse("",validatedSerializableField.getIsValid());
				assertNotNull("",validatedSerializableField.getName());
				assertEquals("",serializableInputField.getName(),validatedSerializableField.getName());
				assertNotNull("",validatedSerializableField.getValue());
				assertEquals("",serializableInputField.getValue(),validatedSerializableField.getValue());
				assertNotNull("",validatedSerializableField.getValidationAttribute());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getName());
				assertEquals("",validatedSerializableField.getValidationAttribute().getName(),
						        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getName());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getType());
				assertEquals("",validatedSerializableField.getValidationAttribute().getType(),
				        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getType());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getConstraintValidationResult());	
			}else if ( i==2 ){
				// field non controllato ma con constraint
				assertNull("",validatedSerializableField.getIsValid());  // non controllato
				assertNotNull("",validatedSerializableField.getName());
				assertEquals("",serializableInputField.getName(),validatedSerializableField.getName());
				assertNotNull("",validatedSerializableField.getValue());
				assertEquals("",serializableInputField.getValue(),validatedSerializableField.getValue());
				assertNotNull("",validatedSerializableField.getValidationAttribute());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getName());
				assertEquals("",validatedSerializableField.getValidationAttribute().getName(),
						        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getName());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getType());
				assertEquals("",validatedSerializableField.getValidationAttribute().getType(),
				        constrainedOccurrence.getConstrainedAttributes().get(serializableInputField.getName()).getType());
				assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
				assertNull("",validatedSerializableField.getValidationAttribute().getConstraintValidationResult()); // non controllato
			}else{
				// field senza constraint quindi non controllabile
				assertNull("",validatedSerializableField.getIsValid());  // non controllabile
				assertNotNull("",validatedSerializableField.getName());
				assertEquals("",serializableInputField.getName(),validatedSerializableField.getName());
				assertNotNull("",validatedSerializableField.getValue());
				assertEquals("",serializableInputField.getValue(),validatedSerializableField.getValue());
				assertNull("",validatedSerializableField.getValidationAttribute()); // senza constraints
			}
			
		}
			
	}



}
