package it.beesol.frameworks.evtl.core.validator.syntactic.table;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.BaseAttributeConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.NotNullAttributeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.AlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali.IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric.InRangeIntegerOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.entity.BaseEntityConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence.BaseOccurrenceConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.field.BaseFieldSyntacticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.row.BaseRowSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidDecimalAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.InRangeIntegerConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.ObjectType;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.alphanum.AlphaNumType;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric.IntegerType;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

public class BaseTableSyntacticValidatorTest {
	
	private static BaseTableSyntacticValidator tableSyntacticValidator = null;
	private static ConstrainedEntity constrainedEntity = null;
	private static BaseRowSyntacticValidator rowSyntacticValidator = null;
	private static ConstrainedOccurrence constrainedOccurrence = null;
	private static BaseFieldSyntacticValidator fieldSyntacticValidator = null;
	private static ConstrainedAttribute constrainedAttribute1 = null;
	private static ConstrainedAttribute constrainedAttribute2 = null;
	
	private static SerializableField serializableField11 = null;
	private static SerializableField serializableField12 = null;
	private static SerializableField serializableField21 = null;
	private static SerializableField serializableField22 = null;
	private static SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow1 = null;
	private static SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow2 = null;
	private static SerializableRowListTable serializableRowListTable = null;
	
	@BeforeClass
	public static void setUpTest() throws Exception{
		

		
		// (A) descriptor(s)

		// (A.1) attributo "valore_monetario"
		constrainedAttribute1 = new ConstrainedAttribute();
		constrainedAttribute1.setName("valore_monetario");
		constrainedAttribute1.setType(new AlphaNumType());
		List<AttributeConstraint> onTypeConstraints = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints.add(new NotNullConstraint());
		//1:AlphaNumConstraint
		onTypeConstraints.add(new AlphaNumConstraint());
		//2:IsValidDecimalAlphaNumConstraint
		IsValidDecimalAlphaNumConstraint isValidDecimalAlphaNumConstraint = new IsValidDecimalAlphaNumConstraint();
		isValidDecimalAlphaNumConstraint.setIncludeNegative(false);
		onTypeConstraints.add(isValidDecimalAlphaNumConstraint);
		constrainedAttribute1.setAttributeConstraints(onTypeConstraints);
		
		// (A.2) attributo "quantita"
		constrainedAttribute2 = new ConstrainedAttribute();
		constrainedAttribute2.setName("quantita");
		constrainedAttribute2.setType(new IntegerType());
		List<AttributeConstraint> onTypeConstraints2 = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints2.add(new NotNullConstraint());
		//1:InRangeIntegerConstraint
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setMin(0);
		inRangeIntegerConstraint.setMax(999);
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		onTypeConstraints2.add(inRangeIntegerConstraint);
		constrainedAttribute2.setAttributeConstraints(onTypeConstraints2);
		
		// (A.3) occurrence
		constrainedOccurrence = new ConstrainedOccurrence();
		constrainedOccurrence.setName("test_occurrence");

		// set constrainedAttributes
		Hashtable<String,ConstrainedAttribute> constrainedAttributes = new Hashtable<String,ConstrainedAttribute>();
		constrainedAttributes.put(constrainedAttribute1.getName(),constrainedAttribute1);
		constrainedAttributes.put(constrainedAttribute2.getName(),constrainedAttribute2);
		constrainedOccurrence.setConstrainedAttributes(constrainedAttributes);
		// occurrence senza controlli trasversali su row
		constrainedOccurrence.setOccurrenceConstraints(null);
		
		// (A.4) entity
		constrainedEntity = new ConstrainedEntity();
		constrainedEntity.setName("test_entity");
		// set constrainedOccurrence
		constrainedEntity.setConstrainedOccurrence(constrainedOccurrence);
		// occurrence senza controlli trasversali su row
		constrainedEntity.setEntityConstraints(null);

		// (B) validators
		
		// (B.1) istanziazione fieldSyntacticValidator		
		fieldSyntacticValidator = new BaseFieldSyntacticValidator();
		BaseAttributeConstraintCheckerFactory onTypeConstraintCheckerFactory =
			   new BaseAttributeConstraintCheckerFactory();
		HashMap<Type, HashMap<String,ConstraintChecker>> typeConsMap =
			  new HashMap<Type, HashMap<String,ConstraintChecker>>(); 
		// (B.1.a) mappa constraint su AlphaNum
		HashMap<String,ConstraintChecker> consMap = new HashMap<String,ConstraintChecker>();
		consMap.put(AlphaNumConstraint.CONSTRAINT_NAME,
				    new AlphaNumOnTypeConstraintCheckerImpl());
		consMap.put(IsValidDecimalAlphaNumConstraint.CONSTRAINT_NAME,
				    new IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl());
		typeConsMap.put(new AlphaNumType(), consMap);
		// (B.1.b) mappa constraint su Object
		HashMap<String,ConstraintChecker> consMap2 = new HashMap<String,ConstraintChecker>();
		consMap2.put(NotNullConstraint.CONSTRAINT_NAME,
				    new NotNullAttributeConstraintCheckerImpl());
		typeConsMap.put(new ObjectType(), consMap2);
		// (B.1.c) mappa constraint su Integer
		HashMap<String,ConstraintChecker> consMap3 = new HashMap<String,ConstraintChecker>();
		consMap3.put(InRangeIntegerConstraint.CONSTRAINT_NAME,
			    new InRangeIntegerOnTypeConstraintCheckerImpl());
		typeConsMap.put(new IntegerType(), consMap3);
		onTypeConstraintCheckerFactory.setPerTypeConstraintCheckerMap(typeConsMap);
		fieldSyntacticValidator.setAttributeConstraintCheckerFactory(onTypeConstraintCheckerFactory);
		
		// (B.2) istanziazione rowSyntacticValidator
		rowSyntacticValidator = new BaseRowSyntacticValidator();
		rowSyntacticValidator.setFieldSyntacticValidator(fieldSyntacticValidator);
		BaseOccurrenceConstraintCheckerFactory occurrenceConstraintCheckerFactory =
			   new BaseOccurrenceConstraintCheckerFactory<OccurrenceConstraint>();
		rowSyntacticValidator.setOccurrenceConstraintCheckerFactory(occurrenceConstraintCheckerFactory);
		
		// (B.3) istanziazionetableSyntacticValidator
		tableSyntacticValidator = new BaseTableSyntacticValidator();
		tableSyntacticValidator.setRowSyntacticValidator(rowSyntacticValidator);
		BaseEntityConstraintCheckerFactory entityConstraintCheckerFactory =
			   new BaseEntityConstraintCheckerFactory<EntityConstraint>();
		tableSyntacticValidator.setEntityConstraintCheckerFactory(entityConstraintCheckerFactory);
		
		
		// (C) container(s)
		
		serializableField11 = new SerializableField();
		serializableField11.setName("valore_monetario");
		serializableField11.setValue("1,11");
		serializableField12 = new SerializableField();
		serializableField12.setName("quantita");
		serializableField12.setValue(new Integer("11"));
		serializableField21 = new SerializableField();
		serializableField21.setName("valore_monetario");
		serializableField21.setValue("2,22");
		serializableField22 = new SerializableField();
		serializableField22.setName("quantita");
		serializableField22.setValue(new Integer("22"));
		serializableFieldListRow1 = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow1.setName("row1");
		serializableFieldListRow1.setNumber(1);
		serializableFieldListRow2 = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow2.setName("row2");
		serializableFieldListRow2.setNumber(2);
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setName("test_table");
	
	}

	@Test
	public void validAllRows() throws Exception{
		
		serializableField11.setValue("1,11");
		serializableField12.setValue(new Integer("11"));
		serializableField21.setValue("2,22");
		serializableField22.setValue(new Integer("22"));
		
		LinkedHashSet<SerializableField> fields1 = new LinkedHashSet<SerializableField>();
		fields1.add(serializableField11);
		fields1.add(serializableField12);
		serializableFieldListRow1.setFields(fields1);
		LinkedHashSet<SerializableField> fields2 = new LinkedHashSet<SerializableField>();
		fields2.add(serializableField21);
		fields2.add(serializableField22);
		serializableFieldListRow2.setFields(fields2);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow1);
		rows.add(serializableFieldListRow2);
		serializableRowListTable.setRows(rows);
		
		ValidatedSerializableRowListTable validatedSerializableRowListTable = 
			  tableSyntacticValidator.validate(serializableRowListTable,constrainedEntity);
		
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),2);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),0);
			
	}
	
	@Test
	public void notValidFirstRowValidSecondRow() throws Exception{
		
		serializableField11.setValue("AA");
		serializableField12.setValue(new Integer("11"));
		serializableField21.setValue("2,22");
		serializableField22.setValue(new Integer("22"));
		
		LinkedHashSet<SerializableField> fields1 = new LinkedHashSet<SerializableField>();
		serializableField11.setValue("AA");
		fields1.add(serializableField11);
		fields1.add(serializableField12);
		serializableFieldListRow1.setFields(fields1);
		LinkedHashSet<SerializableField> fields2 = new LinkedHashSet<SerializableField>();
		fields2.add(serializableField21);
		fields2.add(serializableField22);
		serializableFieldListRow2.setFields(fields2);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow1);
		rows.add(serializableFieldListRow2);
		serializableRowListTable.setRows(rows);
		
		ValidatedSerializableRowListTable validatedSerializableRowListTable = 
			  tableSyntacticValidator.validate(serializableRowListTable,constrainedEntity);
		
		assertNotNull("",validatedSerializableRowListTable);
		assertFalse("",validatedSerializableRowListTable.getIsValid());
		
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		assertEquals("",new Integer(2),validRows.get(0).getNumber());
		
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> notValidRows = validatedSerializableRowListTable.getNotValidRows();
		assertEquals("",new Integer(1),notValidRows.get(0).getNumber());
			
	}
	
	@Ignore
	public void validManyRows() throws Exception{
		
		serializableField11.setValue("1,11");
		serializableField12.setValue(new Integer("11"));
		serializableField21.setValue("2,22");
		serializableField22.setValue(new Integer("22"));
		
		LinkedHashSet<SerializableField> fields1 = new LinkedHashSet<SerializableField>();
		fields1.add(serializableField11);
		fields1.add(serializableField12);
		serializableFieldListRow1.setFields(fields1);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		int numRows = 10000;
		for (int i=0;i<numRows;i++){
			rows.add(serializableFieldListRow1);	
		}
		serializableRowListTable.setRows(rows);
		
		System.out.println((new Date()).getTime());
		ValidatedSerializableRowListTable validatedSerializableRowListTable = 
			  tableSyntacticValidator.validate(serializableRowListTable,constrainedEntity);
		System.out.println((new Date()).getTime());
		
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),numRows);
	}
	
	
	
}
