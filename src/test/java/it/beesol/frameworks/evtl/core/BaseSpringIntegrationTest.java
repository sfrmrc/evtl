package it.beesol.frameworks.evtl.core;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:/evtl-model-catalogue-descriptor-spring-context.xml",
		                         "classpath*:/evtl-core-catalogue-validator-spring-context.xml"})
public class BaseSpringIntegrationTest {
	
	@Test
	public void check() throws Exception{
		assertTrue("",true);
	}

}
