package it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf.impl;

import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Test;
import static org.junit.Assert.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class BinaryFieldReaderXlsHSSFImplTest {
	
	
	BinaryFieldReaderXlsHSSFImpl binaryFieldReaderXlsHSSF = null;
	
	@Test
	public void testReadNumericCell() throws Exception{
		
		Double inpValue = new Double("1.23");

		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet");
		HSSFRow row = sheet.createRow((short)0);
		HSSFCell cell0 = row.createCell(0);
		cell0.setCellValue(inpValue);
		HSSFCell inpCell = wb.getSheetAt(0).getRow(0).getCell(0);
		
		binaryFieldReaderXlsHSSF = new BinaryFieldReaderXlsHSSFImpl();
		SerializableField field = (SerializableField)binaryFieldReaderXlsHSSF.read(inpCell);
		
		assertNotNull("",field);
		assertNotNull("",field.getValue());
		Object objValue = field.getValue();
		assertTrue("",objValue instanceof Double);
		Double outValue = (Double)objValue;
		assertEquals("",inpValue,outValue);
		
		//FileOutputStream fileOut = new FileOutputStream("workbook.xls");
	    //wb.write(fileOut);
	    //fileOut.close();
		
	}
	
	@Test
	public void testReadBooleanCell() throws Exception{
		
		Boolean inpValue = new Boolean("true");

		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet");
		HSSFRow row = sheet.createRow((short)0);
		HSSFCell cell0 = row.createCell(0);
		cell0.setCellValue(inpValue);
		HSSFCell inpCell = wb.getSheetAt(0).getRow(0).getCell(0);
		
		binaryFieldReaderXlsHSSF = new BinaryFieldReaderXlsHSSFImpl();
		SerializableField field = (SerializableField)binaryFieldReaderXlsHSSF.read(inpCell);
		
		assertNotNull("",field);
		assertNotNull("",field.getValue());
		Object objValue = field.getValue();
		assertTrue("objValue di classe "+objValue.getClass()+" e non "+Boolean.class,objValue instanceof Boolean);
		Boolean outValue = (Boolean)objValue;
		assertEquals("",inpValue,outValue);
	}
	
	@Test
	public void testReadStringCell() throws Exception{
		
		String inpValue = new String("AA");

		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet");
		HSSFRow row = sheet.createRow((short)0);
		HSSFCell cell0 = row.createCell(0);
		cell0.setCellValue(new HSSFRichTextString(inpValue));
		HSSFCell inpCell = wb.getSheetAt(0).getRow(0).getCell(0);
		
		binaryFieldReaderXlsHSSF = new BinaryFieldReaderXlsHSSFImpl();
		SerializableField field = (SerializableField)binaryFieldReaderXlsHSSF.read(inpCell);
		
		assertNotNull("",field);
		assertNotNull("",field.getValue());
		Object objValue = field.getValue();
		assertTrue("objValue di classe "+objValue.getClass()+" e non "+String.class,objValue instanceof String);
		String outValue = (String)objValue;
		assertEquals("",inpValue,outValue);
	}

	@Test
	public void testReadBlankCell() throws Exception{
		
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet");
		HSSFRow row = sheet.createRow((short)0);
		HSSFCell cell0 = row.createCell(0);
		cell0.setCellType(HSSFCell.CELL_TYPE_BLANK);
		HSSFCell inpCell = wb.getSheetAt(0).getRow(0).getCell(0);
		
		binaryFieldReaderXlsHSSF = new BinaryFieldReaderXlsHSSFImpl();
		SerializableField field = (SerializableField)binaryFieldReaderXlsHSSF.read(inpCell);
		
		assertNotNull("",field);
		assertNull("",field.getValue());

	}
	
	
	@Test
	public void testReadDateAsDoubleCell() throws Exception{
		Date inpValue = new Date();
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet");
		HSSFRow row = sheet.createRow((short)0);
		HSSFCell cell0 = row.createCell(0);
		cell0.setCellValue(inpValue);
		HSSFCell inpCell = wb.getSheetAt(0).getRow(0).getCell(0);
		
		binaryFieldReaderXlsHSSF = new BinaryFieldReaderXlsHSSFImpl();
		SerializableField field = (SerializableField)binaryFieldReaderXlsHSSF.read(inpCell);
		
		assertNotNull("",field);
		assertNotNull("",field.getValue());
		Object objValue = field.getValue();
		assertTrue("objValue di classe "+objValue.getClass()+" e non "+Double.class,objValue instanceof Double);
		Double outValue = (Double)objValue;
		System.out.println("outValue="+outValue);
//		System.out.println("outValueInMillisec="+outValue.longValue()*(24*60*60*1000));
//		GregorianCalendar gc = new GregorianCalendar(1900,0,1,0,0,0);
//		System.out.println("gc="+gc.getTimeInMillis());
//		Date outDate = new Date(outValue.longValue()*(24*60*60*1000)+gc.getTimeInMillis());
//		System.out.println("outDate="+outDate);
//		System.out.println("inpValue="+inpValue);
		Double posixUtcValue = (outValue-25569)*(24*60*60*1000);
		Date posixDate = new Date(posixUtcValue.longValue());
		int offset = TimeZone.getTimeZone("CET").getOffset(inpValue.getTime());
		Double posixValue = (outValue-25569)*(24*60*60*1000)-offset;
		Date outDate = new Date(posixValue.longValue());
		System.out.println("posixDate="+posixDate+"-outDate="+outDate);
		assertEquals("",inpValue.getTime(),outDate.getTime());

	}
	
	@Test
	public void testReadeDateAsDateCell2() throws Exception {
		
		Date inpValue = new Date();
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("sheet");
		HSSFRow row = sheet.createRow((short)0);
		HSSFCell cell0 = row.createCell(0);
		HSSFCellStyle cellStyle0 = wb.createCellStyle();
		cellStyle0.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
		cell0.setCellStyle(cellStyle0);
		cell0.setCellValue(inpValue);
		HSSFCell inpCell = wb.getSheetAt(0).getRow(0).getCell(0);
		
		binaryFieldReaderXlsHSSF = new BinaryFieldReaderXlsHSSFImpl();
		SerializableField field = (SerializableField)binaryFieldReaderXlsHSSF.read(inpCell);
		
		assertTrue("",field.getValue() instanceof Date);
		
	}

}
