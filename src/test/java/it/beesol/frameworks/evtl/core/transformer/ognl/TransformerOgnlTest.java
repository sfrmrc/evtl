package it.beesol.frameworks.evtl.core.transformer.ognl;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;

import it.beesol.frameworks.evtl.core.BaseSpringIntegrationTest;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;

import ognl.OgnlException;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:/evtl-model-catalogue-descriptor-spring-context.xml",
		                         "classpath*:/evtl-core-catalogue-validator-spring-context.xml",
		"classpath*:test-evtl-model-transformer-spring-context.xml"})
public class TransformerOgnlTest extends BaseSpringIntegrationTest {
	
	@Autowired
	TransformerOgnl transformerOgnl;
	
	@Autowired
	SerializableFieldLinkedSetRow<SerializableField> serializableFields;
	
	@Test
	public void transformXml2Bean() throws Exception {
		
		BeanTest beanTest = (BeanTest) transformerOgnl.run(serializableFields);
		
		Set<SerializableField> fields = serializableFields.getFields();
		Iterator<SerializableField> iterator = fields.iterator();
		
		SerializableField nomeField = iterator.next();
		assertEquals(beanTest.getFirstName(), nomeField.getValue());
		
		SerializableField cognomeField = iterator.next();
		assertEquals(beanTest.getSecondName(), cognomeField.getValue());
		
	}

	public void setSerializableFields(SerializableFieldLinkedSetRow<SerializableField> serializableFields) {
		this.serializableFields = serializableFields;
	}

	public SerializableFieldLinkedSetRow<SerializableField> getSerializableFields() {
		return serializableFields;
	}

	public void setTransformerOgnl(TransformerOgnl transformerOgnl) {
		this.transformerOgnl = transformerOgnl;
	}

	public TransformerOgnl getTransformerOgnl() {
		return transformerOgnl;
	}

}