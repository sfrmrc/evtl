package it.beesol.frameworks.evtl.core.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import it.beesol.frameworks.evtl.core.BaseSpringIntegrationTest;
import it.beesol.frameworks.evtl.core.validator.syntactic.table.TableSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:test-evtl-refimpl-model-container-spring-context.xml"})
public class ValidatorRefImplSpringIntegrationTest extends BaseSpringIntegrationTest{
	
	
	
	@Autowired
	@Qualifier("preventivoDiSpesaEntity")
	public ConstrainedEntity preventivoDiSpesaEntity = null;
	
	@Autowired
	public TableSyntacticValidator tableSyntacticValidator = null;
	
	
	public void setPreventivoDiSpesaEntity(
			ConstrainedEntity preventivoDiSpesaEntity) {
		preventivoDiSpesaEntity = preventivoDiSpesaEntity;
	}
	
	public void setTableSyntacticValidator(
			TableSyntacticValidator tableSyntacticValidator) {
		tableSyntacticValidator = tableSyntacticValidator;
	}

	

	private static SerializableField buildField(String name,Serializable value){
		
		SerializableField serializableField = new SerializableField();
		serializableField.setName(name);
		serializableField.setValue(value);
		return serializableField;
	}
		
	private LinkedHashSet<SerializableField> buildValidRow(){
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		
        fields.add(buildField("edificio","E1"));
		
		fields.add(buildField("servizio","S1"));
		
		fields.add(buildField("tipo","C"));
		
		fields.add(buildField("um","MQ"));
		
		fields.add(buildField("qta",new Double("1.1")));
		
		fields.add(buildField("interventi",new Integer("234")));
		
		fields.add(buildField("pu",new Double("2.2")));
		
		fields.add(buildField("importoAnnuo",new Double("88.88")));
		
		fields.add(buildField("importoTotale",new Double("99.99")));
		
		fields.add(buildField("dataInizio",new Date()));
		
		fields.add(buildField("dataInizio",new Date()));
		
		fields.add(buildField("mesiErogazione",new Integer("12")));
		
		fields.add(buildField("numeroPrestazioniAggiuntive",new Double("44.44")));
		
		fields.add(buildField("riduzioneAttivitaCanone",new Double("55.55")));
		
		fields.add(buildField("percentualeSconto",new Double("66.66")));
		
		fields.add(buildField("servizioAttivo","X"));
		
		fields.add(buildField("reperibilita","S"));
		
		fields.add(buildField("areaNetta",new Double("22.22")));
		
		fields.add(buildField("areaLorda",new Double("11.11")));
		
		return fields;
	}
	
	private LinkedHashSet<SerializableField> changeFieldValueInRow(String name,Serializable value,LinkedHashSet<SerializableField> fields){
		Iterator<SerializableField> fieldIterator = fields.iterator();
		LinkedHashSet<SerializableField> outFields = new LinkedHashSet<SerializableField>();
		while(fieldIterator.hasNext()){
			SerializableField field = fieldIterator.next();
			if ( field.getName().equals(name)){
				field.setValue(value);
			}
			outFields.add(field);
		}
		return outFields;
		
	}
	
	
	
	private SerializableRowListTable buildTableTestOneRowValid() throws Exception{
		
		SerializableRowListTable serializableRowListTable = null;
		
		SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow.setName("row1");
		serializableFieldListRow.setNumber(1);
		serializableFieldListRow.setFields(this.buildValidRow());
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow);
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setRows(rows);
		return serializableRowListTable;

	}

	@Ignore
	public void oneRowValid() throws Exception{
		ValidatedSerializableRowListTable validatedSerializableRowListTable =
			 tableSyntacticValidator.validate(this.buildTableTestOneRowValid(),preventivoDiSpesaEntity);
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),1);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),0);
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
			assertNotNull("",validatedRow.getFields());
			assertTrue("",validatedRow.getIsValid());
			Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
			while(fieldsIterator.hasNext()){
				ValidatedSerializableField validatedField = fieldsIterator.next();
				assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
				assertTrue("",validatedField.getIsValid());
			}
		}
	}
	
    private SerializableRowListTable buildTableTestOneRowNotValidTipo() throws Exception{
		SerializableRowListTable serializableRowListTable = null;
		SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow.setName("row1");
		serializableFieldListRow.setNumber(1);
		LinkedHashSet<SerializableField> fields = this.changeFieldValueInRow("tipo","X",this.buildValidRow());
        serializableFieldListRow.setFields(fields);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow);
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setRows(rows);
		return serializableRowListTable;
	}

	@Ignore
	public void oneRowNotValidTipo() throws Exception{
		ValidatedSerializableRowListTable validatedSerializableRowListTable =
			 tableSyntacticValidator.validate(this.buildTableTestOneRowNotValidTipo(),preventivoDiSpesaEntity);
		assertNotNull("",validatedSerializableRowListTable);
		assertFalse("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),0);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
			assertNotNull("",validatedRow.getFields());
			assertFalse("",validatedRow.getIsValid());
			Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
			while(fieldsIterator.hasNext()){
				ValidatedSerializableField validatedField = fieldsIterator.next();
				assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
				if (!"tipo".equals(validatedField.getName())){
					assertTrue("field "+validatedField.getName()+" deve essere valido",validatedField.getIsValid());	
				}else{
					assertFalse("field "+validatedField.getName()+" deve essere non valido",validatedField.getIsValid());
				}
				
			}
		}
	}

    private SerializableRowListTable buildTableTestOneRowNotValidMesiErogazione() throws Exception{
		SerializableRowListTable serializableRowListTable = null;
		SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow.setName("row1");
		serializableFieldListRow.setNumber(1);
		LinkedHashSet<SerializableField> fields = this.changeFieldValueInRow("mesiErogazione","1000",this.buildValidRow());
        serializableFieldListRow.setFields(fields);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow);
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setRows(rows);
		return serializableRowListTable;
	}

	@Ignore
	// TODO : ripristinare quando ripristinati controlli (numerici non interi su mesiErogazione)
	public void oneRowNotValidMesiErogazione() throws Exception{
		ValidatedSerializableRowListTable validatedSerializableRowListTable =
			 tableSyntacticValidator.validate(this.buildTableTestOneRowNotValidMesiErogazione(),preventivoDiSpesaEntity);
		assertNotNull("",validatedSerializableRowListTable);
		assertFalse("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),0);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
			assertNotNull("",validatedRow.getFields());
			assertFalse("",validatedRow.getIsValid());
			Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
			while(fieldsIterator.hasNext()){
				ValidatedSerializableField validatedField = fieldsIterator.next();
				assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
				if (!"mesiErogazione".equals(validatedField.getName())){
					assertTrue("field "+validatedField.getName()+" deve essere valido",validatedField.getIsValid());	
				}else{
					assertFalse("field "+validatedField.getName()+" deve essere non valido",validatedField.getIsValid());
				}
				
			}
		}
	}


    private SerializableRowListTable buildTableTestOneRowNotValidDataInizioNotDate() throws Exception{
		SerializableRowListTable serializableRowListTable = null;
		SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow.setName("row1");
		serializableFieldListRow.setNumber(1);
		LinkedHashSet<SerializableField> fields = this.changeFieldValueInRow("dataInizio","1000",this.buildValidRow());
        serializableFieldListRow.setFields(fields);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow);
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setRows(rows);
		return serializableRowListTable;
	}

	@Ignore
	public void oneRowNotValidDataInizioNotDate() throws Exception{
		ValidatedSerializableRowListTable validatedSerializableRowListTable =
			 tableSyntacticValidator.validate(this.buildTableTestOneRowNotValidDataInizioNotDate(),preventivoDiSpesaEntity);
		assertNotNull("",validatedSerializableRowListTable);
		assertFalse("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),0);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
			assertNotNull("",validatedRow.getFields());
			assertFalse("",validatedRow.getIsValid());
			Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
			while(fieldsIterator.hasNext()){
				ValidatedSerializableField validatedField = fieldsIterator.next();
				assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
				if (!"dataInizio".equals(validatedField.getName())){
					assertTrue("field "+validatedField.getName()+" deve essere valido",validatedField.getIsValid());	
				}else{
					assertFalse("field "+validatedField.getName()+" deve essere non valido",validatedField.getIsValid());
				}
				
			}
		}
	}
	
	 private SerializableRowListTable buildTableTestOneRowNotValidEdificioNull() throws Exception{
			SerializableRowListTable serializableRowListTable = null;
			SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
			serializableFieldListRow.setName("row1");
			serializableFieldListRow.setNumber(1);
			LinkedHashSet<SerializableField> fields = this.changeFieldValueInRow("edificio",null,this.buildValidRow());
	        serializableFieldListRow.setFields(fields);
			List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
			rows.add(serializableFieldListRow);
			serializableRowListTable = new SerializableRowListTable();
			serializableRowListTable.setRows(rows);
			return serializableRowListTable;
		}

		@Ignore
		public void oneRowNotValidDataEdificioNull() throws Exception{
			ValidatedSerializableRowListTable validatedSerializableRowListTable =
				 tableSyntacticValidator.validate(this.buildTableTestOneRowNotValidEdificioNull(),preventivoDiSpesaEntity);
			assertNotNull("",validatedSerializableRowListTable);
			assertFalse("",validatedSerializableRowListTable.getIsValid());
			assertNotNull("",validatedSerializableRowListTable.getValidRows());
			assertEquals("",validatedSerializableRowListTable.getValidRows().size(),0);
			assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
			assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),1);
			List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
			for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
				assertNotNull("",validatedRow.getFields());
				assertFalse("",validatedRow.getIsValid());
				Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
				while(fieldsIterator.hasNext()){
					ValidatedSerializableField validatedField = fieldsIterator.next();
					assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
					if (!"dataInizio".equals(validatedField.getName())){
						assertTrue("field "+validatedField.getName()+" deve essere valido",validatedField.getIsValid());	
					}else{
						assertFalse("field "+validatedField.getName()+" deve essere non valido",validatedField.getIsValid());
					}
					
				}
			}
		}

		private SerializableRowListTable buildTableTestOneRowValidInterventiNull() throws Exception{
			SerializableRowListTable serializableRowListTable = null;
			SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
			serializableFieldListRow.setName("row1");
			serializableFieldListRow.setNumber(1);
			LinkedHashSet<SerializableField> fields = this.changeFieldValueInRow("interventi",null,this.buildValidRow());
	        serializableFieldListRow.setFields(fields);
			List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
			rows.add(serializableFieldListRow);
			serializableRowListTable = new SerializableRowListTable();
			serializableRowListTable.setRows(rows);
			return serializableRowListTable;
		}

		@Test
		public void oneRowNotValidOneRowValidInterventiNull() throws Exception{
			ValidatedSerializableRowListTable validatedSerializableRowListTable =
				 tableSyntacticValidator.validate(this.buildTableTestOneRowValidInterventiNull(),preventivoDiSpesaEntity);
			assertNotNull("",validatedSerializableRowListTable);
			assertTrue("",validatedSerializableRowListTable.getIsValid());
			assertNotNull("",validatedSerializableRowListTable.getValidRows());
			assertEquals("",validatedSerializableRowListTable.getValidRows().size(),1);
			assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
			assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),0);
			List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
			for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
				assertNotNull("",validatedRow.getFields());
				assertTrue("",validatedRow.getIsValid());
				Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
				while(fieldsIterator.hasNext()){
					ValidatedSerializableField validatedField = fieldsIterator.next();
					if (!"interventi".equals(validatedField.getName())){
						assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
						assertTrue("field "+validatedField.getName()+" deve essere valido",validatedField.getIsValid());	
					}else{
						assertNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
						assertTrue("field "+validatedField.getName()+" deve essere non valido",validatedField.getIsValid());
					}
					
				}
			}
		}


	private SerializableRowListTable buildTableTestOneThousandRowValid(int numRows) throws Exception{
		
		SerializableRowListTable serializableRowListTable = null;
		
		SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow.setName("row1");
		serializableFieldListRow.setNumber(1);
		serializableFieldListRow.setFields(this.buildValidRow());
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		for (int i=0;i<numRows;i++){
			rows.add(serializableFieldListRow);	
		}
		
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setRows(rows);
		return serializableRowListTable;

	}

	@Ignore
	public void oneThousandRowValid() throws Exception{
		ValidatedSerializableRowListTable validatedSerializableRowListTable =
			 tableSyntacticValidator.validate(this.buildTableTestOneThousandRowValid(1000),preventivoDiSpesaEntity);
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),1000);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),0);
	}
}
