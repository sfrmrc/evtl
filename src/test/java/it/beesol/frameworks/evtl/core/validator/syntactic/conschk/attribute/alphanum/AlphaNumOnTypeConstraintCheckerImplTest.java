package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.AlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import org.junit.Test;
import static org.junit.Assert.*;

public class AlphaNumOnTypeConstraintCheckerImplTest {
	
	@Test
	public void validInputTypeIsLikeExpected() throws Exception{
		AlphaNumOnTypeConstraintCheckerImpl constraintChecker= new AlphaNumOnTypeConstraintCheckerImpl();
		AlphaNumConstraint alphaNumConstraint = new AlphaNumConstraint();
		ValidationResult validationResult = constraintChecker.validate(new String("A"),alphaNumConstraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void notValidInputTypeIsNotExpected() throws Exception{
		AlphaNumOnTypeConstraintCheckerImpl constraintChecker= new AlphaNumOnTypeConstraintCheckerImpl();
		AlphaNumConstraint alphaNumConstraint = new AlphaNumConstraint();
		ValidationResult validationResult = constraintChecker.validate(new Integer("1"),alphaNumConstraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidInputIsNull() throws Exception{
		AlphaNumOnTypeConstraintCheckerImpl constraintChecker= new AlphaNumOnTypeConstraintCheckerImpl();
		AlphaNumConstraint alphaNumConstraint = new AlphaNumConstraint();
		ValidationResult validationResult = constraintChecker.validate(null,alphaNumConstraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}

}
