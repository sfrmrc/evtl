package it.beesol.frameworks.evtl.core.extractor;

import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import it.beesol.frameworks.evtl.core.BaseSpringIntegrationTest;
import it.beesol.frameworks.evtl.core.extractor.bin.BinaryWorkBookExtractor;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:test-evtl-refimpl-model-container-spring-context.xml",
		                         "classpath*:test-evtl-refimpl-extractor-spring-context.xml"})
public class ExtractorRefImplSpringIntegrationTest extends BaseSpringIntegrationTest{
	
	@Autowired
	@Qualifier("preventivoDiSpesaBinXlsExtractor")
	BinaryWorkBookExtractor preventivoDiSpesaBinXlsExtractor = null;
	
	public void setPreventivoDiSpesaBinXlsExtractor(
			BinaryWorkBookExtractor preventivoDiSpesaBinXlsExtractor) {
		this.preventivoDiSpesaBinXlsExtractor = preventivoDiSpesaBinXlsExtractor;
	}
	
	@Autowired
	@Qualifier("preventivoDiSpesaConstrainedOccurrence")
	ConstrainedOccurrence preventivoDiSpesaConstrainedOccurrence = null;
	
	public void setPreventivoDiSpesaConstrainedOccurrence(
			ConstrainedOccurrence preventivoDiSpesaConstrainedOccurrence) {
		this.preventivoDiSpesaConstrainedOccurrence = preventivoDiSpesaConstrainedOccurrence;
	}




	@Test
	public void test() throws Exception{
		
		String pathTestXlsFile = "refimpl_binaryWorkBookExtractorXlsHSSF_testFile001.xls";
		InputStream testXlsIS = getClass().getResourceAsStream(pathTestXlsFile);
		
		SerializableSheetSetBook book = preventivoDiSpesaBinXlsExtractor.run(testXlsIS);
		assertNotNull("",book);
		System.out.println("book="+book);
		
		// verifica book
		assertNotNull("",book);

		// verifica sheets
		Set<SerializableTableSetSheet> sheets = book.getSheet();
		assertNotNull("",sheets);
		assertEquals("",1,sheets.size());

		// verifica tablesets
		SerializableTableSetSheet tableSet = sheets.iterator().next();
		assertNotNull("",tableSet);

		// verifica tablesets		
		Set<SerializableRowListTable> tables = tableSet.getTables();
		assertNotNull("",tables);
		assertEquals("",1,tables.size());
		
		// verifica tables
		SerializableRowListTable table = tables.iterator().next();
		assertNotNull("",table);
		
		// verifica rows
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = table.getRows();
		assertNotNull("",rows);
		assertEquals("",1,rows.size());

		// verifica row one by one
		for(int i=0;i<rows.size();i++){
			SerializableFieldLinkedSetRow<SerializableField> row = rows.get(i);
			// System.out.println(row.toString());
			LinkedHashSet<SerializableField> fields = row.getFields();
			assertNotNull("",fields);
			assertEquals("",preventivoDiSpesaConstrainedOccurrence.getAttributes().size(),fields.size());
			Iterator<SerializableField> fieldsIterator = fields.iterator();
			Iterator<ConstrainedAttribute> attributesIterator =  preventivoDiSpesaConstrainedOccurrence.getAttributes().iterator();
			// verifica field one by one
			while(fieldsIterator.hasNext()){
				SerializableField field = fieldsIterator.next();
				ConstrainedAttribute constrainedAttribute = attributesIterator.next();
				assertEquals("",constrainedAttribute.getName(),field.getName());
				//System.out.println(constrainedAttribute.getName()+"-"+field.getName());
			}
		}

		
	}

}
