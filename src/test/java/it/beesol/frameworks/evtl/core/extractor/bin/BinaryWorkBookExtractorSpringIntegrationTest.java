package it.beesol.frameworks.evtl.core.extractor.bin;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import it.beesol.frameworks.evtl.core.BaseSpringIntegrationTest;
import it.beesol.frameworks.evtl.core.extractor.bin.BinaryWorkBookExtractor;
import it.beesol.frameworks.evtl.core.extractor.bin.xls.hssf.BinaryWorkBookExtractorXlsHSSF;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:test-evtl-model-extractor-spring-context.xml"})
public class BinaryWorkBookExtractorSpringIntegrationTest extends BaseSpringIntegrationTest{

	@Autowired
	@Qualifier("binaryXlsTestExtractor")
	BinaryWorkBookExtractor extractor;
	
	public void setExtractor(BinaryWorkBookExtractor extractor) {
		this.extractor = extractor;
	}

	@Autowired
	@Qualifier("binaryExtractorRowTest")
	ConstrainedOccurrence binaryExtractorRowTest = null;
	
	public void setBinaryExtractorRowTest(
			ConstrainedOccurrence binaryExtractorRowTest) {
		this.binaryExtractorRowTest = binaryExtractorRowTest;
	}

	@Test
	public void extract() throws Exception{
		
		String pathTestXlsFile = "binaryWorkBookExtractorXlsHSSF_testFile001.xls";
		InputStream testXlsIS = getClass().getResourceAsStream(pathTestXlsFile);
		
		SerializableSheetSetBook book = extractor.run(testXlsIS);
		
		// verifica book
		assertNotNull("",book);
		
		// verifica sheets
		Set<SerializableTableSetSheet> sheets = book.getSheet();
		assertNotNull("",sheets);
		assertEquals("",1,sheets.size());

		// verifica tablesets
		SerializableTableSetSheet tableSet = sheets.iterator().next();
		assertNotNull("",tableSet);

		// verifica tablesets		
		Set<SerializableRowListTable> tables = tableSet.getTables();
		assertNotNull("",tables);
		assertEquals("",1,tables.size());
		
		// verifica tables
		SerializableRowListTable table = tables.iterator().next();
		assertNotNull("",table);
		
		// verifica rows
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = table.getRows();
		assertNotNull("",rows);
		assertEquals("",3,rows.size());
		// System.out.println(book.toString());
		// verifica row one by one
		for(int i=0;i<rows.size();i++){
			SerializableFieldLinkedSetRow<SerializableField> row = rows.get(i);
			// System.out.println(row.toString());
			LinkedHashSet<SerializableField> fields = row.getFields();
			assertNotNull("",fields);
			assertEquals("",5,fields.size());
			Iterator<SerializableField> fieldsIterator = fields.iterator();
			Iterator<ConstrainedAttribute> attributesIterator =  binaryExtractorRowTest.getAttributes().iterator();
			// verifica field one by one
			while(fieldsIterator.hasNext()){
				SerializableField field = fieldsIterator.next();
				ConstrainedAttribute constrainedAttribute = attributesIterator.next();
				assertEquals("",constrainedAttribute.getName(),field.getName());
				//System.out.println(constrainedAttribute.getName()+"-"+field.getName());
			}
		}
			
		
	}
	
	
	
	@Test
	public void tenThousandRows() throws Exception{
		
		String pathTestXlsFile = "binaryWorkBookExtractorXlsHSSF_testFile002_10000Rows.xls";
		InputStream testXlsIS = getClass().getResourceAsStream(pathTestXlsFile);
		
		Book book = extractor.run(testXlsIS);
		
		// verifica book
		assertNotNull("",book);
		assertTrue("",book instanceof SerializableSheetSetBook);
		SerializableSheetSetBook outputBook  = (SerializableSheetSetBook)book;

		// verifica sheets
		Set<SerializableTableSetSheet> sheets = outputBook.getSheet();
		assertNotNull("",sheets);
		assertEquals("",1,sheets.size());

		// verifica tablesets
		SerializableTableSetSheet tableSet = sheets.iterator().next();
		assertNotNull("",tableSet);

		// verifica tablesets		
		Set<SerializableRowListTable> tables = tableSet.getTables();
		assertNotNull("",tables);
		assertEquals("",1,tables.size());
		
		// verifica tables
		SerializableRowListTable table = tables.iterator().next();
		assertNotNull("",table);
		
		// verifica rows
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = table.getRows();
		assertNotNull("",rows);
		assertEquals("",10000,rows.size());
		System.out.println("rows="+rows.size());

	}
	
}
