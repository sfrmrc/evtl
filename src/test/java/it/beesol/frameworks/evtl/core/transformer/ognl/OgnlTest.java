package it.beesol.frameworks.evtl.core.transformer.ognl;

import static org.junit.Assert.*;

import java.awt.Dimension;
import java.lang.reflect.Field;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;

import org.junit.Test;

public class OgnlTest {

	@Test
	public void DimensionTest() throws OgnlException {

		Dimension d = new Dimension(3, 2);

		String expressionString = "width";
		Object expr = Ognl.parseExpression(expressionString);

		OgnlContext ctx = new OgnlContext();
		Object value = Ognl.getValue(expr, ctx, d);

		System.out.println("Value: " + value);

	}

	@Test
	public void BeanTestPartedTest() throws Exception {

		BeanTestParted root = new BeanTestParted();

		Object value = new String("firstName_value");
		String expression = "bean.firstName";
		
		setAttributeInChain(expression, root, value);
		
		assertEquals(root.getBean().getFirstName(), value);

	}
	
	private String setAttributeInChain(String expression, Object root, Object value) throws OgnlException, InstantiationException, IllegalAccessException {
		
		if(Ognl.isSimpleProperty(expression)) {
			
			Ognl.setValue(expression, root, value);
			return expression;
			
		}else{
			
			String[] chainString = expression.split("\\.");
			String newRootExpression = chainString[0];
			
			Field[] rootFields = root.getClass().getDeclaredFields();
			Object newRoot = null;
			for(int i=0; i<rootFields.length; i++) {
				if(rootFields[i].getName().equals(newRootExpression)) {
					newRoot = rootFields[i].getType().newInstance();
					break;
				}
			}
			
			Ognl.setValue(newRootExpression, root, newRoot);
			
			
			String newExpression = expression.replace(newRootExpression+".", "");
			return setAttributeInChain(newExpression, newRoot, value);
			
		}
		
	}

}