package it.beesol.frameworks.evtl.core.validator.syntactic.table;

import it.beesol.frameworks.evtl.core.BaseSpringIntegrationTest;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidDecimalAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.InRangeIntegerConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.alphanum.AlphaNumType;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric.IntegerType;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
public class BaseTableSyntacticValidatorSpringIntegrationTest extends BaseSpringIntegrationTest{
	
	private static BaseTableSyntacticValidator tableSyntacticValidator = null;
	
	@Autowired
	public void setTableSyntacticValidator(
			BaseTableSyntacticValidator tableSyntacticValidator) {
		BaseTableSyntacticValidatorSpringIntegrationTest.tableSyntacticValidator = tableSyntacticValidator;
	}

	private static ConstrainedEntity constrainedEntity = null;
	private static ConstrainedOccurrence constrainedOccurrence = null;
	private static ConstrainedAttribute constrainedAttribute1 = null;
	private static ConstrainedAttribute constrainedAttribute2 = null;
	
	private static SerializableField serializableField11 = null;
	private static SerializableField serializableField12 = null;
	private static SerializableField serializableField21 = null;
	private static SerializableField serializableField22 = null;
	private static SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow1 = null;
	private static SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow2 = null;
	private static SerializableRowListTable serializableRowListTable = null;
	
	@BeforeClass
	public static void setUpTest() throws Exception{
				
		// (A) descriptor(s)

		// (A.1) attributo "valore_monetario"
		constrainedAttribute1 = new ConstrainedAttribute();
		constrainedAttribute1.setName("valore_monetario");
		constrainedAttribute1.setType(new AlphaNumType());
		List<AttributeConstraint> onTypeConstraints = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints.add(new NotNullConstraint());
		//1:AlphaNumConstraint
		onTypeConstraints.add(new AlphaNumConstraint());
		//2:IsValidDecimalAlphaNumConstraint
		IsValidDecimalAlphaNumConstraint isValidDecimalAlphaNumConstraint = new IsValidDecimalAlphaNumConstraint();
		isValidDecimalAlphaNumConstraint.setIncludeNegative(false);
		onTypeConstraints.add(isValidDecimalAlphaNumConstraint);
		constrainedAttribute1.setAttributeConstraints(onTypeConstraints);
		
		// (A.2) attributo "quantita"
		constrainedAttribute2 = new ConstrainedAttribute();
		constrainedAttribute2.setName("quantita");
		constrainedAttribute2.setType(new IntegerType());
		List<AttributeConstraint> onTypeConstraints2 = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints2.add(new NotNullConstraint());
		//1:InRangeIntegerConstraint
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setMin(0);
		inRangeIntegerConstraint.setMax(999);
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		onTypeConstraints2.add(inRangeIntegerConstraint);
		constrainedAttribute2.setAttributeConstraints(onTypeConstraints2);
		
		// (A.3) occurrence
		constrainedOccurrence = new ConstrainedOccurrence();
		constrainedOccurrence.setName("test_occurrence");

		// set constrainedAttributes
		Hashtable<String,ConstrainedAttribute> constrainedAttributes = new Hashtable<String,ConstrainedAttribute>();
		constrainedAttributes.put(constrainedAttribute1.getName(),constrainedAttribute1);
		constrainedAttributes.put(constrainedAttribute2.getName(),constrainedAttribute2);
		constrainedOccurrence.setConstrainedAttributes(constrainedAttributes);
		// occurrence senza controlli trasversali su row
		constrainedOccurrence.setOccurrenceConstraints(null);
		
		// (A.4) entity
		constrainedEntity = new ConstrainedEntity();
		constrainedEntity.setName("test_entity");
		// set constrainedOccurrence
		constrainedEntity.setConstrainedOccurrence(constrainedOccurrence);
		// occurrence senza controlli trasversali su row
		constrainedEntity.setEntityConstraints(null);

		// (B) validators
		// configurati in spring
		
		
		// (C) container(s)
		
		serializableField11 = new SerializableField();
		serializableField11.setName("valore_monetario");
		serializableField11.setValue("1,11");
		serializableField12 = new SerializableField();
		serializableField12.setName("quantita");
		serializableField12.setValue(new Integer("11"));
		serializableField21 = new SerializableField();
		serializableField21.setName("valore_monetario");
		serializableField21.setValue("2,22");
		serializableField22 = new SerializableField();
		serializableField22.setName("quantita");
		serializableField22.setValue(new Integer("22"));
		serializableFieldListRow1 = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow1.setName("row1");
		serializableFieldListRow1.setNumber(1);
		serializableFieldListRow2 = new SerializableFieldLinkedSetRow<SerializableField>();
		serializableFieldListRow2.setName("row2");
		serializableFieldListRow2.setNumber(2);
		serializableRowListTable = new SerializableRowListTable();
		serializableRowListTable.setName("test_table");
	
	}

	@Test
	public void validAllRows() throws Exception{
		
		serializableField11.setValue("1,11");
		serializableField12.setValue(new Integer("11"));
		serializableField21.setValue("2,22");
		serializableField22.setValue(new Integer("22"));
		
		LinkedHashSet<SerializableField> fields1 = new LinkedHashSet<SerializableField>();
		fields1.add(serializableField11);
		fields1.add(serializableField12);
		serializableFieldListRow1.setFields(fields1);
		LinkedHashSet<SerializableField> fields2 = new LinkedHashSet<SerializableField>();
		fields2.add(serializableField21);
		fields2.add(serializableField22);
		serializableFieldListRow2.setFields(fields2);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow1);
		rows.add(serializableFieldListRow2);
		serializableRowListTable.setRows(rows);
		
		ValidatedSerializableRowListTable validatedSerializableRowListTable = 
			  tableSyntacticValidator.validate(serializableRowListTable,constrainedEntity);
		
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),2);
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),0);
			
	}
	
	@Test
	public void notValidFirstRowValidSecondRow() throws Exception{
		
		serializableField11.setValue("AA");
		serializableField12.setValue(new Integer("11"));
		serializableField21.setValue("2,22");
		serializableField22.setValue(new Integer("22"));
		
		LinkedHashSet<SerializableField> fields1 = new LinkedHashSet<SerializableField>();
		serializableField11.setValue("AA");
		fields1.add(serializableField11);
		fields1.add(serializableField12);
		serializableFieldListRow1.setFields(fields1);
		LinkedHashSet<SerializableField> fields2 = new LinkedHashSet<SerializableField>();
		fields2.add(serializableField21);
		fields2.add(serializableField22);
		serializableFieldListRow2.setFields(fields2);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		rows.add(serializableFieldListRow1);
		rows.add(serializableFieldListRow2);
		serializableRowListTable.setRows(rows);
		
		ValidatedSerializableRowListTable validatedSerializableRowListTable = 
			  tableSyntacticValidator.validate(serializableRowListTable,constrainedEntity);
		
		assertNotNull("",validatedSerializableRowListTable);
		assertFalse("",validatedSerializableRowListTable.getIsValid());
		
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		assertEquals("",new Integer(2),validRows.get(0).getNumber());
		
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),1);
		List<ValidatedSerializableFieldLinkedSetRow> notValidRows = validatedSerializableRowListTable.getNotValidRows();
		assertEquals("",new Integer(1),notValidRows.get(0).getNumber());
			
	}
	
	@Ignore
	public void validManyRows() throws Exception{
		
		serializableField11.setValue("1,11");
		serializableField12.setValue(new Integer("11"));
		serializableField21.setValue("2,22");
		serializableField22.setValue(new Integer("22"));
		
		LinkedHashSet<SerializableField> fields1 = new LinkedHashSet<SerializableField>();
		fields1.add(serializableField11);
		fields1.add(serializableField12);
		serializableFieldListRow1.setFields(fields1);
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		int numRows = 10000;
		for (int i=0;i<numRows;i++){
			rows.add(serializableFieldListRow1);	
		}
		serializableRowListTable.setRows(rows);
		
		System.out.println((new Date()).getTime());
		ValidatedSerializableRowListTable validatedSerializableRowListTable = 
			  tableSyntacticValidator.validate(serializableRowListTable,constrainedEntity);
		System.out.println((new Date()).getTime());
		
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),numRows);
	}
	
	
	
}
