package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali;

import java.util.Locale;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali.IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidIntegerAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImplTest {
	
	private static IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl constraintChecker = null;
	private static IsValidIntegerAlphaNumConstraint constraint = null;
	
	@Before
	public void setUpAllTest() throws Exception{
		constraintChecker =
			new IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl();
		constraint = new IsValidIntegerAlphaNumConstraint();
		constraint.setLocale((new Locale("it","IT")));
	}
	
	@Test
	public void notValidNullInput() throws Exception{
		String input = null;
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidBlankInput() throws Exception{
		String input = " ";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidAlphabeticInput() throws Exception{
		String input = "Aa";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void validNumberInput() throws Exception{
		String input = "1";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNumberWithThousandSeparatorInput1() throws Exception{
		String input = "1.123";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNumberWithThousandSeparatorInput2() throws Exception{
		String input = "98.765.432";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	
	@Test
	public void validNumberWithThousandSeparatorInput3() throws Exception{
		String input = "9.999";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void notValidDecimalInput1() throws Exception{
		String input = "9,999";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidDecimalInput2() throws Exception{
		String input = "0,77";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidDecimalInput3() throws Exception{
		String input = ",77";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
		
	@Test
	public void validZeroInput() throws Exception{
		String input = "0";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNumberWithThousandSeparatorInputEnglishLocale() throws Exception{
		String input = "9,999";
		constraint.setLocale(new Locale("en","US"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void notValidDecimalInputEnglishLocale() throws Exception{
		String input = "9.999";
		constraint.setLocale(new Locale("en","US"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void validNegativeInput1() throws Exception{
		String input = "-1";
		constraint.setLocale(new Locale("it","IT"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNegativeInput2() throws Exception{
		String input = "-23.456.789";
		constraint.setLocale(new Locale("it","IT"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void notValidNegativeDecimalInput() throws Exception{
		String input = "-0,12345678";
		constraint.setLocale(new Locale("it","IT"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	
	
	

}
