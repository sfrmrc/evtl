package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.RangeLenghtAlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric.InRangeIntegerOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.RangeLenghtAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.InRangeIntegerConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import org.junit.Test;
import static org.junit.Assert.*;

public class InRangeIntegerOnTypeConstraintCheckerImplTest {
	
	@Test
	public void boundaryIncludedFalseValueOutOfRangeStrictlyBelow() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new Integer("2"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
	
	@Test
	public void boundaryIncludedFalseValueOutOfRangeStrictlyAbove() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new Integer("110"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
	
	
	@Test
	public void boundaryIncludedTrueValueInOfRangeStrictly() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new Integer("50"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		
	}
	
	@Test
	public void boundaryIncludedTrueValueInRangeEqualsMin() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new Integer("10"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		
	}
         
	
	@Test
	public void boundaryIncludedTrueValueInRangeEqualsMax() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(true);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();

		
		ValidationResult validationResult = constraintChecker.validate(new Integer("100"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		
	}


	@Test
	public void boundaryNotIncludedFalseValueOutRangeEqualsMin() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(false);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new Integer("10"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
         
	
	@Test
	public void boundaryNotIncludedFalseValueOutRangeEqualsMax() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(false);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new Integer("100"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
	
	
	@Test
	public void falseValueNotInteger() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(false);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("100"),inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
 
	@Test
	public void falseValueNull() throws Exception{
		
		InRangeIntegerConstraint inRangeIntegerConstraint = new InRangeIntegerConstraint();
		inRangeIntegerConstraint.setBoundaryIncluded(false);
		inRangeIntegerConstraint.setMin(10);
		inRangeIntegerConstraint.setMax(100);
		
		InRangeIntegerOnTypeConstraintCheckerImpl constraintChecker = new InRangeIntegerOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(null,inRangeIntegerConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
	

}
