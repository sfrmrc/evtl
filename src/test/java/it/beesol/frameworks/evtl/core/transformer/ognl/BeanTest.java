package it.beesol.frameworks.evtl.core.transformer.ognl;

public class BeanTest {

	private String firstName;
	private String secondName;

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getSecondName() {
		return secondName;
	}
	
}