package it.beesol.frameworks.evtl.core.validator.syntactic.field;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.BaseAttributeConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.NotNullAttributeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.AlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali.IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl;
import it.beesol.frameworks.evtl.core.validator.syntactic.field.BaseFieldSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidDecimalAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.ObjectType;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.alphanum.AlphaNumType;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class BaseFieldSyntacticValidatorTest {
	
	private static BaseFieldSyntacticValidator fieldSyntacticValidator = null;
	private static ConstrainedAttribute constrainedAttribute = null;
	
	@BeforeClass
	public static void setUpTest() throws Exception{
		
		fieldSyntacticValidator = new BaseFieldSyntacticValidator();
		
		BaseAttributeConstraintCheckerFactory onTypeConstraintCheckerFactory =
			   new BaseAttributeConstraintCheckerFactory();
		
		HashMap<Type, HashMap<String,ConstraintChecker>> typeConsMap =
			  new HashMap<Type, HashMap<String,ConstraintChecker>>(); 

		// mappa constraint su AlphaNum
		HashMap<String,ConstraintChecker> consMap = new HashMap<String,ConstraintChecker>();
		consMap.put(AlphaNumConstraint.CONSTRAINT_NAME,
				    new AlphaNumOnTypeConstraintCheckerImpl());
		consMap.put(IsValidDecimalAlphaNumConstraint.CONSTRAINT_NAME,
				    new IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl());
		typeConsMap.put(new AlphaNumType(), consMap);
		// mappa constraint su Object
		HashMap<String,ConstraintChecker> consMap2 = new HashMap<String,ConstraintChecker>();
		consMap2.put(NotNullConstraint.CONSTRAINT_NAME,
				    new NotNullAttributeConstraintCheckerImpl());
		typeConsMap.put(new ObjectType(), consMap2);
		onTypeConstraintCheckerFactory.setPerTypeConstraintCheckerMap(typeConsMap);
		
		fieldSyntacticValidator.setAttributeConstraintCheckerFactory(onTypeConstraintCheckerFactory);
		
		constrainedAttribute = new ConstrainedAttribute();
		constrainedAttribute.setName("valore_monetario");
		constrainedAttribute.setType(new AlphaNumType());
		List<AttributeConstraint> onTypeConstraints = new ArrayList<AttributeConstraint>();
		//0:NotNullConstraint
		onTypeConstraints.add(new NotNullConstraint());
		//1:AlphaNumConstraint
		onTypeConstraints.add(new AlphaNumConstraint());
		//2:IsValidDecimalAlphaNumConstraint
		IsValidDecimalAlphaNumConstraint isValidDecimalAlphaNumConstraint = new IsValidDecimalAlphaNumConstraint();
		isValidDecimalAlphaNumConstraint.setIncludeNegative(false);
		onTypeConstraints.add(isValidDecimalAlphaNumConstraint);
		constrainedAttribute.setAttributeConstraints(onTypeConstraints);
		
	}
	
	@Test
	public void validField() throws Exception{
		SerializableField serializableField = new SerializableField();
		serializableField.setName("valore_monetario");
		serializableField.setValue("1,23");
		
		ValidatedSerializableField validatedSerializableField= fieldSyntacticValidator.validate(serializableField,constrainedAttribute);
		
		assertNotNull("",validatedSerializableField);
		assertTrue("",validatedSerializableField.getIsValid());
		
		assertEquals("",serializableField.getValue(),validatedSerializableField.getValue());
		assertEquals("",serializableField.getName(),validatedSerializableField.getName());
		
		assertNotNull("",validatedSerializableField.getValidationAttribute());
		assertEquals("",constrainedAttribute.getName(),validatedSerializableField.getValidationAttribute().getName());
		assertEquals("",constrainedAttribute.getType(),validatedSerializableField.getValidationAttribute().getType());
		assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
		assertEquals("",constrainedAttribute.getAttributeConstraints().size(),
				        validatedSerializableField.getValidationAttribute().getAttributeConstraints().size());
		assertEquals("",constrainedAttribute.getAttributeConstraints(),
				     validatedSerializableField.getValidationAttribute().getAttributeConstraints());
		
		HashMap<Constraint,ValidationResult> constraintValidationResult =
			 validatedSerializableField.getValidationAttribute().getConstraintValidationResult();
		assertNotNull("",constraintValidationResult);
		assertEquals("",constrainedAttribute.getAttributeConstraints().size(),constraintValidationResult.size());
		ValidationResult firstValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(0));
		assertTrue("",firstValidationResult.isValid());
		assertNull("",firstValidationResult.getValidationException());
		ValidationResult secondValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(1));
		assertTrue("",secondValidationResult.isValid());
		assertNull("",secondValidationResult.getValidationException());
		ValidationResult thirdValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(1));
		assertTrue("",thirdValidationResult.isValid());
		assertNull("",thirdValidationResult.getValidationException());
		
	}
	
	@Test
	public void notValidFieldValue() throws Exception{
		SerializableField serializableField = new SerializableField();
		serializableField.setName("valore_monetario");
		serializableField.setValue("1,23.56");
		
		ValidatedSerializableField validatedSerializableField= fieldSyntacticValidator.validate(serializableField,constrainedAttribute);
		
		assertNotNull("",validatedSerializableField);
		assertFalse("",validatedSerializableField.getIsValid());
		
		assertNotNull("",validatedSerializableField.getValidationAttribute());
		assertEquals("",constrainedAttribute.getName(),validatedSerializableField.getValidationAttribute().getName());
		assertEquals("",constrainedAttribute.getType(),validatedSerializableField.getValidationAttribute().getType());
		assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
		assertEquals("",constrainedAttribute.getAttributeConstraints().size(),
				        validatedSerializableField.getValidationAttribute().getAttributeConstraints().size());
		assertEquals("",constrainedAttribute.getAttributeConstraints(),
				     validatedSerializableField.getValidationAttribute().getAttributeConstraints());
		
		HashMap<Constraint,ValidationResult> constraintValidationResult =
			 validatedSerializableField.getValidationAttribute().getConstraintValidationResult();
		assertNotNull("",constraintValidationResult);
		assertEquals("",constrainedAttribute.getAttributeConstraints().size(),constraintValidationResult.size());
		ValidationResult firstValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(0));
		assertTrue("",firstValidationResult.isValid());
		assertNull("",firstValidationResult.getValidationException());
		ValidationResult secondValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(1));
		assertTrue("",secondValidationResult.isValid());
		assertNull("",secondValidationResult.getValidationException());
		ValidationResult thirdValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(2));
		assertFalse("",thirdValidationResult.isValid());
		assertNull("",thirdValidationResult.getValidationException());
	}
	
	@Test
	public void notValidFieldType() throws Exception{
		SerializableField serializableField = new SerializableField();
		serializableField.setName("valore_monetario");
		serializableField.setValue(new Date());
		
		ValidatedSerializableField validatedSerializableField= fieldSyntacticValidator.validate(serializableField,constrainedAttribute);
		assertNotNull("",validatedSerializableField);
		assertFalse("",validatedSerializableField.getIsValid());
		
		assertEquals("",serializableField.getValue(),validatedSerializableField.getValue());
		assertEquals("",serializableField.getName(),validatedSerializableField.getName());
		
		assertNotNull("",validatedSerializableField.getValidationAttribute());
		assertEquals("",constrainedAttribute.getName(),validatedSerializableField.getValidationAttribute().getName());
		assertEquals("",constrainedAttribute.getType(),validatedSerializableField.getValidationAttribute().getType());
		assertNotNull("",validatedSerializableField.getValidationAttribute().getAttributeConstraints());
		assertEquals("",constrainedAttribute.getAttributeConstraints().size(),
				        validatedSerializableField.getValidationAttribute().getAttributeConstraints().size());
		assertEquals("",constrainedAttribute.getAttributeConstraints(),
				     validatedSerializableField.getValidationAttribute().getAttributeConstraints());
		
		HashMap<Constraint,ValidationResult> constraintValidationResult =
			 validatedSerializableField.getValidationAttribute().getConstraintValidationResult();
		assertNotNull("",constraintValidationResult);
		assertEquals("",constrainedAttribute.getAttributeConstraints().size(),constraintValidationResult.size());
		ValidationResult firstValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(0));
		assertTrue("",firstValidationResult.isValid());
		assertNull("",firstValidationResult.getValidationException());
		ValidationResult secondValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(1));
		assertFalse("",secondValidationResult.isValid());
		assertNull("",secondValidationResult.getValidationException());
		ValidationResult thirdValidationResult = constraintValidationResult.get(constrainedAttribute.getAttributeConstraints().get(2));
		assertFalse("",thirdValidationResult.isValid());
		assertNotNull("",thirdValidationResult.getValidationException());
	}
	
	

}
