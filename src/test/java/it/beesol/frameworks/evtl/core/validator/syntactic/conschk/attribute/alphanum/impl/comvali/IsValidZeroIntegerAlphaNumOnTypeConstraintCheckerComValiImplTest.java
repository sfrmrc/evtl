package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali;

import java.util.Locale;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali.IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidIntegerAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class IsValidZeroIntegerAlphaNumOnTypeConstraintCheckerComValiImplTest {
	
	private static IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl constraintChecker = null;
	private static IsValidIntegerAlphaNumConstraint constraint = null;
	
	@Before
	public void setUpAllTest() throws Exception{
		constraintChecker =
			new IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl();
		constraint = new IsValidIntegerAlphaNumConstraint();
		constraint.setLocale((new Locale("it","IT")));
		constraint.setIncludeZero(false);
	}
	
	
	@Test
	public void notValidZeroInput() throws Exception{
		String input = "0";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void validZeroInput() throws Exception{
		String input = "0";
		constraint.setIncludeZero(true);
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
}
