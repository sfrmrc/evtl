package it.beesol.frameworks.evtl.core.refimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.beesol.bean.PreventivoItemImport;
import it.beesol.frameworks.evtl.core.BaseSpringIntegrationTest;
import it.beesol.frameworks.evtl.core.extractor.bin.BinaryWorkBookExtractor;
import it.beesol.frameworks.evtl.core.transformer.ognl.TransformerOgnl;
import it.beesol.frameworks.evtl.core.validator.syntactic.table.TableSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;

import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:test-evtl-refimpl-model-container-spring-context.xml",
		                         "classpath*:test-evtl-refimpl-extractor-spring-context.xml",
		                         "classpath*:test-evtl-refimpl-transformer-spring-context.xml"})
public class ExtractorValidatorRefImplSpringIntegrationTest extends BaseSpringIntegrationTest{
	
	@Autowired
	@Qualifier("preventivoDiSpesaBinXlsExtractor")
	BinaryWorkBookExtractor preventivoDiSpesaBinXlsExtractor = null;
	
	@Autowired
	TransformerOgnl preventivoDiSpesaItemTransformer;
	
	public void setPreventivoDiSpesaBinXlsExtractor(
			BinaryWorkBookExtractor preventivoDiSpesaBinXlsExtractor) {
		this.preventivoDiSpesaBinXlsExtractor = preventivoDiSpesaBinXlsExtractor;
	}
	
	@Autowired
	@Qualifier("preventivoDiSpesaConstrainedOccurrence")
	ConstrainedOccurrence preventivoDiSpesaConstrainedOccurrence = null;
	
	public void setPreventivoDiSpesaConstrainedOccurrence(
			ConstrainedOccurrence preventivoDiSpesaConstrainedOccurrence) {
		this.preventivoDiSpesaConstrainedOccurrence = preventivoDiSpesaConstrainedOccurrence;
	}
	
	@Autowired
	@Qualifier("preventivoDiSpesaEntity")
	public ConstrainedEntity preventivoDiSpesaConstrainedEntity = null;
	
	@Autowired
	public TableSyntacticValidator tableSyntacticValidator = null;
	
	
	public void setPreventivoDiSpesaEntity(
			ConstrainedEntity preventivoDiSpesaConstrainedEntity) {
		preventivoDiSpesaConstrainedEntity = preventivoDiSpesaConstrainedEntity;
	}
	
	public void setTableSyntacticValidator(
			TableSyntacticValidator tableSyntacticValidator) {
		tableSyntacticValidator = tableSyntacticValidator;
	}




	@Test
	public void test() throws Exception{
		
		String pathTestXlsFile = "refimpl_ExtractorValidator_testFile001.xls";
		InputStream testXlsIS = getClass().getResourceAsStream(pathTestXlsFile);
		
		// ESTRAZIONE
		SerializableSheetSetBook book = preventivoDiSpesaBinXlsExtractor.run(testXlsIS);
		assertNotNull("",book);
		System.out.println("book="+book);
		
		// verifica book
		assertNotNull("",book);

		// verifica sheets
		Set<SerializableTableSetSheet> sheets = book.getSheet();
		assertNotNull("",sheets);
		assertEquals("",1,sheets.size());

		// verifica tablesets
		SerializableTableSetSheet tableSet = sheets.iterator().next();
		assertNotNull("",tableSet);

		// verifica tablesets		
		Set<SerializableRowListTable> tables = tableSet.getTables();
		assertNotNull("",tables);
		assertEquals("",1,tables.size());
		
		// verifica tables
		SerializableRowListTable table = tables.iterator().next();
		assertNotNull("",table);
		
		// verifica rows
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = table.getRows();
		assertNotNull("",rows);
		assertEquals("",1,rows.size());

		// verifica row one by one
		for(int i=0;i<rows.size();i++){
			SerializableFieldLinkedSetRow<SerializableField> row = rows.get(i);
			// System.out.println(row.toString());
			LinkedHashSet<SerializableField> fields = row.getFields();
			assertNotNull("",fields);
			assertEquals("",preventivoDiSpesaConstrainedOccurrence.getAttributes().size(),fields.size());
			Iterator<SerializableField> fieldsIterator = fields.iterator();
			Iterator<ConstrainedAttribute> attributesIterator =  preventivoDiSpesaConstrainedOccurrence.getAttributes().iterator();
			// verifica field one by one
			while(fieldsIterator.hasNext()){
				SerializableField field = fieldsIterator.next();
				ConstrainedAttribute constrainedAttribute = attributesIterator.next();
				assertEquals("",constrainedAttribute.getName(),field.getName());
				//System.out.println(constrainedAttribute.getName()+"-"+field.getName());
			}
		}
		
		// VALIDAZIONE
		ValidatedSerializableRowListTable validatedSerializableRowListTable =
			 tableSyntacticValidator.validate(table,preventivoDiSpesaConstrainedEntity);
		
		// verifica validated table
		assertNotNull("",validatedSerializableRowListTable);
		assertTrue("",validatedSerializableRowListTable.getIsValid());
		// verifica rowlist valide
		assertNotNull("",validatedSerializableRowListTable.getValidRows());
		assertEquals("",validatedSerializableRowListTable.getValidRows().size(),1);
		// verifica rowlist non valide
		assertNotNull("",validatedSerializableRowListTable.getNotValidRows());
		assertEquals("",validatedSerializableRowListTable.getNotValidRows().size(),0);
		// verifica fields row valide
		List<ValidatedSerializableFieldLinkedSetRow> validRows = validatedSerializableRowListTable.getValidRows();
		for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
			assertNotNull("",validatedRow.getFields());
			assertTrue("",validatedRow.getIsValid());
			Iterator<ValidatedSerializableField> fieldsIterator = validatedRow.getFields().iterator();
			while(fieldsIterator.hasNext()){
				ValidatedSerializableField validatedField = fieldsIterator.next();
				assertNotNull("field "+validatedField.getName()+" deve avere valore non nullo",validatedField.getValue());
				assertTrue("",validatedField.getIsValid());
			}
		}

		// TRASFORMAZIONE
		for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows){
		
			PreventivoItemImport preventivoItem = (PreventivoItemImport) preventivoDiSpesaItemTransformer.run(validatedRow);
			assertNotNull(preventivoItem);
			assertNotNull(preventivoItem.getCodComponenteSito());
			
		}
		
	}

	public void setPreventivoDiSpesaItemTransformer(
			TransformerOgnl preventivoDiSpesaItemTransformer) {
		this.preventivoDiSpesaItemTransformer = preventivoDiSpesaItemTransformer;
	}

	public TransformerOgnl getPreventivoDiSpesaItemTransformer() {
		return preventivoDiSpesaItemTransformer;
	}

}
