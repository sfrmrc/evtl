package it.beesol.frameworks.evtl.core.extractor.xls.hssf;

import it.beesol.frameworks.evtl.core.extractor.book.bin.xls.hssf.impl.BinaryBookReaderXlsHSSFImpl;
import it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf.impl.BinaryFieldReaderXlsHSSFImpl;
import it.beesol.frameworks.evtl.core.extractor.row.bin.xls.hssf.impl.BinaryRowReaderXlsHSSFImpl;
import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf.impl.BinarySheetReaderXlsHSSFImpl;
import it.beesol.frameworks.evtl.core.extractor.table.bin.xls.hssf.impl.BinaryTableReaderXlsHSSFImpl;
import it.beesol.frameworks.evtl.model.generic.container.book.BookInterface;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.BaseDataSetAll;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.xls.hssf.impl.DataSetProviderAllHSSFImpl;
import it.beesol.frameworks.evtl.model.generic.provider.entity.all.xls.BaseEntityAll;
import it.beesol.frameworks.evtl.model.generic.provider.entity.all.xls.hssf.EntityProviderAllHSSFImpl;
import it.beesol.frameworks.evtl.model.generic.provider.entityset.all.BaseEntitySetAll;
import it.beesol.frameworks.evtl.model.generic.provider.entityset.all.xls.hssf.impl.EntitySetProviderAllHSSFImpl;
import it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.BaseOccurrenceAll;
import it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.xls.hssf.impl.OccurrenceProviderAllHFFSImpl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class BinaryBookReaderXlsHSSFImplTest {

	private static BinaryBookReaderXlsHSSFImpl bookReader;
	private static BinarySheetReaderXlsHSSFImpl sheetReader;
	private static BinaryTableReaderXlsHSSFImpl tableReader;
	private static BinaryRowReaderXlsHSSFImpl rowReader;
	private static BinaryFieldReaderXlsHSSFImpl fieldReader;
	
	private static DataSetProviderAllHSSFImpl dataSetProvider;
	private static EntitySetProviderAllHSSFImpl entitySetProvider;
	private static EntityProviderAllHSSFImpl entityProvider;
	private static OccurrenceProviderAllHFFSImpl occurrenceProvider;
	
	private static BaseDataSetAll ruleDataSetAll = new BaseDataSetAll();
	private static BaseEntitySetAll ruleEntitySetAll = new BaseEntitySetAll();
	private static BaseEntityAll ruleEntityAll = new BaseEntityAll();
	private static BaseOccurrenceAll ruleOccurrenceAll = new BaseOccurrenceAll();
	
	private static ConstrainedOccurrence constrainedOccurrence = new ConstrainedOccurrence();
	private static LinkedHashSet<ConstrainedAttribute> attributes = new LinkedHashSet<ConstrainedAttribute>();

	@BeforeClass
	public static void setUp() {
		
		ConstrainedAttribute a1 = new ConstrainedAttribute();
		a1.setName("Campo String");
		attributes.add(a1);
		ConstrainedAttribute a2 = new ConstrainedAttribute();
		a2.setName("Campo Numerico");
		attributes.add(a2);
		ConstrainedAttribute a3 = new ConstrainedAttribute();
		a3.setName("Campo Data");
		attributes.add(a3);
		ConstrainedAttribute a4 = new ConstrainedAttribute();
		a4.setName("Campo Vuoto");
		attributes.add(a4);
		ConstrainedAttribute a5 = new ConstrainedAttribute();
		a5.setName("Campo Booleano");
		attributes.add(a5);
		constrainedOccurrence.setAttributes(attributes);
		
		bookReader = new BinaryBookReaderXlsHSSFImpl();
		sheetReader = new BinarySheetReaderXlsHSSFImpl();
		tableReader = new BinaryTableReaderXlsHSSFImpl();
		rowReader = new BinaryRowReaderXlsHSSFImpl();
		fieldReader = new BinaryFieldReaderXlsHSSFImpl();

		ruleOccurrenceAll.setMaxSize(4);
		ruleOccurrenceAll.setMinSize(0);
		occurrenceProvider = new OccurrenceProviderAllHFFSImpl();
		occurrenceProvider.setRule(ruleOccurrenceAll);
		
		ruleEntityAll.setMaxSize(2);
		ruleEntityAll.setMinSize(0);
		entityProvider = new EntityProviderAllHSSFImpl();
		entityProvider.setRule(ruleEntityAll);
		
		ruleEntitySetAll.setMaxSize(100);
		ruleEntitySetAll.setMinSize(0);
		entitySetProvider = new EntitySetProviderAllHSSFImpl();
		entitySetProvider.setRule(ruleEntitySetAll);
		
		ruleDataSetAll.setMaxSize(1);
		ruleDataSetAll.setMinSize(0);
		dataSetProvider = new DataSetProviderAllHSSFImpl();
		dataSetProvider.setRule(ruleDataSetAll);

		rowReader.setFieldReader(fieldReader);
		rowReader.setConstrainedOccurrence(constrainedOccurrence);
		tableReader.setRowReader(rowReader);
		sheetReader.setTableReader(tableReader);
		bookReader.setSheetReader(sheetReader);
		
		rowReader.setProvider(occurrenceProvider);
		tableReader.setProvider(entityProvider);
		sheetReader.setProvider(entitySetProvider);
		bookReader.setProvider(dataSetProvider);
		
	}
	
	@Test
	public void testXlsReader() throws FileNotFoundException, IOException {

		String pathTestXlsFile = "binaryBookReaderXlsHSSFImpl_testFile_001.xls";
		InputStream testXlsIS = getClass().getResourceAsStream(pathTestXlsFile);
		
		POIFSFileSystem poiTestXlsFile = new POIFSFileSystem(testXlsIS);
		HSSFWorkbook workbookHssf = new HSSFWorkbook(poiTestXlsFile);
		
		BookInterface book = bookReader.read(workbookHssf);
		
		// verifica book
		assertNotNull("",book);
		assertTrue("",book instanceof SerializableSheetSetBook);
		SerializableSheetSetBook outputBook  = (SerializableSheetSetBook)book;

		// verifica sheets
		Set<SerializableTableSetSheet> sheets = outputBook.getSheet();
		assertNotNull("",sheets);
		assertEquals("",1,sheets.size());

		// verifica tablesets
		SerializableTableSetSheet tableSet = sheets.iterator().next();
		assertNotNull("",tableSet);

		// verifica tablesets		
		Set<SerializableRowListTable> tables = tableSet.getTables();
		assertNotNull("",tables);
		assertEquals("",1,tables.size());
		
		// verifica tables
		SerializableRowListTable table = tables.iterator().next();
		assertNotNull("",table);
		
		// verifica rows
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = table.getRows();
		assertNotNull("",rows);
		assertEquals("",3,rows.size());
		
		// verifica row one by one
		for(int i=0;i<rows.size();i++){
			SerializableFieldLinkedSetRow<SerializableField> row = rows.get(i);
			System.out.println(row.toString());
			LinkedHashSet<SerializableField> fields = row.getFields();
			assertNotNull("",fields);
			assertEquals("",5,fields.size());
			Iterator<SerializableField> fieldsIterator = fields.iterator();
			Iterator<ConstrainedAttribute> attributesIterator = attributes.iterator();
			// verifica field one by one
			while(fieldsIterator.hasNext()){
				SerializableField field = fieldsIterator.next();
				ConstrainedAttribute constrainedAttribute = attributesIterator.next();
				assertEquals("",constrainedAttribute.getName(),field.getName());
				//System.out.println(constrainedAttribute.getName()+"-"+field.getName());
			}
		}
		System.out.println(book.toString());
		
	}

}