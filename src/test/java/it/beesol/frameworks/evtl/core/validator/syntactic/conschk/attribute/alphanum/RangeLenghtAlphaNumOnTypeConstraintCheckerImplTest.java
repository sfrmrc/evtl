package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import org.junit.Test;
import static org.junit.Assert.*;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.RangeLenghtAlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.RangeLenghtAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class RangeLenghtAlphaNumOnTypeConstraintCheckerImplTest{
	
	@Test
	public void boundaryIncludedFalseValueOutOfRangeStrictlyBelow() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(true);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
	
	@Test
	public void boundaryIncludedFalseValueOutOfRangeStrictlyAbove() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(true);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAAAAAAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
	
	
	@Test
	public void boundaryIncludedTrueValueInOfRangeStrictly() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(true);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAAAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		
	}
	
	@Test
	public void boundaryIncludedTrueValueInRangeEqualsMin() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(true);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		
	}
         
	
	@Test
	public void boundaryIncludedTrueValueInRangeEqualsMax() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(true);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();

		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAAAAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
		
	}


	@Test
	public void boundaryNotIncludedFalseValueOutRangeEqualsMin() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(false);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}
         
	
	@Test
	public void boundaryNotIncludedFalseValueOutRangeEqualsMax() throws Exception{
		
		RangeLenghtAlphaNumConstraint rangeLenghtAlphaNumConstraint = new RangeLenghtAlphaNumConstraint();
		rangeLenghtAlphaNumConstraint.setBoundaryIncluded(false);
		rangeLenghtAlphaNumConstraint.setMinLenght(6);
		rangeLenghtAlphaNumConstraint.setMaxLenght(8);
		
		RangeLenghtAlphaNumOnTypeConstraintCheckerImpl constraintChecker = new RangeLenghtAlphaNumOnTypeConstraintCheckerImpl();
		
		ValidationResult validationResult = constraintChecker.validate(new String("AAAAAAAA"),rangeLenghtAlphaNumConstraint);
		
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
		
	}





}
