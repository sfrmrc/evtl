package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali;

import java.util.Locale;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali.IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidDecimalAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImplTest {
	
	private static IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl constraintChecker = null;
	
	private static IsValidDecimalAlphaNumConstraint constraint = null;
	
	@Before
	public void setUpAllTest() throws Exception{
		constraintChecker =
			new IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl();
		constraint = new IsValidDecimalAlphaNumConstraint();
		constraint.setLocale((new Locale("it","IT")));
	}
	
	@Test
	public void notValidNullInput() throws Exception{
		String input = null;
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidBlankInput() throws Exception{
		String input = " ";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void notValidAlphabeticInput() throws Exception{
		String input = "Aa";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void validNumberInput() throws Exception{
		String input = "1";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNumberWithThousandSeparatorInput1() throws Exception{
		String input = "1.123";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNumberWithThousandSeparatorInput2() throws Exception{
		String input = "98.765.432";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	
	@Test
	public void validNumberWithThousandSeparatorInput3() throws Exception{
		String input = "9.999";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validDecimalInput1() throws Exception{
		String input = "9,999";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validDecimalInput2() throws Exception{
		String input = "0,77";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validDecimalInput3() throws Exception{
		String input = ",77";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
		
	@Test
	public void validZeroInput() throws Exception{
		String input = "0";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNegativeInput1() throws Exception{
		String input = "-1";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNegativeInput2() throws Exception{
		String input = "-23.456.789";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validNegativeDecimalInput() throws Exception{
		String input = "-0,12345678";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validBigNumberInput() throws Exception{
		String input = "1234567890123456789012345678901234567890";
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	

	
	@Test
	public void validNumberWithThousandSeparatorInputEnglishLocale() throws Exception{
		String input = "9,999";
		constraint.setLocale(new Locale("en","US"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validDecimalInputEnglishLocale() throws Exception{
		String input = "9.999";
		constraint.setLocale(new Locale("en","US"));
		ValidationResult validationResult = constraintChecker.validate(input,constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
		
	

}
