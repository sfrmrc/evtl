package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import java.util.HashSet;
import java.util.Set;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.AllowedValuesAlphaNumOnTypeConstraintCheckerImpl;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AllowedValuesAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import org.junit.Test;
import static org.junit.Assert.*;

public class AllowedValuesAlphaNumOnTypeConstraintCheckerImplTest {
	
	@Test
	public void notValidInputNotInAllowedValues() throws Exception{
		AllowedValuesAlphaNumOnTypeConstraintCheckerImpl constraintChecker= new AllowedValuesAlphaNumOnTypeConstraintCheckerImpl();
		AllowedValuesAlphaNumConstraint constraint = new AllowedValuesAlphaNumConstraint();
		Set<String> allowedValues = new HashSet<String>();
		allowedValues.add("S");
		allowedValues.add("N");
		constraint.setAllowedValues(allowedValues);
		ValidationResult validationResult = constraintChecker.validate(new String("A"),constraint);
		assertNotNull("",validationResult);
		assertFalse("",validationResult.isValid());
	}
	
	@Test
	public void validInputNotInAllowedValues1() throws Exception{
		AllowedValuesAlphaNumOnTypeConstraintCheckerImpl constraintChecker= new AllowedValuesAlphaNumOnTypeConstraintCheckerImpl();
		AllowedValuesAlphaNumConstraint constraint = new AllowedValuesAlphaNumConstraint();
		Set<String> allowedValues = new HashSet<String>();
		allowedValues.add("S");
		allowedValues.add("N");
		constraint.setAllowedValues(allowedValues);
		ValidationResult validationResult = constraintChecker.validate(new String("S"),constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}
	
	@Test
	public void validInputNotInAllowedValues2() throws Exception{
		AllowedValuesAlphaNumOnTypeConstraintCheckerImpl constraintChecker= new AllowedValuesAlphaNumOnTypeConstraintCheckerImpl();
		AllowedValuesAlphaNumConstraint constraint = new AllowedValuesAlphaNumConstraint();
		Set<String> allowedValues = new HashSet<String>();
		allowedValues.add("S");
		allowedValues.add("N");
		constraint.setAllowedValues(allowedValues);
		ValidationResult validationResult = constraintChecker.validate(new String("N"),constraint);
		assertNotNull("",validationResult);
		assertTrue("",validationResult.isValid());
	}

}
