package it.beesol.frameworks.evtl.controller.xls;

import it.beesol.frameworks.evtl.controller.BaseSpringIntegrationTest;

import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:test-evtl-refimpl-model-container-spring-context.xml",
                             "classpath*:test-evtl-refimpl-extractor-spring-context.xml",
                             "classpath*:test-evtl-refimpl-transformer-spring-context.xml",
                             "classpath*:test-evtl-refimpl-controller-spring-context.xml"})
public class ControllerIntegrationTest extends BaseSpringIntegrationTest {

  @Autowired
  @Qualifier("preventivoDiSpesaWorkflowController")
  ControllerXls preventivoDiSpesaWorkflowController;
  
  @Test
  public void test() throws Exception {
   
    String pathTestXlsFile = "refimpl_ControllerIntegration_testFile001.xls";
    InputStream testXlsIS = getClass().getResourceAsStream(pathTestXlsFile);
    
    preventivoDiSpesaWorkflowController.run(testXlsIS);
    
  }

  public void setPreventivoDiSpesaWorkflowController(ControllerXls preventivoDiSpesaWorkflowController) {
    this.preventivoDiSpesaWorkflowController = preventivoDiSpesaWorkflowController;
  }

}
