package it.beesol.bean;

public class ComponenteSito {

	private Long id;
	private String codComponenteSito;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCodComponenteSito(String codComponenteSito) {
		this.codComponenteSito = codComponenteSito;
	}

	public String getCodComponenteSito() {
		return codComponenteSito;
	}
	
}
