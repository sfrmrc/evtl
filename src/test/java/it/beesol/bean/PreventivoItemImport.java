package it.beesol.bean;

import java.util.Date;

public class PreventivoItemImport {

  private String codComponenteSito;
  private String codServizio;
  private String tipoServizio;
  private Double quantita;
  private Double interventi;
  private Double importoUnitario;
  private Double importoAnnuo;
  private Double importoTotale;
  private Date dataInizio;
  private Date dataFine;
  private Integer durataMesi;
  private Double nprestAggAnno;
  private Double ridAttPcanone;
  private Double percScontoPelaborati;
  private String flagServizioAttivo;
  private String reperibilita;
  private Double areaMqNetta;
  private Double areaMqLorda;
  private String um;

  public String getCodComponenteSito() {
    return codComponenteSito;
  }

  public void setCodComponenteSito(String codComponenteSito) {
    this.codComponenteSito = codComponenteSito;
  }

  public String getCodServizio() {
    return codServizio;
  }

  public void setCodServizio(String codServizio) {
    this.codServizio = codServizio;
  }

  public String getTipoServizio() {
    return tipoServizio;
  }

  public void setTipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  public Double getQuantita() {
    return quantita;
  }

  public void setQuantita(Double quantita) {
    this.quantita = quantita;
  }

  public Double getInterventi() {
    return interventi;
  }

  public void setInterventi(Double interventi) {
    this.interventi = interventi;
  }

  public Double getImportoUnitario() {
    return importoUnitario;
  }

  public void setImportoUnitario(Double importoUnitario) {
    this.importoUnitario = importoUnitario;
  }

  public Double getImportoAnnuo() {
    return importoAnnuo;
  }

  public void setImportoAnnuo(Double importoAnnuo) {
    this.importoAnnuo = importoAnnuo;
  }

  public Double getImportoTotale() {
    return importoTotale;
  }

  public void setImportoTotale(Double importoTotale) {
    this.importoTotale = importoTotale;
  }

  public Date getDataInizio() {
    return dataInizio;
  }

  public void setDataInizio(Date dataInizio) {
    this.dataInizio = dataInizio;
  }

  public Date getDataFine() {
    return dataFine;
  }

  public void setDataFine(Date dataFine) {
    this.dataFine = dataFine;
  }

  public Integer getDurataMesi() {
    return durataMesi;
  }

  public void setDurataMesi(Integer durataMesi) {
    this.durataMesi = durataMesi;
  }

  public Double getNprestAggAnno() {
    return nprestAggAnno;
  }

  public void setNprestAggAnno(Double nprestAggAnno) {
    this.nprestAggAnno = nprestAggAnno;
  }

  public Double getRidAttPcanone() {
    return ridAttPcanone;
  }

  public void setRidAttPcanone(Double ridAttPcanone) {
    this.ridAttPcanone = ridAttPcanone;
  }

  public Double getPercScontoPelaborati() {
    return percScontoPelaborati;
  }

  public void setPercScontoPelaborati(Double percScontoPelaborati) {
    this.percScontoPelaborati = percScontoPelaborati;
  }

  public String getFlagServizioAttivo() {
    return flagServizioAttivo;
  }

  public void setFlagServizioAttivo(String flagServizioAttivo) {
    this.flagServizioAttivo = flagServizioAttivo;
  }

  public String getReperibilita() {
    return reperibilita;
  }

  public void setReperibilita(String reperibilita) {
    this.reperibilita = reperibilita;
  }

  public Double getAreaMqNetta() {
    return areaMqNetta;
  }

  public void setAreaMqNetta(Double areaMqNetta) {
    this.areaMqNetta = areaMqNetta;
  }

  public Double getAreaMqLorda() {
    return areaMqLorda;
  }

  public void setAreaMqLorda(Double areaMqLorda) {
    this.areaMqLorda = areaMqLorda;
  }

  public String getUm() {
    return um;
  }

  public void setUm(String um) {
    this.um = um;
  }

}
