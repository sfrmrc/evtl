package it.beesol.frameworks.evtl.core.validator.syntactic.conschk;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;

public abstract class BaseConstraintChecker<C extends Constraint>
       implements ConstraintChecker<C> {

	protected Log logger = LogFactory.getLog(this.getClass());

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getName());
		return builder.toString();
	}

}
