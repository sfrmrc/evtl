package it.beesol.frameworks.evtl.core.validator.syntactic.field;

import it.beesol.frameworks.evtl.core.validator.syntactic.SyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;

public interface FieldSyntacticValidator extends SyntacticValidator<ValidatedSerializableField,SerializableField,ConstrainedAttribute> {

}
