package it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf.impl;

import java.util.HashSet;
import java.util.Set;

import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf.BaseBinarySheetReaderXlsHSSF;
import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf.BinarySheetReaderXlsHSSF;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SheetInterface;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;

import org.apache.poi.hssf.usermodel.HSSFSheet;

public class BinarySheetReaderXlsHSSFImpl extends BaseBinarySheetReaderXlsHSSF implements
		BinarySheetReaderXlsHSSF {

	private static final long serialVersionUID = 2180281271435791687L;

	public SheetInterface read(HSSFSheet sheetHssf) {
		
		SerializableTableSetSheet sheet = new SerializableTableSetSheet();
		Set<SerializableRowListTable> tables = new HashSet<SerializableRowListTable>();
		
		this.provider.init(sheetHssf);
		
		while(this.provider.hasNext()) {
			
			tables.add((SerializableRowListTable) this.tableReader.read((HSSFSheet) this.provider.next()));
			
		}
		
		sheet.setTables(tables);
		
		return sheet;
		
	}

}
