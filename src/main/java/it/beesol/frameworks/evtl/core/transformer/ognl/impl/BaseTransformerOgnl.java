package it.beesol.frameworks.evtl.core.transformer.ognl.impl;

import java.lang.reflect.ParameterizedType;

import it.beesol.frameworks.evtl.core.transformer.ognl.TransformerOgnl;

public class BaseTransformerOgnl<O> extends TransformerOgnl<O> {

	private static final long serialVersionUID = 5667846155667368432L;

	private Class<O> clazz;
	
	public BaseTransformerOgnl() {
		clazz = (Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public O createBean() {

		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
