package it.beesol.frameworks.evtl.core.extractor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class BaseExtractor<R extends Reader,I,C> implements Extractor<I,C>{
	
	protected Log logger = LogFactory.getLog(this.getClass());

	protected R reader;

	public void setReader(R reader) {
		this.reader = reader;
	}

	public R getReader() {
		return reader;
	}
	
}
