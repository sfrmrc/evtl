package it.beesol.frameworks.evtl.core.extractor.book;

import it.beesol.frameworks.evtl.core.extractor.Reader;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;

public interface BookReader<I,B extends Book>
		extends Reader<I,B> {

}
