package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.entity;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;

public interface EntityConstraintChecker<OC extends EntityConstraint> extends
		ConstraintChecker<EntityConstraint> {

}
