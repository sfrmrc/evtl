package it.beesol.frameworks.evtl.core.extractor.table.bin.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import it.beesol.frameworks.evtl.core.extractor.table.bin.xls.BinaryTableReaderXls;

public interface BinaryTableReaderXlsHSSF extends BinaryTableReaderXls<HSSFSheet> {

}
