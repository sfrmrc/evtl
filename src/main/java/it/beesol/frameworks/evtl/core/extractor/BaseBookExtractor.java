package it.beesol.frameworks.evtl.core.extractor;

import it.beesol.frameworks.evtl.core.extractor.BaseExtractor;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;

public abstract class BaseBookExtractor<R extends Reader,I,B extends Book,O extends Object>
        extends BaseExtractor<R,I,B>
        implements BookExtractor<I,B> {

	private static final long serialVersionUID = -8628470160768057218L;

	public B run(I input)  throws Exception {
		O obj = this.extract(input);
		return (B)this.reader.read(obj);
	}
	
	protected abstract O extract(I input)  throws Exception;

}
