package it.beesol.frameworks.evtl.core.extractor.book;

import it.beesol.frameworks.evtl.core.extractor.sheet.SheetReader;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;

public abstract class BaseSerializableBookReader<I,S extends SheetReader>
       extends BaseBookReader<I,SerializableSheetSetBook,S>
       implements SerializableBookReader<I>{

	protected S sheetReader;

	public void setSheetReader(S sheetReader) {
		this.sheetReader = sheetReader;
	}

	public S getSheetReader() {
		return sheetReader;
	}

}
