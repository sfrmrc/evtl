package it.beesol.frameworks.evtl.core.extractor.book.bin.xls;

import it.beesol.frameworks.evtl.core.extractor.book.bin.BaseBinaryBookReader;
import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.BinarySheetReaderXls;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;

public abstract class BaseBinaryBookReaderXls<I,S extends BinarySheetReaderXls>
       extends BaseBinaryBookReader<I,S>
	   implements BinaryBookReaderXls<I>{

}
