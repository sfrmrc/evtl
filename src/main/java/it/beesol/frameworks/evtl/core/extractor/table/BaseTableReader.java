package it.beesol.frameworks.evtl.core.extractor.table;

import it.beesol.frameworks.evtl.core.extractor.BaseReader;
import it.beesol.frameworks.evtl.core.extractor.row.RowReader;
import it.beesol.frameworks.evtl.model.generic.provider.entity.EntityProvider;

public class BaseTableReader<R extends RowReader> extends BaseReader<EntityProvider> {

	protected R rowReader;

	public void setRowReader(R rowReader) {
		this.rowReader = rowReader;
	}

	public R getRowReader() {
		return rowReader;
	}
	
}
