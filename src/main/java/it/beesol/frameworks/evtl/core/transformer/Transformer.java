package it.beesol.frameworks.evtl.core.transformer;

import it.beesol.frameworks.evtl.model.generic.GenericModel;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;

public interface Transformer<O> extends GenericModel {

	public O run(SerializableFieldLinkedSetRow<SerializableField> input) throws Exception;
	
}