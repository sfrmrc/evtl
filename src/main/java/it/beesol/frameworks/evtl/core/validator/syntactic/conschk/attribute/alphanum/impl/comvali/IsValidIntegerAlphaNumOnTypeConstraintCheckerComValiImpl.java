package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali;

import org.apache.commons.validator.routines.IntegerValidator;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.BaseAlphaNumOnTypeConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.IsValidIntegerAlphaNumOnTypeConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidIntegerAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class IsValidIntegerAlphaNumOnTypeConstraintCheckerComValiImpl
         extends BaseConstraintChecker<IsValidIntegerAlphaNumConstraint>
         implements IsValidIntegerAlphaNumOnTypeConstraintChecker {



	public ValidationResult validate(Object input,IsValidIntegerAlphaNumConstraint constraint) {
		String inputAsString = (String)input;
		IntegerValidator validator = IntegerValidator.getInstance();
		boolean isValid = false;
		//System.out.println("XX:"+inputAsString+";"+this.constraint.getLocale()+";"+this.constraint.getPattern()+":"+validator.validate(inputAsString,this.constraint.getPattern(),this.constraint.getLocale()));
		Integer inputAsInteger = validator.validate(inputAsString,constraint.getPattern(),constraint.getLocale());
		//System.out.println("XX:isValid="+isValid);
		if ( inputAsInteger!=null ){
			isValid=true;
			if ( !constraint.isIncludeZero() && inputAsInteger==0) {
				isValid=false;
			}
			if ( !constraint.isIncludePositive() && inputAsInteger>0) {
				isValid=false;
			}
			if ( !constraint.isIncludeNegative() && inputAsInteger<0) {
				isValid=false;
			}
		}
		return new ValidationResult(isValid);
	}

}
