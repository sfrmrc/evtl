package it.beesol.frameworks.evtl.core.extractor;

import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;

public interface SerializableBookExtractor<I> extends BookExtractor<I,SerializableSheetSetBook> {
	
	public SerializableSheetSetBook run(I input)  throws Exception;

}
