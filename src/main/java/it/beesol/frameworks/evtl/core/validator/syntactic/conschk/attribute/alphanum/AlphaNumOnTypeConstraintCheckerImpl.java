package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class AlphaNumOnTypeConstraintCheckerImpl
        extends BaseConstraintChecker<AlphaNumConstraint>
		implements AlphaNumOnTypeConstraintChecker {
	
	// TODO : rinominare in TypeOnTypeConstraintCheckerImpl (anche interfaccia) e spostare in package superiore
	
	public ValidationResult validate(Object input,AlphaNumConstraint constraint){
		logger.debug("validate: constraint="+constraint);
		boolean isValid = false;
		try{
			Class expectedClass = Class.forName(constraint.getType().getJavaFqn());
			//logger.debug("expectedClass="+expectedClass);
			if ( input!=null && expectedClass.equals(input.getClass()) ){
				isValid = true;
			}	
		}catch(Exception ex){
			// TODO : migliorare gestione eccezioni
			isValid = false;
		}
		return new ValidationResult(isValid);
	}
	
	
}
