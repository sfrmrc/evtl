package it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf;

import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.BaseBinarySheetReaderXls;
import it.beesol.frameworks.evtl.core.extractor.table.bin.xls.hssf.BinaryTableReaderXlsHSSF;

public abstract class BaseBinarySheetReaderXlsHSSF extends BaseBinarySheetReaderXls<BinaryTableReaderXlsHSSF> {

}
