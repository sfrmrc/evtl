package it.beesol.frameworks.evtl.core.transformer.ognl;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.LinkedHashSet;

import ognl.Ognl;
import ognl.OgnlException;

import it.beesol.frameworks.evtl.core.transformer.BaseTransformer;
import it.beesol.frameworks.evtl.core.transformer.Transformer;

import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializablePersistenceField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;

public abstract class TransformerOgnl<O> extends BaseTransformer implements Transformer<O> {

	private static final long serialVersionUID = -5597942776903854859L;

	public O run(SerializableFieldLinkedSetRow<SerializableField> input) throws Exception {

		logger.debug("transforming "+input);
		
		O element = createBean();
		
		logger.debug("created bean "+element);
		
		LinkedHashSet<SerializableField> fields = input.getFields();
		Iterator<SerializableField> iterator = fields.iterator();
		
		while(iterator.hasNext()) {
		
			SerializableField field = iterator.next();
			String name = field.getName();
			Object value = field.getValue();
			
			SerializablePersistenceField persistence = this.persistenceFields.get(name);
			
			logger.info("transforming "+field+" into ["+persistence+"]");
			
		    String expression = null;
		    
		    if(persistence!=null) {
		    	expression = persistence.getPersistence();
		    }else{
		    	
		    	logger.warn("persistence is null for field "+field);
		    	expression = field.getName();
		    	logger.info("using default persistence name "+expression);
		    	
		    }

			try {

				setAttributeInChain(expression, element, value);

			} catch (Exception e) {
				logger.warn(field.getName() + " no persistence found");
			}
	    
		}
		
		return element;
		
	}
	
	private String setAttributeInChain(String expression, Object root, Object value) throws OgnlException, InstantiationException, IllegalAccessException {
		
		if(Ognl.isSimpleProperty(expression)) {
			
			Ognl.setValue(expression, root, value);
			return expression;
			
		}else{
			
			String[] chainString = expression.split("\\.");
			String newRootExpression = chainString[0];
			
			Field[] rootFields = root.getClass().getDeclaredFields();
			Object newRoot = null;
			for(int i=0; i<rootFields.length; i++) {
				if(rootFields[i].getName().equals(newRootExpression)) {
					newRoot = rootFields[i].getType().newInstance();
					break;
				}
			}
			
			Ognl.setValue(newRootExpression, root, newRoot);
			
			String newExpression = expression.replace(newRootExpression+".", "");
			return setAttributeInChain(newExpression, newRoot, value);
			
		}
		
	}
	
	public abstract O createBean() throws Exception;

}
