package it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls;

import it.beesol.frameworks.evtl.core.extractor.sheet.bin.BinarySheetReader;

public interface BinarySheetReaderXls<D> extends BinarySheetReader<D> {

}
