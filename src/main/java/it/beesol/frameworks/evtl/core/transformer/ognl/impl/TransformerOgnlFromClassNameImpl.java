package it.beesol.frameworks.evtl.core.transformer.ognl.impl;

import it.beesol.frameworks.evtl.core.transformer.ognl.TransformerOgnl;

public class TransformerOgnlFromClassNameImpl extends TransformerOgnl<Object> {

	private static final long serialVersionUID = 6177058781899837097L;
	private String className;
	
	@Override
	public Object createBean() throws Exception {
		
		try {
			return Class.forName(className).newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassName() {
		return className;
	}

}
