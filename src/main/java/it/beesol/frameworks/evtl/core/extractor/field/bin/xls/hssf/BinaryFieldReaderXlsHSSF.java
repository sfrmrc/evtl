package it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFCell;

import it.beesol.frameworks.evtl.core.extractor.field.bin.xls.BinaryFieldReaderXls;

public interface BinaryFieldReaderXlsHSSF extends BinaryFieldReaderXls<HSSFCell> {

}
