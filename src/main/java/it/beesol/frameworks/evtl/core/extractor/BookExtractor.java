package it.beesol.frameworks.evtl.core.extractor;

import it.beesol.frameworks.evtl.core.extractor.Extractor;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;

public interface BookExtractor<I,B extends Book> extends Extractor<I,B> {

}
