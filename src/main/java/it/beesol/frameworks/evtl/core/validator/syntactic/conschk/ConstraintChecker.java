package it.beesol.frameworks.evtl.core.validator.syntactic.conschk;

import it.beesol.frameworks.evtl.core.validator.syntactic.SyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public interface ConstraintChecker<C extends Constraint>
       extends SyntacticValidator<ValidationResult,Object,C>{


	public ValidationResult validate(Object input,C constraint);

}
