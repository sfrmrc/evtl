package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;

public abstract class BaseAlphaNumOnTypeConstraintChecker<C extends Constraint>
		extends BaseConstraintChecker<Constraint> {

	protected String castInput(Object input) {
		// TODO : generics in modo da evitare controllo del cast o inserire
		// try/catch
		String inputAsString = (String) input;
		return inputAsString;
	}

}
