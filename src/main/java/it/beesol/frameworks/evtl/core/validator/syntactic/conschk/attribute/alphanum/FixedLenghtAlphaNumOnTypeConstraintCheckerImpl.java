package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.FixedLenghtAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class FixedLenghtAlphaNumOnTypeConstraintCheckerImpl extends
		BaseConstraintChecker<FixedLenghtAlphaNumConstraint> implements
		FixedLenghtAlphaNumOnTypeConstraintChecker {

	public ValidationResult validate(Object input,
			FixedLenghtAlphaNumConstraint constraint) {
		String inputAsString = (String) input;
		if (inputAsString.length() != constraint.getFixedLenght()) {
			return new ValidationResult(false, "input " + inputAsString
					+ " lenght is " + inputAsString.length() + "and not "
					+ constraint.getFixedLenght() + " as expected");
		}
		return new ValidationResult(true);
	}

}
