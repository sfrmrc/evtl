package it.beesol.frameworks.evtl.core.extractor;

import it.beesol.frameworks.evtl.core.extractor.BaseBookExtractor;
import it.beesol.frameworks.evtl.core.extractor.book.SerializableBookReader;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;

public abstract class BaseSerializableBookExtractor<R extends SerializableBookReader<O>,I,O extends Object>
       extends BaseBookExtractor<R,I,SerializableSheetSetBook,O>
       implements SerializableBookExtractor<I> {

	private static final long serialVersionUID = -8628470160768057218L;
	
	public SerializableSheetSetBook run(I input)  throws Exception {
		O obj = this.extract(input);
		return this.reader.read(obj);
	}


}
