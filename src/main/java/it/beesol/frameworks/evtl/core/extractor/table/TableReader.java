package it.beesol.frameworks.evtl.core.extractor.table;

import it.beesol.frameworks.evtl.core.extractor.Reader;
import it.beesol.frameworks.evtl.model.generic.container.table.TableInterface;

public interface TableReader<I>
		extends Reader<I, TableInterface> {

}
