package it.beesol.frameworks.evtl.core.extractor.row;

import it.beesol.frameworks.evtl.core.extractor.BaseReader;
import it.beesol.frameworks.evtl.core.extractor.field.FieldReader;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.provider.occurrence.OccurrenceProvider;

public class BaseRowReader<F extends FieldReader> extends BaseReader<OccurrenceProvider> {

	protected F fieldReader;
	protected ConstrainedOccurrence constrainedOccurrence;

	public void setFieldReader(F fieldReader) {
		this.fieldReader = fieldReader;
	}

	public F getFieldReader() {
		return fieldReader;
	}

	public void setConstrainedOccurrence(ConstrainedOccurrence constrainedOccurrence) {
		this.constrainedOccurrence = constrainedOccurrence;
	}

	public ConstrainedOccurrence getConstrainedOccurrence() {
		return constrainedOccurrence;
	}
	
}
