package it.beesol.frameworks.evtl.core.validator.semantic;

import it.beesol.frameworks.evtl.core.validator.Validator;



public interface SemanticValidator<O extends Object,I extends Object> extends Validator<O,I> {
	 
	public O validate(I input) throws Exception;
	
}
 
