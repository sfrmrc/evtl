package it.beesol.frameworks.evtl.core.validator.syntactic.row;

import it.beesol.frameworks.evtl.core.validator.syntactic.SyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;

public interface RowSyntacticValidator
       extends SyntacticValidator<ValidatedSerializableFieldLinkedSetRow,
                                  SerializableFieldLinkedSetRow<SerializableField>,
                                  ConstrainedOccurrence> {

}
