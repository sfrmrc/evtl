package it.beesol.frameworks.evtl.core.validator.syntactic.conschk;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class BaseConstraintCheckerFactory<CC extends ConstraintChecker<C>, C extends Constraint>
		implements ConstraintCheckerFactory<CC, C> {

	protected Log logger = LogFactory.getLog(this.getClass());

}
