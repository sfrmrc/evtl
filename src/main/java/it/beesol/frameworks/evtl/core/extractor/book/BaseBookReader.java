package it.beesol.frameworks.evtl.core.extractor.book;

import it.beesol.frameworks.evtl.core.extractor.BaseReader;
import it.beesol.frameworks.evtl.core.extractor.sheet.SheetReader;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.DataSetProvider;

public abstract class BaseBookReader<I,B extends Book,S extends SheetReader>
       extends BaseReader<DataSetProvider>
       implements BookReader<I,B>{

	protected S sheetReader;

	public void setSheetReader(S sheetReader) {
		this.sheetReader = sheetReader;
	}

	public S getSheetReader() {
		return sheetReader;
	}

}
