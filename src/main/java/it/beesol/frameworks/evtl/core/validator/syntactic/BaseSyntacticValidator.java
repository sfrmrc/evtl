package it.beesol.frameworks.evtl.core.validator.syntactic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import it.beesol.frameworks.evtl.core.validator.BaseValidator;
import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;



public abstract class BaseSyntacticValidator<O extends Object,I extends Object,D extends Descriptor> extends BaseValidator<O,I>
  implements SyntacticValidator<O,I,D>{
	
	protected Log logger = LogFactory.getLog(this.getClass());
 
	 
}
 
