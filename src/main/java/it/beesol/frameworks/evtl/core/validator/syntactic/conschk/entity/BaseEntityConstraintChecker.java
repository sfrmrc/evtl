package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.entity;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;

public abstract class BaseEntityConstraintChecker<EC extends EntityConstraint>
		extends BaseConstraintChecker<EntityConstraint>
        implements EntityConstraintChecker<EC> {


}
