package it.beesol.frameworks.evtl.core.extractor;

import it.beesol.frameworks.evtl.model.generic.provider.Provider;

public abstract class BaseReader<P extends Provider> {
	
	protected P provider;

	public void setProvider(P provider) {
		this.provider = provider;
	}

	public P getProvider() {
		return provider;
	}
	
}
