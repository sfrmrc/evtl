package it.beesol.frameworks.evtl.core.extractor.bin;

import java.io.InputStream;

import it.beesol.frameworks.evtl.core.extractor.SerializableBookExtractor;

public interface BinaryWorkBookExtractor extends SerializableBookExtractor<InputStream> {

}
