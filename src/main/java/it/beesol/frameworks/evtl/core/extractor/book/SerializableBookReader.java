package it.beesol.frameworks.evtl.core.extractor.book;

import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;

public interface SerializableBookReader<I>
		extends BookReader<I,SerializableSheetSetBook> {

}
