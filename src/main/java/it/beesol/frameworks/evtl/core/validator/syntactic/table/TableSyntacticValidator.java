package it.beesol.frameworks.evtl.core.validator.syntactic.table;

import it.beesol.frameworks.evtl.core.validator.syntactic.SyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;

public interface TableSyntacticValidator
       extends SyntacticValidator<ValidatedSerializableRowListTable,SerializableRowListTable,ConstrainedEntity> {

}
