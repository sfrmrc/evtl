package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;

public abstract class BaseAttributeConstraintChecker<OTC extends AttributeConstraint>
		extends BaseConstraintChecker<AttributeConstraint> implements
		AttributeConstraintChecker<OTC> {

	// public abstract ValidationResult check(Object input,OTC constraint);

}
