package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.InRangeIntegerConstraint;

public interface InRangeIntegerOnTypeConstraintChecker extends ConstraintChecker<InRangeIntegerConstraint> {
	

}
