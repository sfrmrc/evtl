package it.beesol.frameworks.evtl.core.loader.datastore;

import it.beesol.frameworks.evtl.core.loader.BaseLoader;
import it.beesol.frameworks.evtl.core.loader.Loader;

import java.util.Collection;

public class LoaderDatastoreCollection<C> extends BaseLoader implements Loader<C, Collection<C>> {

  private static final long serialVersionUID = -4706966071157244581L;
  
  protected Collection<C> collection;
  
  public Collection<C> store(C input) {
    
    collection.add(input);
    
    return collection;
    
  }

  public void setCollection(Collection<C> collection) {
    this.collection = collection;
  }

  public Collection<C> getCollection() {
    return collection;
  }

  public Collection<C> get() {
    return collection;
  }

}
