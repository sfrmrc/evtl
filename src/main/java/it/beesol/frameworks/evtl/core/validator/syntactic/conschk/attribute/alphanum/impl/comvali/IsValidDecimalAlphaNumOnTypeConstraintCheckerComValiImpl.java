package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali;

import java.math.BigDecimal;

import org.apache.commons.validator.routines.BigDecimalValidator;


import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.BaseAlphaNumOnTypeConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.IsValidDecimalAlphaNumOnTypeConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidDecimalAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class IsValidDecimalAlphaNumOnTypeConstraintCheckerComValiImpl
         extends BaseConstraintChecker<IsValidDecimalAlphaNumConstraint>
                                                implements IsValidDecimalAlphaNumOnTypeConstraintChecker {

	


	public ValidationResult validate(Object input,IsValidDecimalAlphaNumConstraint constraint) {
		String inputAsString = (String)input;
		logger.debug("applyValidation: constraint="+constraint);
		BigDecimalValidator validator = BigDecimalValidator.getInstance();
		boolean isValid = false;
		//logger.debug("XX:"+inputAsString+";"+this.constraint.getLocale()+";"+this.constraint.getPattern()+":"+validator.validate(inputAsString,this.constraint.getPattern(),this.constraint.getLocale()));
		BigDecimal inputAsBigDecimal = validator.validate(inputAsString,constraint.getPattern(),constraint.getLocale());
		if ( inputAsBigDecimal!=null ){
			isValid=true;
			if ( !constraint.isIncludeZero() && inputAsBigDecimal.equals(new BigDecimal(0))) {
				isValid=false;
			}
			if ( !constraint.isIncludePositive() && inputAsBigDecimal.compareTo(new BigDecimal(0))>0) {
				isValid=false;
			}
			if ( !constraint.isIncludeNegative() && inputAsBigDecimal.compareTo(new BigDecimal(0))<0) {
				isValid=false;
			}
		}
		//logger.debug("XX:isValid="+isValid);
		return new ValidationResult(isValid);
	}

}
