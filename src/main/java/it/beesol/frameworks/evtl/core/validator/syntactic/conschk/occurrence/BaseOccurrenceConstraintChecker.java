package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;

public abstract class BaseOccurrenceConstraintChecker<OC extends OccurrenceConstraint>
		extends BaseConstraintChecker<OccurrenceConstraint>
        implements OccurrenceConstraintChecker<OC> {

	// public abstract ValidationResult check(Object input,OTC constraint);

}
