package it.beesol.frameworks.evtl.core.extractor.book.bin.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import it.beesol.frameworks.evtl.core.extractor.book.bin.xls.BaseBinaryBookReaderXls;
import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf.BinarySheetReaderXlsHSSF;

public abstract class BaseBinaryBookReaderXlsHSSF
		extends BaseBinaryBookReaderXls<HSSFWorkbook,BinarySheetReaderXlsHSSF>
		implements BinaryBookReaderXlsHSSF{

}
