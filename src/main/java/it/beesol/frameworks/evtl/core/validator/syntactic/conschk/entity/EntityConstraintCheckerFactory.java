package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.entity;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintCheckerFactory;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;

public interface EntityConstraintCheckerFactory<EC extends EntityConstraint>
       extends ConstraintCheckerFactory<ConstraintChecker<EC>,EC>{
	
	// TODO : exccezioen specifica per costrant non gestito - gerarchia di eccezioni
	
	public ConstraintChecker<EC> getConstraintChecker(EC constraint) throws Exception;

}
