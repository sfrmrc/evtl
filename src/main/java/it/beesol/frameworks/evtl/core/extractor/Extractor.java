package it.beesol.frameworks.evtl.core.extractor;

public interface Extractor<I, C>{

	public C run(I input) throws Exception;
	
}
