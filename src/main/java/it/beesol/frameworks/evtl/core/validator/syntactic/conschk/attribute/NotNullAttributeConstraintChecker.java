package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;

public interface NotNullAttributeConstraintChecker extends ConstraintChecker<NotNullConstraint> {

}
