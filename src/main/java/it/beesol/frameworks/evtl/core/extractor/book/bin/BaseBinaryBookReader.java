package it.beesol.frameworks.evtl.core.extractor.book.bin;

import it.beesol.frameworks.evtl.core.extractor.book.BaseSerializableBookReader;
import it.beesol.frameworks.evtl.core.extractor.sheet.bin.BinarySheetReader;

public abstract class BaseBinaryBookReader<I,S extends BinarySheetReader>
		extends BaseSerializableBookReader<I,S> 
		implements BinaryBookReader<I> {

}
