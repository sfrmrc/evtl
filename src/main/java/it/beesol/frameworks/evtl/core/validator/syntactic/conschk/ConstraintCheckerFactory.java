package it.beesol.frameworks.evtl.core.validator.syntactic.conschk;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;

public interface ConstraintCheckerFactory<CC extends ConstraintChecker, C extends Constraint> {

	// TODO : exccezioen specifica per costrant non gestito - gerarchia di
	// eccezioni

	public CC getConstraintChecker(C constraint) throws Exception;

}
