package it.beesol.frameworks.evtl.core.extractor.sheet;

import it.beesol.frameworks.evtl.core.extractor.Reader;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SheetInterface;

public interface SheetReader<I>
		extends Reader<I, SheetInterface> {

}
