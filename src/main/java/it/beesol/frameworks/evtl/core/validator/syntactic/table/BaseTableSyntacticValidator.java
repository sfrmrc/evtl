package it.beesol.frameworks.evtl.core.validator.syntactic.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.beesol.frameworks.evtl.core.validator.syntactic.BaseSyntacticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.entity.EntityConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.row.RowSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ValidationEntity;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class BaseTableSyntacticValidator
        extends BaseSyntacticValidator<ValidatedSerializableRowListTable,SerializableRowListTable,ConstrainedEntity>
        implements TableSyntacticValidator {
	
	private RowSyntacticValidator rowSyntacticValidator = null;
	
	public void setRowSyntacticValidator(RowSyntacticValidator rowSyntacticValidator) {
		this.rowSyntacticValidator = rowSyntacticValidator;
	}
	
	private EntityConstraintCheckerFactory<EntityConstraint> entityConstraintCheckerFactory = null;
	

	public void setEntityConstraintCheckerFactory(
			EntityConstraintCheckerFactory<EntityConstraint> entityConstraintCheckerFactory) {
		this.entityConstraintCheckerFactory = entityConstraintCheckerFactory;
	}





	public ValidatedSerializableRowListTable validate(SerializableRowListTable input, ConstrainedEntity descriptor)
			throws Exception {
		
		logger.info("validating " + input);

		// (0) istanziazione validatedSerializableRowListTable di out
		ValidatedSerializableRowListTable validatedSerializableRowListTable = new ValidatedSerializableRowListTable(input);
		validatedSerializableRowListTable.setIsValid(true);
		
		// (1) validazione su row
		// (1.a) istanziazione lista row valide
		List<ValidatedSerializableFieldLinkedSetRow> validRows = new ArrayList<ValidatedSerializableFieldLinkedSetRow>();
		// (1.b) istanziazione lista row non valide
		List<ValidatedSerializableFieldLinkedSetRow> notValidRows = new ArrayList<ValidatedSerializableFieldLinkedSetRow>();
		// (1.c) determinazione row valide e non valide
		for (SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow : input.getRows()){
			logger.debug("start validating row "+serializableFieldListRow.getNumber());
			ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldLinkedSetRow = 
				  this.rowSyntacticValidator.validate(serializableFieldListRow,descriptor.getConstrainedOccurrence());
			if ( validatedSerializableFieldLinkedSetRow.getIsValid() ){
				logger.debug("row "+serializableFieldListRow.getNumber()+" is valid");
				validRows.add(validatedSerializableFieldLinkedSetRow);
			}else{
				logger.debug("row "+serializableFieldListRow.getNumber()+" is not valid");
				validatedSerializableRowListTable.setIsValid(false);
				notValidRows.add(validatedSerializableFieldLinkedSetRow);
			}
		}
		logger.info("valid rows="+validRows.size()+";not valid rows="+notValidRows.size());
		validatedSerializableRowListTable.setValidRows(validRows);
		validatedSerializableRowListTable.setNotValidRows(notValidRows);

		// (2) validazione table trasversale su constraints su insieme di row
		// (2.a) istanziazione validationEntity
		ValidationEntity validationEntity = new ValidationEntity(descriptor);
		if (descriptor.getEntityConstraints()!=null){
			// (2.b) determinazione mappa constraintValidationResult per constraint
			HashMap<Constraint, ValidationResult> constraintValidationResult = new HashMap<Constraint, ValidationResult>();
			for (EntityConstraint entityConstraint : descriptor.getEntityConstraints()){
				logger.debug("entityConstraint="+entityConstraint);
				ValidationResult validationResult = this.validateByEntityConstraint(entityConstraint,input);
				if ( !validationResult.isValid() ){
					logger.debug("table is not valid agaisnt "+entityConstraint);
					validatedSerializableRowListTable.setIsValid(false);
				}
				constraintValidationResult.put(entityConstraint,validationResult);
			}
			validationEntity.setConstraintValidationResult(constraintValidationResult);
		}
		validatedSerializableRowListTable.setValidationEntity(validationEntity);
		
		logger.debug("isValid="+validatedSerializableRowListTable.getIsValid());
		
		return validatedSerializableRowListTable;
	}
	
	
	protected ValidationResult validateByEntityConstraint(EntityConstraint entityConstraint,
			                                                  SerializableRowListTable serializableRowListTable) {
		ValidationResult validationResult = null;
		try {
			ConstraintChecker<EntityConstraint> entityConstraintChecker =
				this.entityConstraintCheckerFactory.getConstraintChecker(entityConstraint);
			logger.debug("entityConstraint " + entityConstraint.getName()
					+ "-checker="+entityConstraintChecker.getClass().getName());
			validationResult = entityConstraintChecker.validate(serializableRowListTable,entityConstraint);
		} catch (Exception ex) {
			validationResult = new ValidationResult();
			validationResult.setValid(false);
			validationResult.setValidationException(ex);
		}
		return validationResult;
}
	

}
