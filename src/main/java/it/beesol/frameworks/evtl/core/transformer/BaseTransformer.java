package it.beesol.frameworks.evtl.core.transformer;

import it.beesol.frameworks.evtl.model.generic.container.field.SerializablePersistenceField;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BaseTransformer {

	protected Log logger = LogFactory.getLog(this.getClass());
	
	protected Map<String, SerializablePersistenceField> persistenceFields;

	public void setPersistenceFields(Map<String, SerializablePersistenceField> persistenceFields) {
		this.persistenceFields = persistenceFields;
	}

	public Map<String, SerializablePersistenceField> getPersistenceFields() {
		return persistenceFields;
	}
	
}