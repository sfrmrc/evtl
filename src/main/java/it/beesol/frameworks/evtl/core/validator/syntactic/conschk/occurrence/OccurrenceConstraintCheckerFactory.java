package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintCheckerFactory;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;

public interface OccurrenceConstraintCheckerFactory<OC extends OccurrenceConstraint>
       extends ConstraintCheckerFactory<ConstraintChecker<OC>,OC>{
	
	// TODO : exccezioen specifica per costrant non gestito - gerarchia di eccezioni
	
	public ConstraintChecker<OC> getConstraintChecker(OC constraint) throws Exception;

}
