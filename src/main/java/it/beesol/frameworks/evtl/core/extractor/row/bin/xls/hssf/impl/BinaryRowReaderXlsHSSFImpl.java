package it.beesol.frameworks.evtl.core.extractor.row.bin.xls.hssf.impl;

import java.awt.List;
import java.util.Iterator;
import java.util.LinkedHashSet;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import it.beesol.frameworks.evtl.core.extractor.row.bin.xls.hssf.BaseBinaryRowReaderXlsHSSF;
import it.beesol.frameworks.evtl.core.extractor.row.bin.xls.hssf.BinaryRowReaderXlsHSSF;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.RowInterface;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;

public class BinaryRowReaderXlsHSSFImpl extends BaseBinaryRowReaderXlsHSSF
		implements BinaryRowReaderXlsHSSF {

	private static final long serialVersionUID = 8753036980485548553L;

	public RowInterface read(HSSFRow hssfRow) {
		
		SerializableFieldLinkedSetRow<SerializableField> row = new SerializableFieldLinkedSetRow<SerializableField>();
		LinkedHashSet<SerializableField> fields = new LinkedHashSet<SerializableField>();
		LinkedHashSet<ConstrainedAttribute> constrainedOccurrenceList = this.constrainedOccurrence.getAttributes();
		Iterator<ConstrainedAttribute> iterator = constrainedOccurrenceList.iterator();
		
		this.provider.init(hssfRow);
		
		while(this.provider.hasNext()) {
			
			SerializableField field = (SerializableField) this.fieldReader.read((HSSFCell) this.provider.next());
			field.setName(iterator.next().getName());
			fields.add((SerializableField) field);
			
		}
		
		row.setFields(fields);
		
		return row;
		
	}


}
