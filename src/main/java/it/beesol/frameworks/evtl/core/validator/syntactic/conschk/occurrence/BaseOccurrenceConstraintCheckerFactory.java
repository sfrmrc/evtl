package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence;

import java.util.HashMap;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;


public class BaseOccurrenceConstraintCheckerFactory<OC extends OccurrenceConstraint>
      extends BaseConstraintCheckerFactory<ConstraintChecker<OC>,OC>
      implements OccurrenceConstraintCheckerFactory<OC> {
	
	private HashMap<String,ConstraintChecker<OC>> perNameConstraintCheckerMap = null; 
	
	public void setPerNameConstraintCheckerMap(
			HashMap<String, ConstraintChecker<OC>> perNameConstraintCheckerMap) {
		this.perNameConstraintCheckerMap = perNameConstraintCheckerMap;
	}





	public ConstraintChecker<OC> getConstraintChecker(OC constraint) throws Exception {
		logger.debug("getConstraintChecker:constraint "+constraint.getName());
		ConstraintChecker<OC> occurrenceConstraintChecker = perNameConstraintCheckerMap.get(constraint.getName());
		// TODO : evolvere gestione eccezioni con eccezioni custom
		if ( occurrenceConstraintChecker==null ){
			throw new Exception("no occurrenceConstraintChecker for constraint="+constraint.getName());
		}
		logger.debug("occurrenceConstraintChecker:"+occurrenceConstraintChecker);
		return occurrenceConstraintChecker;
	}

}
