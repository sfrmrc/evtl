package it.beesol.frameworks.evtl.core.extractor.row.bin.xls.hssf;

import it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf.BinaryFieldReaderXlsHSSF;
import it.beesol.frameworks.evtl.core.extractor.row.bin.xls.BaseBinaryRowReaderXls;

public abstract class BaseBinaryRowReaderXlsHSSF extends BaseBinaryRowReaderXls<BinaryFieldReaderXlsHSSF> {

}
