package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class NotNullAttributeConstraintCheckerImpl
       extends BaseConstraintChecker<NotNullConstraint>
       implements NotNullAttributeConstraintChecker {

	public ValidationResult validate(Object input,NotNullConstraint constraint) {
		if ( input==null && constraint.getShouldBeNotNull()==true){
			return new ValidationResult(false,"object is null but should be "+constraint.getShouldBeNotNull());
		}
		return new ValidationResult(true);
	}

}
