package it.beesol.frameworks.evtl.core.extractor;

public interface Reader<I extends Object, O extends Object>{

	public O read(I i);

}
