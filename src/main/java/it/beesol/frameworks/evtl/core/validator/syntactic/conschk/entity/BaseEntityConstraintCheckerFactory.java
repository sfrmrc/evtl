package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.entity;

import java.util.HashMap;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;

public class BaseEntityConstraintCheckerFactory<EC extends EntityConstraint>
      extends BaseConstraintCheckerFactory<ConstraintChecker<EC>,EC>
      implements EntityConstraintCheckerFactory<EC> {
	
	private HashMap<String,ConstraintChecker<EC>> perNameConstraintCheckerMap = null; 

	public void setPerNameConstraintCheckerMap(
			HashMap<String, ConstraintChecker<EC>> perNameConstraintCheckerMap) {
		this.perNameConstraintCheckerMap = perNameConstraintCheckerMap;
	}




	public ConstraintChecker<EC> getConstraintChecker(EC constraint) throws Exception {
		logger.debug("getConstraintChecker:constraint "+constraint.getName());
		ConstraintChecker<EC> entityConstraintChecker = perNameConstraintCheckerMap.get(constraint.getName());
		// TODO : evolvere gestione eccezioni con eccezioni custom
		if ( entityConstraintChecker==null ){
			throw new Exception("no entityConstraintChecker for constraint="+constraint.getName());
		}
		logger.debug("entityConstraintChecker:"+entityConstraintChecker);
		return entityConstraintChecker;
	}

}
