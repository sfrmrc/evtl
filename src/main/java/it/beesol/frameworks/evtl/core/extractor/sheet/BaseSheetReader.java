package it.beesol.frameworks.evtl.core.extractor.sheet;

import it.beesol.frameworks.evtl.core.extractor.BaseReader;
import it.beesol.frameworks.evtl.core.extractor.table.TableReader;
import it.beesol.frameworks.evtl.model.generic.provider.entityset.EntitySetProvider;

public class BaseSheetReader<T extends TableReader> extends BaseReader<EntitySetProvider> {

	protected T tableReader;

	public void setTableReader(T tableReader) {
		this.tableReader = tableReader;
	}

	public T getTableReader() {
		return tableReader;
	}
	
}
