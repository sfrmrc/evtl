package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;

public interface OccurrenceConstraintChecker<OC extends OccurrenceConstraint> extends
		ConstraintChecker<OccurrenceConstraint> {

}
