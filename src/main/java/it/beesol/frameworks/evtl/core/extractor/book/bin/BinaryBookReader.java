package it.beesol.frameworks.evtl.core.extractor.book.bin;

import it.beesol.frameworks.evtl.core.extractor.book.SerializableBookReader;

public interface BinaryBookReader<I> extends SerializableBookReader<I> {

}
