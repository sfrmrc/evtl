package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintCheckerFactory;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;

public interface AttributeConstraintCheckerFactory<OTC extends AttributeConstraint>
       extends ConstraintCheckerFactory<ConstraintChecker<OTC>,OTC>{
	
	// TODO : exccezioen specifica per costrant non gestito - gerarchia di eccezioni
	
	public ConstraintChecker<OTC> getConstraintChecker(OTC constraint) throws Exception;

}
