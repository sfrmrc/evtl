package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.FixedLenghtAlphaNumConstraint;

public interface FixedLenghtAlphaNumOnTypeConstraintChecker extends
		ConstraintChecker<FixedLenghtAlphaNumConstraint> {

}
