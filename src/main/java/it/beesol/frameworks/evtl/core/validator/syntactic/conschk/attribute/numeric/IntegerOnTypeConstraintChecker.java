package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.IntegerConstraint;

public interface IntegerOnTypeConstraintChecker extends ConstraintChecker<IntegerConstraint> {
	

}
