package it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import it.beesol.frameworks.evtl.core.extractor.sheet.bin.xls.BinarySheetReaderXls;

public interface BinarySheetReaderXlsHSSF extends BinarySheetReaderXls<HSSFSheet> {

}
