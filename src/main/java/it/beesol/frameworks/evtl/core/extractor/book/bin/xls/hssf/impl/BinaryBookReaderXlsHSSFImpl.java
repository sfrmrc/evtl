package it.beesol.frameworks.evtl.core.extractor.book.bin.xls.hssf.impl;

import java.util.HashSet;
import java.util.Set;

import it.beesol.frameworks.evtl.core.extractor.book.bin.xls.hssf.BaseBinaryBookReaderXlsHSSF;
import it.beesol.frameworks.evtl.core.extractor.book.bin.xls.hssf.BinaryBookReaderXlsHSSF;
import it.beesol.frameworks.evtl.model.generic.container.book.BookInterface;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class BinaryBookReaderXlsHSSFImpl extends BaseBinaryBookReaderXlsHSSF
		implements BinaryBookReaderXlsHSSF {

	private static final long serialVersionUID = 2872000509986470564L;

	public SerializableSheetSetBook read(HSSFWorkbook workbookHssf) {
		
		SerializableSheetSetBook book = new SerializableSheetSetBook();
		Set<SerializableTableSetSheet> sheets = new HashSet<SerializableTableSetSheet>();
		
		this.provider.init(workbookHssf);
		
		while(this.provider.hasNext()) {
			
			sheets.add((SerializableTableSetSheet) this.sheetReader.read((HSSFSheet) this.provider.next()));
			
		}
		
		book.setSheet(sheets);

		return book;
		
	}

}
