package it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf.impl;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;

import it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf.BaseBinaryFieldReaderXlsHSSF;
import it.beesol.frameworks.evtl.core.extractor.field.bin.xls.hssf.BinaryFieldReaderXlsHSSF;
import it.beesol.frameworks.evtl.model.generic.container.field.FieldInterface;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;

public class BinaryFieldReaderXlsHSSFImpl extends BaseBinaryFieldReaderXlsHSSF
		implements BinaryFieldReaderXlsHSSF {

	private static final long serialVersionUID = -7628097986965348654L;

	public FieldInterface read(HSSFCell cellHssf) {

		SerializableField field = new SerializableField();

		if (cellHssf != null) {
			
			if(cellHssf.getCellType() == HSSFCell.CELL_TYPE_NUMERIC && HSSFDateUtil.isCellDateFormatted(cellHssf))
				field.setValue(cellHssf.getDateCellValue());
			else if (cellHssf.getCellType() == HSSFCell.CELL_TYPE_BOOLEAN)
				field.setValue(cellHssf.getBooleanCellValue());
			else if (cellHssf.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
				field.setValue(cellHssf.getNumericCellValue());
			else if (cellHssf.getCellType() == HSSFCell.CELL_TYPE_BLANK)
				field.setValue(null);
			else if (cellHssf.getCellType() == HSSFCell.CELL_TYPE_STRING)
				field.setValue(cellHssf.getRichStringCellValue().toString());
		}
		
		return field;

	}

}