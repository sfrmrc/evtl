package it.beesol.frameworks.evtl.core.extractor.book.bin.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import it.beesol.frameworks.evtl.core.extractor.book.bin.xls.BinaryBookReaderXls;

public interface BinaryBookReaderXlsHSSF
       extends BinaryBookReaderXls<HSSFWorkbook> {

}
