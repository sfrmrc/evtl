package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.numeric;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric.InRangeIntegerConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class InRangeIntegerOnTypeConstraintCheckerImpl
        extends BaseConstraintChecker<InRangeIntegerConstraint>
		implements InRangeIntegerOnTypeConstraintChecker {
	
	
	public ValidationResult validate(Object input,InRangeIntegerConstraint constraint){
		logger.debug("validate: constraint="+constraint);
		boolean isValid = false;
		if (input!=null){
			try{
				Integer intToValidate = (Integer)input;
				if ( constraint.getBoundaryIncluded() ){
					if ( constraint.getMin()<=intToValidate && intToValidate<=constraint.getMax()){
						isValid = true;
					}
				}else{
					if ( constraint.getMin()<intToValidate && intToValidate<constraint.getMax()){
						isValid = true;
					}
				}	
			}catch(ClassCastException ccex){
				// This catch protect against this cosntraint applied without having applied before IntegerTypeConstraint
				// could be removed wne input will be typed with generics notation (vedi TODOS)
				logger.warn("input type "+input.getClass().getName()+" different from "+Integer.class.getName()+" expected");
			}catch(NumberFormatException nfex){
				logger.warn("input value ="+input+"; not allowed");
			}
		}
		return new ValidationResult(isValid);
	}
	
	
}
