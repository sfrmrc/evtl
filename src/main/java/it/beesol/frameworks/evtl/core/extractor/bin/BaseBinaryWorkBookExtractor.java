package it.beesol.frameworks.evtl.core.extractor.bin;

import java.io.InputStream;

import it.beesol.frameworks.evtl.core.extractor.BaseBookExtractor;
import it.beesol.frameworks.evtl.core.extractor.BaseExtractor;
import it.beesol.frameworks.evtl.core.extractor.BaseSerializableBookExtractor;
import it.beesol.frameworks.evtl.core.extractor.book.bin.BinaryBookReader;
import it.beesol.frameworks.evtl.model.generic.container.book.Book;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;

public abstract class BaseBinaryWorkBookExtractor<O extends Object>
       extends BaseSerializableBookExtractor<BinaryBookReader<O>,InputStream,O>
       implements BinaryWorkBookExtractor {

	private static final long serialVersionUID = -8628470160768057218L;


}
