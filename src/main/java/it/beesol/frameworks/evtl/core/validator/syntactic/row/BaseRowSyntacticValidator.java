package it.beesol.frameworks.evtl.core.validator.syntactic.row;

import it.beesol.frameworks.evtl.core.validator.syntactic.BaseSyntacticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.occurrence.OccurrenceConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.field.FieldSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ValidationAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ValidationOccurrence;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class BaseRowSyntacticValidator
       extends BaseSyntacticValidator<ValidatedSerializableFieldLinkedSetRow,
                                      SerializableFieldLinkedSetRow<SerializableField>,ConstrainedOccurrence>
       implements RowSyntacticValidator{
	
	private FieldSyntacticValidator fieldSyntacticValidator = null;
	
	public void setFieldSyntacticValidator(
			FieldSyntacticValidator fieldSyntacticValidator) {
		this.fieldSyntacticValidator = fieldSyntacticValidator;
	}

	private OccurrenceConstraintCheckerFactory<OccurrenceConstraint> occurrenceConstraintCheckerFactory = null;
	
	public void setOccurrenceConstraintCheckerFactory(
			OccurrenceConstraintCheckerFactory<OccurrenceConstraint> occurrenceConstraintCheckerFactory) {
		this.occurrenceConstraintCheckerFactory = occurrenceConstraintCheckerFactory;
	}

	private boolean stopWhenFoundFirstNotValid = false;
	
	
	public void setStopWhenFoundFirstNotValid(boolean stopWhenFoundFirstNotValid) {
		this.stopWhenFoundFirstNotValid = stopWhenFoundFirstNotValid;
	}

	public ValidatedSerializableFieldLinkedSetRow validate(SerializableFieldLinkedSetRow<SerializableField> input,
			                                               ConstrainedOccurrence constrainedOccurrence)
	   throws Exception{

		logger.info("start validating "+input);

		// (0) istanziazione validatedSerializableFieldListRow di out
		ValidatedSerializableFieldLinkedSetRow validatedSerializableFieldListRow = new ValidatedSerializableFieldLinkedSetRow(input);
		validatedSerializableFieldListRow.setIsValid(true);
		
		// (1) validazione row su singoli fields
		// (1.a) istanziazione mappa validatedSerializableFieldMap
		LinkedHashSet<ValidatedSerializableField> validatedFields = new LinkedHashSet<ValidatedSerializableField>();
		// (1.b) determinazione mappa validatedSerializableFieldMap
		Iterator<SerializableField> fieldIterator = input.getFields().iterator();
		while(fieldIterator.hasNext()){
			SerializableField serializableField = fieldIterator.next();
			ValidatedSerializableField validatedSerializableField = null;
			logger.debug("field "+serializableField.getName());
			ConstrainedAttribute constrainedAttribute = constrainedOccurrence.getConstrainedAttributes().get(serializableField.getName());
			if ( constrainedAttribute==null){
				logger.info("no constrainedAttribute for serializableField="+serializableField+":could not be verified");
				validatedFields.add(new ValidatedSerializableField(serializableField));
			}else{
				logger.debug("constrainedAttribute="+constrainedAttribute);
				if ( stopWhenFoundFirstNotValid && !validatedSerializableFieldListRow.getIsValid() ){
					logger.debug("already found field not valid : field "+serializableField.getName()+" will not be verified");
					validatedSerializableField = new ValidatedSerializableField(serializableField);
					validatedSerializableField.setValidationAttribute(new ValidationAttribute(constrainedAttribute));
					validatedFields.add(validatedSerializableField);
				}else{
					logger.debug("start validating field");
					validatedSerializableField = this.fieldSyntacticValidator.validate(serializableField,constrainedAttribute);	
					if ( !validatedSerializableField.getIsValid() ){
						logger.warn("row is not valid for field ="+serializableField.getName());
						validatedSerializableFieldListRow.setIsValid(false);
					}
					validatedFields.add(validatedSerializableField);	
				}			
			}	
		}
		
		// (1.c) set mappa validatedSerializableFieldMap in validatedSerializableFieldListRow
		validatedSerializableFieldListRow.setFields(validatedFields);
		
		// (2) validazione row trasversale (su constraint su insieme di fields)
		// (2.a) istanziazione validationOccurrence
		ValidationOccurrence validationOccurrence = new ValidationOccurrence(constrainedOccurrence);
		// (2.b) determinazione mappa constraintValidationResult per constraint
		if (constrainedOccurrence.getOccurrenceConstraints()!=null){
			if ( stopWhenFoundFirstNotValid && !validatedSerializableFieldListRow.getIsValid() ){
				logger.debug("already found field not valid : occurrenceConstraints will not be verified");
			}else{
				logger.debug("start validating with occurrenceConstraints");
				HashMap<Constraint, ValidationResult> constraintValidationResult = new HashMap<Constraint, ValidationResult>();
				for(OccurrenceConstraint occurrenceConstraint: constrainedOccurrence.getOccurrenceConstraints()){
					if ( stopWhenFoundFirstNotValid && !validatedSerializableFieldListRow.getIsValid() ){
						logger.debug("already found not valid : occurrenceConstraint="+occurrenceConstraint+" will not be verified");
						constraintValidationResult.put(occurrenceConstraint,null);
					}else{
						logger.debug("validating against occurrenceConstraint="+occurrenceConstraint);
						ValidationResult validationResult = this.validateByOccurrenceConstraint(occurrenceConstraint,input);
						if ( !validationResult.isValid() ){
							logger.warn("row is not valid against occurrenceConstraint="+occurrenceConstraint);
							validatedSerializableFieldListRow.setIsValid(false);
						}
						constraintValidationResult.put(occurrenceConstraint,validationResult);	
					}
				}
				// (2.c) set mappa constraintValidationResult in validationOccurrence
				validationOccurrence.setConstraintValidationResult(constraintValidationResult);	
			}
		}
		// (2.d) set validationOccurrence in validatedSerializableFieldListRow		
		validatedSerializableFieldListRow.setValidationOccurrence(validationOccurrence);
		logger.debug("validatedSerializableFieldListRow.isValid="+validatedSerializableFieldListRow.getIsValid());
		return validatedSerializableFieldListRow;
	}

	protected ValidationResult validateByOccurrenceConstraint(OccurrenceConstraint occurrenceConstraint,
			                                                  SerializableFieldLinkedSetRow<SerializableField> serializableFieldListRow) {
		ValidationResult validationResult = null;
		try {
			ConstraintChecker<OccurrenceConstraint> occurrenceConstraintChecker =
				    this.occurrenceConstraintCheckerFactory.getConstraintChecker(occurrenceConstraint);
			logger.debug("occurrenceConstraint " + occurrenceConstraint.getName()
					+ "-checker="+occurrenceConstraintChecker.getClass().getName());
			validationResult = occurrenceConstraintChecker.validate(serializableFieldListRow,occurrenceConstraint);
		} catch (Exception ex) {
			validationResult = new ValidationResult();
			validationResult.setValid(false);
			validationResult.setValidationException(ex);
		}
		return validationResult;
	}

}
