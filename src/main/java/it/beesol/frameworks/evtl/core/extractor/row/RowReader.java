package it.beesol.frameworks.evtl.core.extractor.row;

import it.beesol.frameworks.evtl.core.extractor.Reader;
import it.beesol.frameworks.evtl.model.generic.container.row.RowInterface;

public interface RowReader<I>
		extends Reader<I, RowInterface> {

}
