package it.beesol.frameworks.evtl.core.extractor.table.bin.xls.hssf.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import it.beesol.frameworks.evtl.core.extractor.table.bin.xls.hssf.BaseBinaryTableReaderXlsHSSF;
import it.beesol.frameworks.evtl.core.extractor.table.bin.xls.hssf.BinaryTableReaderXlsHSSF;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.TableInterface;

public class BinaryTableReaderXlsHSSFImpl extends BaseBinaryTableReaderXlsHSSF implements
		BinaryTableReaderXlsHSSF {

	public TableInterface read(HSSFSheet sheetHssf) {

		SerializableRowListTable row = new SerializableRowListTable();
		List<SerializableFieldLinkedSetRow<SerializableField>> rows = new ArrayList<SerializableFieldLinkedSetRow<SerializableField>>();
		
		this.provider.init(sheetHssf);
		
		while(this.provider.hasNext()) {
			
			rows.add((SerializableFieldLinkedSetRow<SerializableField>) this.rowReader.read((HSSFRow) this.provider.next()));
			
		}
		
		row.setRows(rows);
		
		return row;
		
	}

}
