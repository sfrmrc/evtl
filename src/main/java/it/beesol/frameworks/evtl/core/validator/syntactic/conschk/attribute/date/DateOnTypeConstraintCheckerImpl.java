package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.date;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.date.DateConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class DateOnTypeConstraintCheckerImpl
        extends BaseConstraintChecker<DateConstraint>
		implements DateOnTypeConstraintChecker {
	
	// TODO : rinominare in TypeOnTypeConstraintCheckerImpl (anche interfaccia) e spostare in package superiore
	
	public ValidationResult validate(Object input,DateConstraint constraint){
		logger.debug("validate: constraint="+constraint);
		boolean isValid = false;
		try{
			Class expectedClass = Class.forName(constraint.getType().getJavaFqn());
			//logger.debug("expectedClass="+expectedClass);
			if ( input!=null && expectedClass.equals(input.getClass()) ){
				isValid = true;
			}	
		}catch(Exception ex){
			// TODO : migliorare gestione eccezioni
			isValid = false;
		}
		return new ValidationResult(isValid);
	}
	
	
}
