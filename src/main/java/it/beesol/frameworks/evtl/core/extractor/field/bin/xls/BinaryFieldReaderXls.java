package it.beesol.frameworks.evtl.core.extractor.field.bin.xls;

import it.beesol.frameworks.evtl.core.extractor.field.bin.BinaryFieldReader;

public interface BinaryFieldReaderXls<I> extends BinaryFieldReader<I> {

}
