package it.beesol.frameworks.evtl.core.extractor.field;

import it.beesol.frameworks.evtl.core.extractor.Reader;
import it.beesol.frameworks.evtl.model.generic.container.field.FieldInterface;

public interface FieldReader<I>
		extends Reader<I, FieldInterface> {

}
