package it.beesol.frameworks.evtl.core.validator.syntactic;

import it.beesol.frameworks.evtl.core.validator.Validator;
import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;


public interface SyntacticValidator<O extends Object,I extends Object,D extends Descriptor> extends Validator<O,I> {
	
	public O validate(I input,D descriptor) throws Exception;
 
	
}
 
