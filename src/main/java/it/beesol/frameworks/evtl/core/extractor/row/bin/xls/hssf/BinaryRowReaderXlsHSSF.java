package it.beesol.frameworks.evtl.core.extractor.row.bin.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFRow;

import it.beesol.frameworks.evtl.core.extractor.row.bin.xls.BinaryRowReaderXls;

public interface BinaryRowReaderXlsHSSF extends BinaryRowReaderXls<HSSFRow> {

}
