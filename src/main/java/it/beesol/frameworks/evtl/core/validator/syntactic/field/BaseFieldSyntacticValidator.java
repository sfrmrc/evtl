package it.beesol.frameworks.evtl.core.validator.syntactic.field;

import it.beesol.frameworks.evtl.core.validator.syntactic.BaseSyntacticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.AttributeConstraintCheckerFactory;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ValidationAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.NotNullConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class BaseFieldSyntacticValidator
       extends BaseSyntacticValidator<ValidatedSerializableField,SerializableField,ConstrainedAttribute>
       implements FieldSyntacticValidator{

	private AttributeConstraintCheckerFactory<AttributeConstraint> attributeConstraintCheckerFactory = null;

	public void setAttributeConstraintCheckerFactory(
			AttributeConstraintCheckerFactory<AttributeConstraint> attributeConstraintCheckerFactory) {
		this.attributeConstraintCheckerFactory = attributeConstraintCheckerFactory;
	}

	public ValidatedSerializableField validate(SerializableField input,ConstrainedAttribute constrainedAttribute)
	    throws Exception{

		// TODO : in classi derivate costruttori da superclasse
		logger.debug("validating " + input);

		ValidatedSerializableField validatedSerializableField = new ValidatedSerializableField(input);
		validatedSerializableField.setIsValid(true);

		ValidationAttribute validationAttribute = new ValidationAttribute(constrainedAttribute);
		
		HashMap<Constraint, ValidationResult> constraintValidationResult = new HashMap<Constraint, ValidationResult>();
		List<AttributeConstraint> onTypeConstraints = constrainedAttribute.getAttributeConstraints();
		if (onTypeConstraints != null) {
			for (AttributeConstraint onTypeConstraint : onTypeConstraints) {
				logger.debug("onTypeConstraint " + onTypeConstraint.getName());
				ValidationResult validationResult = 
						this.validateByFieldConstraint(onTypeConstraint,input.getValue());
				constraintValidationResult.put(onTypeConstraint,validationResult);
				if ( onTypeConstraint.getName().equals(NotNullConstraint.CONSTRAINT_NAME) ){
					if ( ((NotNullConstraint)onTypeConstraint).getShouldBeNotNull()==false
							&& input.getValue()==null ){
						logger.info("field="+input.getName()+" could be null and has value null :"+
								    " skipping all following constraints checks");
						break;
					}
				}
				if (!validationResult.isValid()) {
					logger.warn("field="+input.getName()+" with value="+input.getValue()+
							    " NOT VALID against constraint="+onTypeConstraint.getName()+
							    " exception="+validationResult.getValidationException());
					validatedSerializableField.setIsValid(false);
				}
			}
		}
		validationAttribute.setConstraintValidationResult(constraintValidationResult);
		validatedSerializableField.setValidationAttribute(validationAttribute);

		return validatedSerializableField;
	}

	protected ValidationResult validateByFieldConstraint(
			AttributeConstraint onTypeConstraint, Serializable value) {
		ValidationResult validationResult = null;
		try {
			ConstraintChecker<AttributeConstraint> onTypeConstraintChecker = this.attributeConstraintCheckerFactory
					.getConstraintChecker(onTypeConstraint);
			logger.debug("onTypeConstraint " + onTypeConstraint.getName()
					+ "-checker="
					+ onTypeConstraintChecker.getClass().getName());
			logger.debug("checker=" + onTypeConstraintChecker);
			validationResult = onTypeConstraintChecker.validate(value,onTypeConstraint);
		} catch (Exception ex) {
			validationResult = new ValidationResult();
			validationResult.setValid(false);
			validationResult.setValidationException(ex);
		}
		return validationResult;
	}

}
