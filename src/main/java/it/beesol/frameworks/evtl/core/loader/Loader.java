package it.beesol.frameworks.evtl.core.loader;

import it.beesol.frameworks.evtl.model.generic.GenericModel;

public interface Loader<I, O> extends GenericModel {

	public O store(I input);
	
	public O get();
	
}