package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;


import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.RangeLenghtAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class RangeLenghtAlphaNumOnTypeConstraintCheckerImpl extends
		BaseConstraintChecker<RangeLenghtAlphaNumConstraint> implements
		RangeLenghtAlphaNumOnTypeConstraintChecker {

	public ValidationResult validate(Object input,
			RangeLenghtAlphaNumConstraint constraint) {
		if ( input==null ){
			return new ValidationResult(false,"input is null");
		}
		String inputAsString = (String) input;
		if (constraint.getBoundaryIncluded()) {
			if (inputAsString.length() < constraint.getMinLenght()
					|| constraint.getMaxLenght() < inputAsString.length()) {
				return new ValidationResult(false, "input " + inputAsString
						+ " lenght is " + inputAsString.length()
						+ "and not in [" + constraint.getMinLenght() + ","
						+ constraint.getMaxLenght() + "] as expected");
			}
		} else {
			if (inputAsString.length() <= constraint.getMinLenght()
					|| constraint.getMaxLenght() <= inputAsString.length()) {
				return new ValidationResult(false, "input " + inputAsString
						+ " lenght is " + inputAsString.length()
						+ "and not in (" + constraint.getMinLenght() + ","
						+ constraint.getMaxLenght() + ") as expected");
			}
		}
		return new ValidationResult(true);
	}

}
