package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.date;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.date.DateConstraint;

public interface DateOnTypeConstraintChecker extends ConstraintChecker<DateConstraint> {
	

}
