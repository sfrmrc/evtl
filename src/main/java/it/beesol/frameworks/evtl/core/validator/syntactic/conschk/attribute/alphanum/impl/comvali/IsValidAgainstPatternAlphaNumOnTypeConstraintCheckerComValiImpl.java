package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.impl.comvali;

import java.util.Date;

import org.apache.commons.validator.routines.DateValidator;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum.IsValidAgainstPatternAlphaNumOnTypeConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.IsValidAgainstPatternAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class IsValidAgainstPatternAlphaNumOnTypeConstraintCheckerComValiImpl
		extends BaseConstraintChecker<IsValidAgainstPatternAlphaNumConstraint>
		implements IsValidAgainstPatternAlphaNumOnTypeConstraintChecker {

	public ValidationResult validate(Object input,
			IsValidAgainstPatternAlphaNumConstraint constraint) {
		String inputAsString = (String) input;
		DateValidator validator = DateValidator.getInstance();
		Date dateValidate = validator.validate(inputAsString,
				constraint.getPattern(), constraint.getLocale(),
				constraint.getTimezone());
		if (dateValidate != null) {
			return new ValidationResult(true);
		}
		return new ValidationResult(false);
	}

}
