package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute;

import java.util.HashMap;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintCheckerFactory;
import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.ConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class BaseAttributeConstraintCheckerFactory<OTC extends AttributeConstraint>
      extends BaseConstraintCheckerFactory<ConstraintChecker<OTC>,OTC>
      implements AttributeConstraintCheckerFactory<OTC> {
	
	private HashMap<Type, HashMap<String,ConstraintChecker<OTC>>> perTypeConstraintCheckerMap = null; 
	
	public void setPerTypeConstraintCheckerMap(
			HashMap<Type, HashMap<String,ConstraintChecker<OTC>>> perTypeConstraintCheckerMap) {
		this.perTypeConstraintCheckerMap = perTypeConstraintCheckerMap;
	}



	public ConstraintChecker<OTC> getConstraintChecker(OTC constraint) throws Exception {
		logger.debug("getConstraintChecker:constraint "+constraint.getName());
		logger.debug("getConstraintChecker:constraint.getType "+constraint.getType());
		ConstraintChecker<OTC> onTypeConstraintChecker = perTypeConstraintCheckerMap.get(constraint.getType()).get(constraint.getName());
		// TODO : evolvere gestione eccezioni con eccezioni custom
		if ( onTypeConstraintChecker==null ){
			throw new Exception("no onTypeConstraintChecker for type="+constraint.getType()+" and constraint="+constraint.getName());
		}
		logger.debug("onTypeConstraintChecker:"+onTypeConstraintChecker);
		return onTypeConstraintChecker;
	}

}
