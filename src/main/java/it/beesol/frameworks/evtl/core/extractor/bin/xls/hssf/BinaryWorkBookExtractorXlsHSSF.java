package it.beesol.frameworks.evtl.core.extractor.bin.xls.hssf;

import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import it.beesol.frameworks.evtl.core.extractor.bin.BaseBinaryWorkBookExtractor;
import it.beesol.frameworks.evtl.core.extractor.bin.BinaryWorkBookExtractor;

public class BinaryWorkBookExtractorXlsHSSF extends
		BaseBinaryWorkBookExtractor<HSSFWorkbook> implements BinaryWorkBookExtractor {

	private static final long serialVersionUID = 1054210756699770693L;

	@Override
	public HSSFWorkbook extract(InputStream input)  throws Exception{
		try {
			POIFSFileSystem poiFS = new POIFSFileSystem(input);
			return new HSSFWorkbook(poiFS);
		} catch (IOException e) {
			logger.error("error in extract:"+e.getMessage(),e);
			throw e;
		}
	}

}
