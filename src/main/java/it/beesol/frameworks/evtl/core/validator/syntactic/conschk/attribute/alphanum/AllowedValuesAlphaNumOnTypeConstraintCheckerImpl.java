package it.beesol.frameworks.evtl.core.validator.syntactic.conschk.attribute.alphanum;

import it.beesol.frameworks.evtl.core.validator.syntactic.conschk.BaseConstraintChecker;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum.AllowedValuesAlphaNumConstraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

public class AllowedValuesAlphaNumOnTypeConstraintCheckerImpl
       extends BaseConstraintChecker<AllowedValuesAlphaNumConstraint>
       implements AllowedValuesAlphaNumOnTypeConstraintChecker {

	public ValidationResult validate(Object input,AllowedValuesAlphaNumConstraint constraint) {
		String inputAsString = (String)input;
		if (!constraint.getAllowedValues().contains(inputAsString)){
			return new ValidationResult(false,"input "+inputAsString+" is not contained in"+constraint.getAllowedValues()+" as expected");	
		}
		return new ValidationResult(true);
	}

}
