package it.beesol.frameworks.evtl.controller.xls;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import it.beesol.frameworks.evtl.controller.BaseController;
import it.beesol.frameworks.evtl.core.extractor.bin.BinaryWorkBookExtractor;
import it.beesol.frameworks.evtl.core.loader.Loader;
import it.beesol.frameworks.evtl.core.transformer.ognl.TransformerOgnl;
import it.beesol.frameworks.evtl.core.validator.semantic.SemanticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.table.TableSyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.container.book.SerializableSheetSetBook;
import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;
import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;
import it.beesol.frameworks.evtl.model.generic.container.table.ValidatedSerializableRowListTable;

/**
 * ControllerXls without SemanticValidator and Loader
 * 
 * 1.BinaryWorkBookExtractor
 * 2.TableSyntacticValidator
 * 3.TransformerOgnl
 * 
 * @author marco
 *
 */
public class ControllerXls extends BaseController<InputStream, 
                    BinaryWorkBookExtractor, 
                    SemanticValidator, 
                    TableSyntacticValidator, 
                    TransformerOgnl, 
                    Loader> {
  
  private List<ValidatedSerializableRowListTable> validatedSerializableRowListTableList;
  private List<ValidatedSerializableFieldLinkedSetRow> notValidRows;
  private List<ValidatedSerializableFieldLinkedSetRow> validRows;

  public void run(InputStream input) throws Exception {

    long startedTime = System.currentTimeMillis();
    
    logger.debug("start: "+startedTime);
    
    SerializableSheetSetBook book = this.extractor.run(input);
    
    Set<SerializableTableSetSheet> sheets = book.getSheet();
    
    validatedSerializableRowListTableList = new ArrayList<ValidatedSerializableRowListTable>();
    notValidRows = new ArrayList<ValidatedSerializableFieldLinkedSetRow>();
    validRows = new ArrayList<ValidatedSerializableFieldLinkedSetRow>();

    logger.debug("validating sheets");
    
    for (SerializableTableSetSheet sheet : sheets) {

      Set<SerializableRowListTable> tables = sheet.getTables();

      for (SerializableRowListTable table : tables)
        validatedSerializableRowListTableList.add(this.syntacticValidator.validate(table, this.constrainedEntity));

    }
    
    logger.debug("converting rows in persistenObject");
    
    for(ValidatedSerializableRowListTable validatedSerializableRowListTable : validatedSerializableRowListTableList) {
      
      validRows = validatedSerializableRowListTable.getValidRows();
      notValidRows = validatedSerializableRowListTable.getNotValidRows();
      
      for (ValidatedSerializableFieldLinkedSetRow validatedRow : validRows) {

        Object persitenceObj = this.transformer.run(validatedRow);
        this.loader.store(persitenceObj);
        logger.debug(persitenceObj);
        
      }
      
    }

    logger.debug("stop: "+(System.currentTimeMillis()-startedTime));
    
  }
  
  public List<ValidatedSerializableRowListTable> getValidatedRows() {
    return validatedSerializableRowListTableList;
  }
  
  public List<ValidatedSerializableFieldLinkedSetRow> getNotValidRows() {
    return notValidRows;
  }
  
  public List<ValidatedSerializableFieldLinkedSetRow> getValidRows() {
    return notValidRows;
  }
  
}
