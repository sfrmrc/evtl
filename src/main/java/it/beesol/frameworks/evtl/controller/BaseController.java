package it.beesol.frameworks.evtl.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import it.beesol.frameworks.evtl.core.extractor.Extractor;
import it.beesol.frameworks.evtl.core.loader.Loader;
import it.beesol.frameworks.evtl.core.transformer.Transformer;
import it.beesol.frameworks.evtl.core.validator.semantic.SemanticValidator;
import it.beesol.frameworks.evtl.core.validator.syntactic.SyntacticValidator;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ConstrainedEntity;

public abstract class BaseController<I, 
                E extends Extractor, 
                Se extends SemanticValidator, 
                Sy extends SyntacticValidator, 
                T extends Transformer, 
                L extends Loader>
    implements Controller<I> {
  
  protected Log logger = LogFactory.getLog(this.getClass());

  protected E extractor;
  protected Se semanticValidator;
  protected Sy syntacticValidator;
  protected T transformer;
  protected L loader;
  protected ConstrainedEntity constrainedEntity;

  public void setExtractor(E extractor) {
    this.extractor = extractor;
  }

  public E getExtractor() {
    return extractor;
  }

  public void setSemanticValidator(Se semanticValidator) {
    this.semanticValidator = semanticValidator;
  }

  public Se getSemanticValidator() {
    return semanticValidator;
  }

  public void setSyntacticValidator(Sy syntacticValidator) {
    this.syntacticValidator = syntacticValidator;
  }

  public Sy getSyntacticValidator() {
    return syntacticValidator;
  }

  public void setTransformer(T transformer) {
    this.transformer = transformer;
  }

  public T getTransformer() {
    return transformer;
  }

  public void setLoader(L loader) {
    this.loader = loader;
  }

  public L getLoader() {
    return loader;
  }

  public void setConstrainedEntity(ConstrainedEntity constrainedEntity) {
    this.constrainedEntity = constrainedEntity;
  }

  public ConstrainedEntity getConstrainedEntity() {
    return constrainedEntity;
  }
  
}
