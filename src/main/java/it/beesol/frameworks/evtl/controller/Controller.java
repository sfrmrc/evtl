package it.beesol.frameworks.evtl.controller;

public interface Controller<I> {

  public void run(I input) throws Exception;
  
}
