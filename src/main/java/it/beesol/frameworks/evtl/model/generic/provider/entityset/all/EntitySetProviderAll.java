package it.beesol.frameworks.evtl.model.generic.provider.entityset.all;

import it.beesol.frameworks.evtl.model.generic.provider.entityset.EntitySetProvider;

public interface EntitySetProviderAll<C, E> extends
		EntitySetProvider<C, E, EntitySetAll> {

}
