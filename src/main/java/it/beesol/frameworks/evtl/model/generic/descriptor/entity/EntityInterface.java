package it.beesol.frameworks.evtl.model.generic.descriptor.entity;

import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public interface EntityInterface<T extends Type> extends Descriptor {

}
