package it.beesol.frameworks.evtl.model.generic.provider.attribute;

import it.beesol.frameworks.evtl.model.generic.provider.Provider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.Rule;

public interface AttributeProvider<C, E, R extends Rule> 
		extends Provider<C, E, R> {

}
