package it.beesol.frameworks.evtl.model.generic.descriptor.occurrence;

import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.Attribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

import java.util.LinkedHashSet;


public class Occurrence<A extends Attribute<Type>> implements OccurrenceInterface {
	
	protected LinkedHashSet<A> attributes;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LinkedHashSet<A> getAttributes() {
		return attributes;
	}

	public void setAttributes(LinkedHashSet<A> attributes) {
		this.attributes = attributes;
	}
	
	
	 
}
 
