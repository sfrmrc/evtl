package it.beesol.frameworks.evtl.model.generic.descriptor.occurrence;

import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.occurrence.OccurrenceConstraint;

public class ConstrainedOccurrence extends Occurrence<ConstrainedAttribute>{
	
	protected Hashtable<String,ConstrainedAttribute> constrainedAttributes = null;

	private List<OccurrenceConstraint> occurrenceConstraints = null;

	public Hashtable<String, ConstrainedAttribute> getConstrainedAttributes() {
		return constrainedAttributes;
	}

	public void setConstrainedAttributes(
			Hashtable<String, ConstrainedAttribute> constrainedAttributes) {
		this.constrainedAttributes = constrainedAttributes;
	}

	public List<OccurrenceConstraint> getOccurrenceConstraints() {
		return occurrenceConstraints;
	}

	public void setOccurrenceConstraints(
			List<OccurrenceConstraint> occurrenceConstraints) {
		this.occurrenceConstraints = occurrenceConstraints;
	}

	
	

}
