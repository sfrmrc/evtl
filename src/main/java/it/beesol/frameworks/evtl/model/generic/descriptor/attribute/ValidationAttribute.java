package it.beesol.frameworks.evtl.model.generic.descriptor.attribute;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import java.util.HashMap;

public class ValidationAttribute extends ConstrainedAttribute {

	private HashMap<Constraint, ValidationResult> constraintValidationResult = null;

	public HashMap<Constraint, ValidationResult> getConstraintValidationResult() {
		return constraintValidationResult;
	}

	public void setConstraintValidationResult(
			HashMap<Constraint, ValidationResult> constraintValidationResult) {
		this.constraintValidationResult = constraintValidationResult;
	}
	
	public ValidationAttribute(){}
	
	public ValidationAttribute(ConstrainedAttribute constrainedAttribute){
		this.setName(constrainedAttribute.getName());
		this.setType(constrainedAttribute.getType());
		this.setAttributeConstraints(constrainedAttribute.getAttributeConstraints());
	}

}
