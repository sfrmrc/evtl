package it.beesol.frameworks.evtl.model.generic.provider.dataset.all;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.DataSetProvider;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.DataSetAll;

public interface DataSetProviderAll<C, E> extends DataSetProvider<C, E, DataSetAll> {

}
