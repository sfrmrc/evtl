package it.beesol.frameworks.evtl.model.generic.validation;

import it.beesol.frameworks.evtl.model.generic.GenericModel;

public class ValidationResult implements GenericModel {
 
	private Boolean isValid = null;
	 
	private Exception validationException;
	 
	private String validationMessage;

	public Boolean isValid() {
		return isValid;
	}

	public void setValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public Exception getValidationException() {
		return validationException;
	}

	public void setValidationException(Exception validationException) {
		this.validationException = validationException;
	}

	public String getValidationMessage() {
		return validationMessage;
	}

	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}
	
	public ValidationResult(){};
	
	public ValidationResult(Boolean isValid){
		this.isValid=isValid;
	};
	
	public ValidationResult(Boolean isValid,String validationMessage){
		this.isValid=isValid;
		this.validationMessage=validationMessage;
	};
	
	public ValidationResult(Exception validationException){
		this.validationException=validationException;
	};
	
	

	 
	 
}
 
