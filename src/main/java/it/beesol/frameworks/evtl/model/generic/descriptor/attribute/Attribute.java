package it.beesol.frameworks.evtl.model.generic.descriptor.attribute;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class Attribute<T extends Type> implements AttributeInterface<T> {
 
	private String name;
	 
	private T type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public T getType() {
		return type;
	}

	public void setType(T type) {
		this.type = type;
	}
	 
}
 
