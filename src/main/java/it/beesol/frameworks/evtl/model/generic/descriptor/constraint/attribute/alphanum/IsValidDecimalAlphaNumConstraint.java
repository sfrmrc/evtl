package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;


public class IsValidDecimalAlphaNumConstraint extends IsValidIntegerAlphaNumConstraint {
	
	public final static String CONSTRAINT_NAME = "IsValidDecimalAlphaNumConstraint";
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CONSTRAINT_NAME;
	}
		
	protected String pattern = "#,##0.#;#,##0.#";

	@Override
	public String getPattern() {
		// TODO Auto-generated method stub
		return this.pattern;
	}
	
	
	

	
}
