package it.beesol.frameworks.evtl.model.generic.container.field;

public class SerializablePersistenceField implements FieldInterface {

	private static final long serialVersionUID = -1046911703151513387L;
	
	protected String name;
	protected String persistence;

	public void setPersistence(String persistence) {
		this.persistence = persistence;
	}

	public String getPersistence() {
		return persistence;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
			.append("[")
			.append("name:").append(name)
			.append(", persistence:").append(persistence)
			.append("]")
			.toString();
	}

}
