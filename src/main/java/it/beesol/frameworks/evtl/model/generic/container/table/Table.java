package it.beesol.frameworks.evtl.model.generic.container.table;

import java.util.Collection;

import it.beesol.frameworks.evtl.model.generic.container.field.Field;
import it.beesol.frameworks.evtl.model.generic.container.row.Row;

public class Table<T extends Field, H extends Collection<T>, R extends Row<T,H>, C extends Collection<R>>
       implements TableInterface{
	
  private static final long serialVersionUID = -241652335763804773L;

  protected String name = null;
	
	protected C rows;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public C getRows() {
		return rows;
	}

	public void setRows(C rows) {
		this.rows = rows;
	}

}
