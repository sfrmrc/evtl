package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.ObjectType;

public class NotNullConstraint extends AttributeConstraint {
	
    public final static String CONSTRAINT_NAME = "NotNullConstraint";
    
    private Boolean shouldBeNotNull = null;
    
	public Boolean getShouldBeNotNull() {
		return shouldBeNotNull;
	}

	public void setShouldBeNotNull(Boolean shouldBeNotNull) {
		this.shouldBeNotNull = shouldBeNotNull;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CONSTRAINT_NAME;
	}
	
	public 	NotNullConstraint(){
		this.type = new ObjectType();
	}
	
}
