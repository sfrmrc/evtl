package it.beesol.frameworks.evtl.model.generic.provider.dataset;

import it.beesol.frameworks.evtl.model.generic.provider.Provider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.Rule;

public interface DataSetProvider<C, E, R extends Rule> extends Provider<C, E, R> {

}
