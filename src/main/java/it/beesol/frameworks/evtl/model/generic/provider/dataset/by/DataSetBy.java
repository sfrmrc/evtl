package it.beesol.frameworks.evtl.model.generic.provider.dataset.by;

import it.beesol.frameworks.evtl.model.generic.provider.rule.RuleBy;

public interface DataSetBy<S> extends RuleBy<S> {
	
}
