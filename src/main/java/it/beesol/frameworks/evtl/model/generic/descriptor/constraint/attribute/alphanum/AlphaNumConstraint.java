package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.alphanum.AlphaNumType;

public class AlphaNumConstraint extends AttributeConstraint {
	
	   public final static String CONSTRAINT_NAME = "AlphaNumConstraint";
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return CONSTRAINT_NAME;
		}
	
	public 	AlphaNumConstraint(){
		this.type = new AlphaNumType();
	}

}
