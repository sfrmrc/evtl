package it.beesol.frameworks.evtl.model.generic.provider.entity.all.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class EntityProviderAllHSSFImpl extends BaseEntityProviderAllHSSF implements EntityProviderAllHSSF {

	int rowNum;
	int index;
	
	public void init(HSSFSheet sheet) {

		this.container = sheet;
		index = this.rule.getMinSize();
		rowNum = sheet.getLastRowNum() < this.rule.getMaxSize() ? sheet.getLastRowNum() : this.rule.getMaxSize();
		logger.debug("initialized with index="+index+";rowNum="+rowNum);

	}

	public boolean hasNext() {
		return index <= rowNum;
	}

	public HSSFRow next() {
		return container.getRow(index++);
	}

}