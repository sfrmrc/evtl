package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric.IntegerType;

public class InRangeIntegerConstraint extends AttributeConstraint {
	
	public final static String CONSTRAINT_NAME = "InRangeIntegerConstraint";
	
    private Integer min = null;
	
	private Integer max = null;
	
	private Boolean boundaryIncluded = null;
		
	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	

	public Boolean getBoundaryIncluded() {
		return boundaryIncluded;
	}

	public void setBoundaryIncluded(Boolean boundaryIncluded) {
		this.boundaryIncluded = boundaryIncluded;
	}

	@Override
	public String getName() {
		return CONSTRAINT_NAME;
	}
	
	public 	InRangeIntegerConstraint(){
		this.type = new IntegerType();
	}

}
