package it.beesol.frameworks.evtl.model.generic.container.sheet;

import it.beesol.frameworks.evtl.model.generic.container.table.Table;

import java.util.Collection;

public class Sheet<T extends Table, CT extends Collection<T>>
		implements SheetInterface {

	protected String name;

	protected CT tables;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CT getTables() {
		return tables;
	}

	public void setTables(CT tables) {
		this.tables = tables;
	}

}
