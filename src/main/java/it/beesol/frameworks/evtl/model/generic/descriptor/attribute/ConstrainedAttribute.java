package it.beesol.frameworks.evtl.model.generic.descriptor.attribute;

import java.util.List;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class ConstrainedAttribute extends Attribute<Type> implements
		AttributeInterface<Type> {

	private List<AttributeConstraint> attributeConstraints = null;

	public List<AttributeConstraint> getAttributeConstraints() {
		return attributeConstraints;
	}

	public void setAttributeConstraints(
			List<AttributeConstraint> attributeConstraints) {
		this.attributeConstraints = attributeConstraints;
	}

}
