package it.beesol.frameworks.evtl.model.generic.provider.occurrence.by;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.BaseOccurrenceProvider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.BaseRuleBy;

public abstract class BaseOccurrenceProviderBy<C, E, R extends BaseRuleBy> extends BaseOccurrenceProvider<C, E, R> {

}
