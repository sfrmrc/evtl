package it.beesol.frameworks.evtl.model.generic.descriptor.type.alphanum;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class AlphaNumType extends Type {
	
	public AlphaNumType(){
		this.name="ALPHANUMERIC";
		this.javaFqn="java.lang.String";
	}

}
