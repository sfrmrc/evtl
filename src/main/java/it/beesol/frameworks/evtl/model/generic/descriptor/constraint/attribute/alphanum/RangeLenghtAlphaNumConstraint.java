package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;

// TODO : controllo su set verificando congruenza max e min (??)
public class RangeLenghtAlphaNumConstraint extends AlphaNumConstraint {
	
	  public final static String CONSTRAINT_NAME = "RangeLenghtAlphaNumConstraint";
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return CONSTRAINT_NAME;
		}
	
	private Integer minLenght = null;
	
	private Integer maxLenght = null;
	
	private Boolean boundaryIncluded = null;

	public Integer getMinLenght() {
		return minLenght;
	}

	public void setMinLenght(Integer minLenght) {
		this.minLenght = minLenght;
	}

	public Integer getMaxLenght() {
		return maxLenght;
	}

	public void setMaxLenght(Integer maxLenght) {
		this.maxLenght = maxLenght;
	}

	public Boolean getBoundaryIncluded() {
		return boundaryIncluded;
	}

	public void setBoundaryIncluded(Boolean boundaryIncluded) {
		this.boundaryIncluded = boundaryIncluded;
	}

	
	
	
	

}
