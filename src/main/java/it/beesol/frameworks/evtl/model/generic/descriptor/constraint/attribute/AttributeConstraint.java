package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class AttributeConstraint extends Constraint {
 
	protected Type type = null;

	public Type getType() {
		return type;
	}

    public 	AttributeConstraint(){	}
	
    public AttributeConstraint(Type type){
    	this.type = type;
    	
    }
	 
}
 
