package it.beesol.frameworks.evtl.model.generic.provider.entityset.all.xls.hssf;

import it.beesol.frameworks.evtl.model.generic.provider.entityset.all.BaseEntitySetProviderAll;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public abstract class BaseEntitySetProviderAllHSSF extends BaseEntitySetProviderAll<HSSFSheet, HSSFRow> {

}
