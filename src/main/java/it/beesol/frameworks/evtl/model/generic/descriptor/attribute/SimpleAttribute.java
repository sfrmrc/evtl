package it.beesol.frameworks.evtl.model.generic.descriptor.attribute;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class SimpleAttribute implements AttributeInterface<Type> {
 
	private String name;
	 
	private Type type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	
	 
	 
}
 
