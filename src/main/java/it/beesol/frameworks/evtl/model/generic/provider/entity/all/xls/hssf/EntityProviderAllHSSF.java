package it.beesol.frameworks.evtl.model.generic.provider.entity.all.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import it.beesol.frameworks.evtl.model.generic.provider.entity.all.xls.EntityProviderAll;

public interface EntityProviderAllHSSF extends EntityProviderAll<HSSFSheet, HSSFRow> {

}
