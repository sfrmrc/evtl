package it.beesol.frameworks.evtl.model.generic.provider.entityset.all.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import it.beesol.frameworks.evtl.model.generic.provider.entityset.all.EntitySetProviderAll;

public interface EntitySetProviderAllHSSF extends EntitySetProviderAll<HSSFSheet, HSSFSheet> {

}
