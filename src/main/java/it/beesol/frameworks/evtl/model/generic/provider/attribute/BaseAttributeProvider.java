package it.beesol.frameworks.evtl.model.generic.provider.attribute;

import it.beesol.frameworks.evtl.model.generic.provider.BaseProvider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.BaseRule;

public abstract class BaseAttributeProvider<C, E, R extends BaseRule> extends BaseProvider<C, E, R> {

}
