package it.beesol.frameworks.evtl.model.generic.container.row;

import it.beesol.frameworks.evtl.model.generic.container.field.ValidatedSerializableField;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ValidationOccurrence;

public class ValidatedSerializableFieldLinkedSetRow extends SerializableFieldLinkedSetRow<ValidatedSerializableField> {
	
	// occurrence con risultati validazione row su base constrained occurrence
	private ValidationOccurrence validationOccurrence = null;
	
	// boolean riassuntivo validita' complessiva
	private Boolean isValid = null;
	
	public ValidationOccurrence getValidationOccurrence() {
		return validationOccurrence;
	}

	public void setValidationOccurrence(ValidationOccurrence validationOccurrence) {
		this.validationOccurrence = validationOccurrence;
	}

	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	
	public ValidatedSerializableFieldLinkedSetRow(){};
	
	public ValidatedSerializableFieldLinkedSetRow(SerializableFieldLinkedSetRow serializableFieldLinkedSetRow){
		// setting all properties that derive from serializableFieldLinkedSetRow
		this.setName(serializableFieldLinkedSetRow.getName());
		this.setNumber(serializableFieldLinkedSetRow.getNumber());
	}
	
	

}
