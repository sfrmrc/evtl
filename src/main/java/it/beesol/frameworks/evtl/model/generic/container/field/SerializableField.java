package it.beesol.frameworks.evtl.model.generic.container.field;

import java.io.Serializable;

public class SerializableField extends Field implements FieldInterface {

	protected String name;

	protected Serializable value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Serializable getValue() {
		return value;
	}

	public void setValue(Serializable value) {
		this.value = value;
	}

	// TODO : introdurre uso di classi commons per supporto a toString
	
	

	@Override
	public String toString() {
		return "[name=" + this.getName() + ";value=" + this.getValue() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if ( obj == null || !(obj instanceof SerializableField) || this.getName()==null){
			return false;
		}
		SerializableField that = (SerializableField)obj;
		if ( this.getName().equals(that.getName())){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if ( this.getName()!=null ){
			return this.getName().hashCode();
		}
		return 0;
	}

}
