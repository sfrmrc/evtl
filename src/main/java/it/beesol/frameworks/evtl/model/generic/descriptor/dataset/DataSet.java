package it.beesol.frameworks.evtl.model.generic.descriptor.dataset;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class DataSet<T extends Type> implements DataSetInterface<T> {

	private String name;
	 
	private T type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public T getType() {
		return type;
	}

	public void setType(T type) {
		this.type = type;
	}
	
}
