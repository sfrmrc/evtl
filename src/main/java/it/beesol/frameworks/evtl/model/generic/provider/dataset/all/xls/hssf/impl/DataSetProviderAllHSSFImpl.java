package it.beesol.frameworks.evtl.model.generic.provider.dataset.all.xls.hssf.impl;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.xls.hssf.BaseDataSetProviderAllHSSF;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.xls.hssf.DataSetProviderAllHSSF;

/**
 * HSSF manages sheets number as zero based 
 * @author ruggiano
 *
 */
public class DataSetProviderAllHSSFImpl extends BaseDataSetProviderAllHSSF
		implements DataSetProviderAllHSSF {

	private static final long serialVersionUID = 8339017774764962331L;
	
	int index;
	int size;

	public void init(HSSFWorkbook workbook) {
		
		this.container = workbook;
		size = workbook.getNumberOfSheets()<this.rule.getMaxSize() ? workbook.getNumberOfSheets() : this.rule.getMaxSize();
		index = this.rule.getMinSize();
		
	}

	public boolean hasNext() {
		return index!=size;
	}

	public HSSFSheet next() {
		// index incremented after used as sheet index
		return container.getSheetAt(index++);
	}

}
