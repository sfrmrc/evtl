package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;


public class FixedLenghtAlphaNumConstraint extends AlphaNumConstraint {
	
   public final static String CONSTRAINT_NAME = "FixedLenghtAlphaNumConstraint";
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CONSTRAINT_NAME;
	}
	
	private Integer fixedLenght = null;

	public Integer getFixedLenght() {
		return fixedLenght;
	}

	public void setFixedLenght(Integer fixedLenght) {
		this.fixedLenght = fixedLenght;
	}
	
	

}
