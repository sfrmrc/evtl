package it.beesol.frameworks.evtl.model.generic.provider.occurrence.by;

import it.beesol.frameworks.evtl.model.generic.provider.rule.RuleBy;

public interface OccurrenceBy<S> extends RuleBy<S> {

}
