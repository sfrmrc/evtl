package it.beesol.frameworks.evtl.model.generic.descriptor.occurrence;

import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public interface OccurrenceInterface<T extends Type> extends Descriptor {

}
