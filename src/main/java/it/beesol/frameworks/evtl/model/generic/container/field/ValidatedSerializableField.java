package it.beesol.frameworks.evtl.model.generic.container.field;

import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ValidationAttribute;

public class ValidatedSerializableField extends SerializableField {

	private ValidationAttribute validationAttribute;

	private Boolean isValid = null;

	public ValidationAttribute getValidationAttribute() {
		return validationAttribute;
	}

	public void setValidationAttribute(ValidationAttribute validationAttribute) {
		this.validationAttribute = validationAttribute;
	}
	
	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public ValidatedSerializableField(){}
	
	public ValidatedSerializableField(SerializableField serializableField){
		this.setName(serializableField.getName());
		this.setValue(serializableField.getValue());
	}

}
