package it.beesol.frameworks.evtl.model.generic.provider.dataset.all.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.DataSetProviderAll;

public interface DataSetProviderAllHSSF extends DataSetProviderAll<HSSFWorkbook, HSSFSheet> {

}
