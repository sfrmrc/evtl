package it.beesol.frameworks.evtl.model.generic.provider.entityset;

import it.beesol.frameworks.evtl.model.generic.provider.Provider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.Rule;

public interface EntitySetProvider<C, E, R extends Rule> extends Provider<C, E, R> {

}
