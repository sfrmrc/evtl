package it.beesol.frameworks.evtl.model.generic.provider.occurrence.all;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.BaseOccurrenceProvider;

public abstract class BaseOccurrenceProviderAll<C, E>
		extends BaseOccurrenceProvider<C, E, BaseOccurrenceAll> {

}
