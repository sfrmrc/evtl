package it.beesol.frameworks.evtl.model.generic.descriptor.entity;

import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.Attribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.Occurrence;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;


public class Entity<A extends Attribute<Type>,O extends Occurrence> implements EntityInterface {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	 
}
 
