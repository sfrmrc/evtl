package it.beesol.frameworks.evtl.model.generic.provider.occurrence.by.index;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.by.OccurrenceBy;

public interface OccurrenceByIndex extends OccurrenceBy<Integer> {

}
