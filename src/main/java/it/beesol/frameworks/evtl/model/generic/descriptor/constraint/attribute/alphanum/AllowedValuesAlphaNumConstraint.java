package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;

import java.util.Set;

public class AllowedValuesAlphaNumConstraint extends AlphaNumConstraint {
	
	  public final static String CONSTRAINT_NAME = "AllowedValuesAlphaNumConstraint";
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return CONSTRAINT_NAME;
		}
	
	private Set<String> allowedValues = null;

	public Set<String> getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(Set<String> allowedValues) {
		this.allowedValues = allowedValues;
	}
	
	
	
	

}
