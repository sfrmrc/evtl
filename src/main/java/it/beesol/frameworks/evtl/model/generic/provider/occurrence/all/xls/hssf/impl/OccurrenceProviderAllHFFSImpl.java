package it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.xls.hssf.impl;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.xls.hssf.BaseOccurrenceProviderAllHSSF;
import it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.xls.hssf.OccurrenceProviderAllHSSF;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

/**
 * HSSF manages cells number as zero based 
 *
 */
public class OccurrenceProviderAllHFFSImpl extends
		BaseOccurrenceProviderAllHSSF implements OccurrenceProviderAllHSSF {

	private static final long serialVersionUID = -3163675644992641658L;

	int colNum;
	int size;

	public void init(HSSFRow row) {
		
		this.container = row;
		this.colNum = this.rule.getMinSize();
		this.size = this.rule.getMaxSize();
		
	}

	public boolean hasNext() {
		return colNum <= size;
	}

	public HSSFCell next() {
		// colNum incremented after used as cell index
		logger.debug("colNum="+colNum);
		return container.getCell(colNum++);
	}

}