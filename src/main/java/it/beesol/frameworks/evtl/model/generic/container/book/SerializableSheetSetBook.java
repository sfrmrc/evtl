package it.beesol.frameworks.evtl.model.generic.container.book;

import it.beesol.frameworks.evtl.model.generic.container.sheet.SerializableTableSetSheet;

import java.util.Set;

public class SerializableSheetSetBook extends Book<SerializableTableSetSheet, Set<SerializableTableSetSheet>> implements BookInterface {

	private static final long serialVersionUID = 486068161861329483L;

	@Override
	public String toString() {
		return new StringBuilder()
			.append("[name: ").append(name)
			.append(", sheets: ").append(sheet)
			.append("]")
			.toString();
	}
	
}