package it.beesol.frameworks.evtl.model.generic.provider.entityset;

import it.beesol.frameworks.evtl.model.generic.provider.BaseProvider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.BaseRule;

public abstract class BaseEntitySetProvider<C, E, R extends BaseRule>
		extends BaseProvider<C, E, R> {

}
