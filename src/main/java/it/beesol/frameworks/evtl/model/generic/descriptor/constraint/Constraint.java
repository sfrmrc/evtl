package it.beesol.frameworks.evtl.model.generic.descriptor.constraint;



public class Constraint implements ConstraintInterface {
 
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Constraint) ){
			return false;
		}
		Constraint that = (Constraint)obj;
		if ( this.name.equals(that.getName())){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if ( this.getName()!=null ){
			return this.getName().hashCode();
		}
		return 1;
	}
	 
}
 
