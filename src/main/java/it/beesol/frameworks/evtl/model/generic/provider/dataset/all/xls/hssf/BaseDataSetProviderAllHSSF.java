package it.beesol.frameworks.evtl.model.generic.provider.dataset.all.xls.hssf;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.BaseDataSetProvider;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.all.BaseDataSetAll;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class BaseDataSetProviderAllHSSF extends BaseDataSetProvider<HSSFWorkbook, HSSFSheet, BaseDataSetAll> {

}
