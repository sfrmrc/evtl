package it.beesol.frameworks.evtl.model.generic.provider.entity;

import it.beesol.frameworks.evtl.model.generic.provider.Provider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.Rule;

public interface EntityProvider<C, E, R extends Rule> extends Provider<C, E, R> {

}
