package it.beesol.frameworks.evtl.model.generic.descriptor.type;


public class Type implements TypeInterface {
 
	protected String name;
	 
	protected String javaFqn;

	public String getName() {
		return name;
	}

	public String getJavaFqn() {
		return javaFqn;
	}
	
	public Type(){
	}
	
	public Type(String name,String javaFqn){
		this.name=name;
		this.javaFqn=javaFqn;
	}

	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Type) ){
			return false;
		}
		Type that = (Type)obj;
		if ( this.name.equals(that.getName())){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if ( this.getName()!=null ){
			return this.getName().hashCode();
		}
		return 1;
	}
	
	
	 
	 
}
 
