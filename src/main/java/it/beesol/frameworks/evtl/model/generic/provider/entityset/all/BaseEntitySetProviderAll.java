package it.beesol.frameworks.evtl.model.generic.provider.entityset.all;

import it.beesol.frameworks.evtl.model.generic.provider.entityset.BaseEntitySetProvider;

public abstract class BaseEntitySetProviderAll<C, E> extends BaseEntitySetProvider<C, E, BaseEntitySetAll> {

}
