package it.beesol.frameworks.evtl.model.generic.provider.entityset.all.xls.hssf.impl;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import it.beesol.frameworks.evtl.model.generic.provider.entityset.all.xls.hssf.BaseEntitySetProviderAllHSSF;
import it.beesol.frameworks.evtl.model.generic.provider.entityset.all.xls.hssf.EntitySetProviderAllHSSF;
/**
 * 
 * 
 *
 */
public class EntitySetProviderAllHSSFImpl extends BaseEntitySetProviderAllHSSF
		implements EntitySetProviderAllHSSF {

	int rowNum;
	int index;

	private static final long serialVersionUID = 5060235772798822662L;

	public void init(HSSFSheet sheet) {

		this.container = sheet;
		index = 0;
		rowNum = 1;

	}

	public boolean hasNext() {
		return index < rowNum;
	}

	// always returns same sheet on which it has been initialized
	public HSSFSheet next() {
		index++;
		return container;
	}

}
