package it.beesol.frameworks.evtl.model.generic.provider.occurrence;

import it.beesol.frameworks.evtl.model.generic.provider.Provider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.Rule;

public interface OccurrenceProvider<C, E, R extends Rule>
		extends Provider<C, E, R> {

}
