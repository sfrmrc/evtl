package it.beesol.frameworks.evtl.model.generic.descriptor.type.date;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class DateType extends Type {
	
	public DateType(){
		this.name="DATE";
		this.javaFqn="java.util.Date";
	}

}
