package it.beesol.frameworks.evtl.model.generic.descriptor.entityset;

import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public interface EntitySetInterface<T extends Type> extends Descriptor {

}
