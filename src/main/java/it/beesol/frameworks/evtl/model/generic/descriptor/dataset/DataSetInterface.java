package it.beesol.frameworks.evtl.model.generic.descriptor.dataset;

import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public interface DataSetInterface<T extends Type> extends Descriptor {
 
}
