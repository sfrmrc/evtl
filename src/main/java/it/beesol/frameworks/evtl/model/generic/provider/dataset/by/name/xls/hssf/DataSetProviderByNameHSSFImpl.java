package it.beesol.frameworks.evtl.model.generic.provider.dataset.by.name.xls.hssf;

import java.util.Iterator;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.BaseDataSetProvider;
import it.beesol.frameworks.evtl.model.generic.provider.dataset.by.name.BaseDataSetProviderByName;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class DataSetProviderByNameHSSFImpl extends
		BaseDataSetProviderByName<HSSFWorkbook, HSSFSheet> implements
		DataSetProviderByNameHSSF {

	private static final long serialVersionUID = -7478693389964437941L;

	Iterator<String> iterator;

	public void init(HSSFWorkbook workbook) {
		this.container = workbook;
		this.rule.getEntitySet().iterator();
	}

	public boolean hasNext() {
		return iterator.hasNext();
	}

	public HSSFSheet next() {
		return container.getSheet(iterator.next());
	}

}
