package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.date;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.date.DateType;

public class DateConstraint extends AttributeConstraint {
	
	   public final static String CONSTRAINT_NAME = "DateConstraint";
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return CONSTRAINT_NAME;
		}
	
	public 	DateConstraint(){
		this.type = new DateType();
	}

}
