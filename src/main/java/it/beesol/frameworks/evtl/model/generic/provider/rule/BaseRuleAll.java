package it.beesol.frameworks.evtl.model.generic.provider.rule;

public class BaseRuleAll extends BaseRule {

	private Integer maxSize;
	
	private Integer minSize;

	public void setMaxSize(Integer maxSize) {
		this.maxSize = maxSize;
	}

	public Integer getMaxSize() {
		return maxSize;
	}

	public void setMinSize(Integer minSize) {
		this.minSize = minSize;
	}

	public Integer getMinSize() {
		return minSize;
	}
	
}
