package it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric;

import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class NumericType extends Type {
	
	public NumericType(){
		this.name="NUMERIC";
		this.javaFqn="java.lang.Number";
	}

}
