package it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.BaseOccurrenceProviderAll;

public abstract class BaseOccurrenceProviderAllHSSF extends
		BaseOccurrenceProviderAll<HSSFRow, HSSFCell> {

}
