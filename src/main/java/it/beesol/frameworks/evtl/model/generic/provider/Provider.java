package it.beesol.frameworks.evtl.model.generic.provider;

import it.beesol.frameworks.evtl.model.generic.GenericModel;
import it.beesol.frameworks.evtl.model.generic.provider.rule.Rule;

public interface Provider<C, E, R extends Rule> extends GenericModel {

	public void init(C objectToIterateOn);

	public boolean hasNext();

	public E next();

}
