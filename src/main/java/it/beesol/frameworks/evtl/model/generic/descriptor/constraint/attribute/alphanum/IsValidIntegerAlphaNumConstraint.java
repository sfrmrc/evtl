package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;

import java.util.Locale;


public class IsValidIntegerAlphaNumConstraint extends AlphaNumConstraint {
	 public final static String CONSTRAINT_NAME = "IsValidIntegerAlphaNumConstraint";
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return CONSTRAINT_NAME;
		}
	
	private Locale locale = null;
	
	protected String pattern = "#,##0;";
	
	private boolean includePositive = true;
	
	private boolean includeNegative = true;
	
	private boolean includeZero = true;

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getPattern() {
		return pattern;
	}

	public boolean isIncludePositive() {
		return includePositive;
	}

	public void setIncludePositive(boolean includePositive) {
		this.includePositive = includePositive;
	}

	public boolean isIncludeNegative() {
		return includeNegative;
	}

	public void setIncludeNegative(boolean includeNegative) {
		this.includeNegative = includeNegative;
	}

	public boolean isIncludeZero() {
		return includeZero;
	}

	public void setIncludeZero(boolean includeZero) {
		this.includeZero = includeZero;
	}
	
}
