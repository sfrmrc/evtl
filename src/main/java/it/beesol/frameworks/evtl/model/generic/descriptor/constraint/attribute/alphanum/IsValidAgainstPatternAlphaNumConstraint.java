package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.alphanum;

import java.util.Locale;
import java.util.TimeZone;


public class IsValidAgainstPatternAlphaNumConstraint extends AlphaNumConstraint {
	
	public final static String CONSTRAINT_NAME = "IsValidAgainstPatternAlphaNumConstraint";
	
	@Override
	public String getName() {
		return CONSTRAINT_NAME;
	}
		
	private String pattern = null;
	
	private Locale locale = null;
	
	private TimeZone timezone = null;

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public TimeZone getTimezone() {
		return timezone;
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}
	
}
