package it.beesol.frameworks.evtl.model.generic.provider.dataset.by.name;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.by.DataSetBy;

public interface DataSetByName extends DataSetBy<String> {

}
