package it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.xls.hssf;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.all.OccurrenceProviderAll;

public interface OccurrenceProviderAllHSSF extends OccurrenceProviderAll<HSSFRow, HSSFCell> {

}
