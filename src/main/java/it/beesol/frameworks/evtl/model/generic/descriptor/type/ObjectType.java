package it.beesol.frameworks.evtl.model.generic.descriptor.type;

public class ObjectType extends Type {
	
	public ObjectType(){
		this.name="ALL";
		this.javaFqn="java.lang.Object";
	}

}
