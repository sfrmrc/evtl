package it.beesol.frameworks.evtl.model.generic.provider.dataset.by.name;

import it.beesol.frameworks.evtl.model.generic.provider.dataset.DataSetProvider;

public interface DataSetProviderByName<C, E> extends
		DataSetProvider<C, E, DataSetByName> {

}
