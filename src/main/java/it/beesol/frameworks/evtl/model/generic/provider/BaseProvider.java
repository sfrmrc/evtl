package it.beesol.frameworks.evtl.model.generic.provider;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import it.beesol.frameworks.evtl.model.generic.provider.rule.BaseRule;

public abstract class BaseProvider<C, E, R extends BaseRule> {
	
	protected Log logger = LogFactory.getLog(this.getClass());
	
	protected C container;
	protected R rule;
	
	public void setContainer(C container) {
		this.container = container;
	}
	
	public C getContainer() {
		return container;
	}
	
	public void setRule(R rule) {
		this.rule = rule;
	}
	
	public R getRule() {
		return rule;
	}
	
}
