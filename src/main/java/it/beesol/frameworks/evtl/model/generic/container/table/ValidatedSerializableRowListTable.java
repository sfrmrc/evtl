package it.beesol.frameworks.evtl.model.generic.container.table;

import java.util.List;

import it.beesol.frameworks.evtl.model.generic.container.row.ValidatedSerializableFieldLinkedSetRow;
import it.beesol.frameworks.evtl.model.generic.descriptor.entity.ValidationEntity;

public class ValidatedSerializableRowListTable extends SerializableRowListTable {
	
	// lista row validate
	private List<ValidatedSerializableFieldLinkedSetRow> validRows = null;
	
	// lista row non validate
	private List<ValidatedSerializableFieldLinkedSetRow> notValidRows = null;
	
	// entity con risultati validazione table su base constrained entity
	private ValidationEntity validationEntity = null;
	
	private Boolean isValid = null;
	
	public List<ValidatedSerializableFieldLinkedSetRow> getValidRows() {
		return validRows;
	}

	public void setValidRows(List<ValidatedSerializableFieldLinkedSetRow> validRows) {
		this.validRows = validRows;
	}

	public List<ValidatedSerializableFieldLinkedSetRow> getNotValidRows() {
		return notValidRows;
	}

	public void setNotValidRows(List<ValidatedSerializableFieldLinkedSetRow> notValidRows) {
		this.notValidRows = notValidRows;
	}

	public ValidationEntity getValidationEntity() {
		return validationEntity;
	}

	public void setValidationEntity(ValidationEntity validationEntity) {
		this.validationEntity = validationEntity;
	}
	
	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public ValidatedSerializableRowListTable(){}
	
	public ValidatedSerializableRowListTable(SerializableRowListTable serializableRowListTable){
		this.setName(serializableRowListTable.getName());
		this.setRows(serializableRowListTable.getRows());
	}
	
}
