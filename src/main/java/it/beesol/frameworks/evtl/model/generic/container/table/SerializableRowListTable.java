package it.beesol.frameworks.evtl.model.generic.container.table;

import java.util.LinkedHashSet;
import java.util.List;
import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;
import it.beesol.frameworks.evtl.model.generic.container.row.SerializableFieldLinkedSetRow;


public class SerializableRowListTable
   extends Table<SerializableField,
                 LinkedHashSet<SerializableField>,
                 SerializableFieldLinkedSetRow<SerializableField>,
                 List<SerializableFieldLinkedSetRow<SerializableField>>> {
 
  private static final long serialVersionUID = 732710729983691863L;

	@Override
	public String toString() {
		return new StringBuilder()
			.append("[")
			.append("name:").append(name)
			.append(", rows:").append(rows)
			.append("]")
			.toString();
	}
	 
}
 
