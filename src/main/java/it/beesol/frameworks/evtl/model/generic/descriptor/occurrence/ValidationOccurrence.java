package it.beesol.frameworks.evtl.model.generic.descriptor.occurrence;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import java.util.HashMap;

public class ValidationOccurrence extends ConstrainedOccurrence {

	private HashMap<Constraint, ValidationResult> constraintValidationResult = null;

	public HashMap<Constraint, ValidationResult> getConstraintValidationResult() {
		return constraintValidationResult;
	}

	public void setConstraintValidationResult(
			HashMap<Constraint, ValidationResult> constraintValidationResult) {
		this.constraintValidationResult = constraintValidationResult;
	}
	
	
	public ValidationOccurrence(){}
	
	public ValidationOccurrence(ConstrainedOccurrence constrainedOccurrence){
		this.setName(constrainedOccurrence.getName());
		this.setConstrainedAttributes(constrainedOccurrence.getConstrainedAttributes());
		this.setOccurrenceConstraints(constrainedOccurrence.getOccurrenceConstraints());
	}

}
