package it.beesol.frameworks.evtl.model.generic.container.sheet;

import it.beesol.frameworks.evtl.model.generic.container.table.SerializableRowListTable;

import java.util.Set;

public class SerializableTableSetSheet extends Sheet<SerializableRowListTable, Set<SerializableRowListTable>> {

	@Override
	public String toString() {
		return new StringBuilder()
			.append("[")
			.append("name: ").append(name)
			.append(", tables: ").append(tables)
			.append("]")
			.toString();
	}
	
}
 
