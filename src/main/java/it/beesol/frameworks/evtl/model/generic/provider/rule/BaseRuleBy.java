package it.beesol.frameworks.evtl.model.generic.provider.rule;

import java.util.Set;

public class BaseRuleBy<S> extends BaseRule implements RuleBy<S> {

	protected Set<S> entitySet;
	
	public void setEntitySet(Set<S> entitySet) {
		this.entitySet = entitySet;
	}

	public Set<S> getEntitySet() {
		return entitySet;
	}
	
}
