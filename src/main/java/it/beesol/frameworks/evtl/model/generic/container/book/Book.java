package it.beesol.frameworks.evtl.model.generic.container.book;

import java.util.Collection;

import it.beesol.frameworks.evtl.model.generic.container.sheet.SheetInterface;

public class Book<S extends SheetInterface, CS extends Collection<S>> {

	protected String name;
	
	protected CS sheet;

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setSheet(CS sheet) {
		this.sheet = sheet;
	}

	public CS getSheet() {
		return sheet;
	}
	
}
