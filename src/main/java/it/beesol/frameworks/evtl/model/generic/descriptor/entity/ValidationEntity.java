package it.beesol.frameworks.evtl.model.generic.descriptor.entity;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.Constraint;
import it.beesol.frameworks.evtl.model.generic.validation.ValidationResult;

import java.util.HashMap;

public class ValidationEntity extends ConstrainedEntity {

	private HashMap<Constraint, ValidationResult> constraintValidationResult = null;

	public HashMap<Constraint, ValidationResult> getConstraintValidationResult() {
		return constraintValidationResult;
	}

	public void setConstraintValidationResult(
			HashMap<Constraint, ValidationResult> constraintValidationResult) {
		this.constraintValidationResult = constraintValidationResult;
	}
	
	
	public ValidationEntity(){}
	
	public ValidationEntity(ConstrainedEntity constrainedEntity){
		this.setName(constrainedEntity.getName());
		this.setConstrainedOccurrence(constrainedEntity.getConstrainedOccurrence());
		this.setEntityConstraints(constrainedEntity.getEntityConstraints());
	}

}
