package it.beesol.frameworks.evtl.model.generic.provider.entity.all.xls;

import it.beesol.frameworks.evtl.model.generic.provider.entity.EntityProvider;

public interface EntityProviderAll<C, E> extends EntityProvider<C, E, EntityAll> {

}