package it.beesol.frameworks.evtl.model.generic.container.row;

import it.beesol.frameworks.evtl.model.generic.container.field.SerializableField;

import java.util.LinkedHashSet;

public class SerializableFieldLinkedSetRow<SR extends SerializableField> extends Row<SR,LinkedHashSet<SR>> {
 
	@Override
	public String toString() {
		return new StringBuilder()
			.append("[")
			.append("name: ").append(name)
			.append(", fields: ").append(fields)
			.append("]")
			.toString();
	}
	 
}
 
