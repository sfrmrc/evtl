package it.beesol.frameworks.evtl.model.generic.descriptor.attribute;

import it.beesol.frameworks.evtl.model.generic.descriptor.Descriptor;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public interface AttributeInterface<T extends Type> extends Descriptor {
 
}
 
