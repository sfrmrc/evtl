package it.beesol.frameworks.evtl.model.generic.provider.occurrence;

import it.beesol.frameworks.evtl.model.generic.provider.BaseProvider;
import it.beesol.frameworks.evtl.model.generic.provider.rule.BaseRule;

public abstract class BaseOccurrenceProvider<C, E, R extends BaseRule>
		extends BaseProvider<C, E, R> {

}
