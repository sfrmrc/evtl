package it.beesol.frameworks.evtl.model.generic.provider.occurrence.by;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.OccurrenceProvider;

public interface OccurrenceProviderBy<C, E> extends
		OccurrenceProvider<C, E, OccurrenceBy> {

}
