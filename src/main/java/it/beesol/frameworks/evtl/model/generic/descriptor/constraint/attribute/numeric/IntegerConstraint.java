package it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.numeric;

import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.attribute.AttributeConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric.IntegerType;

public class IntegerConstraint extends AttributeConstraint {
	
	   public final static String CONSTRAINT_NAME = "IntegerConstraint";
		
	@Override
	public String getName() {
		return CONSTRAINT_NAME;
	}
	
	public 	IntegerConstraint(){
		this.type = new IntegerType();
	}

}
