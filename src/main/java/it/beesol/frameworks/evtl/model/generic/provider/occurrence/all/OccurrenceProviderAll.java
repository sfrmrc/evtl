package it.beesol.frameworks.evtl.model.generic.provider.occurrence.all;

import it.beesol.frameworks.evtl.model.generic.provider.occurrence.OccurrenceProvider;

public interface OccurrenceProviderAll<C, E> extends
		OccurrenceProvider<C, E, OccurrenceAll> {

}
