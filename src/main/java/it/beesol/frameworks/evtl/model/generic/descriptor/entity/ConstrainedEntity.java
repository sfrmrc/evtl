package it.beesol.frameworks.evtl.model.generic.descriptor.entity;


import java.util.List;



import it.beesol.frameworks.evtl.model.generic.descriptor.attribute.ConstrainedAttribute;
import it.beesol.frameworks.evtl.model.generic.descriptor.constraint.entity.EntityConstraint;
import it.beesol.frameworks.evtl.model.generic.descriptor.occurrence.ConstrainedOccurrence;
import it.beesol.frameworks.evtl.model.generic.descriptor.type.Type;

public class ConstrainedEntity extends Entity<ConstrainedAttribute,ConstrainedOccurrence>{
	
	protected ConstrainedOccurrence constrainedOccurrence = null;

	private List<EntityConstraint> entityConstraints = null;

	public ConstrainedOccurrence getConstrainedOccurrence() {
		return constrainedOccurrence;
	}

	public void setConstrainedOccurrence(ConstrainedOccurrence constrainedOccurrence) {
		this.constrainedOccurrence = constrainedOccurrence;
	}

	public List<EntityConstraint> getEntityConstraints() {
		return entityConstraints;
	}

	public void setEntityConstraints(List<EntityConstraint> entityConstraints) {
		this.entityConstraints = entityConstraints;
	}

	
	
	
	

}
