package it.beesol.frameworks.evtl.model.generic.descriptor.type.numeric;

public class IntegerType extends NumericType {
	
	public IntegerType(){
		this.name="INTEGER";
		this.javaFqn="java.lang.Integer";
	}


}
