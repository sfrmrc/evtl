package it.beesol.frameworks.evtl.model.generic.container.row;

import it.beesol.frameworks.evtl.model.generic.container.field.Field;

import java.util.Collection;

public class Row<T extends Field,C extends Collection<T>> implements RowInterface{

	protected String name = null;
	
	protected Integer number = null;
	
	protected C fields;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public C getFields() {
		return fields;
	}

	public void setFields(C fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Row [");
		if (name != null)
			builder.append("name=").append(name).append(", ");
		if (number != null)
			builder.append("number=").append(number);
		builder.append("]");
		return builder.toString();
	}

	

		
	
	
}
